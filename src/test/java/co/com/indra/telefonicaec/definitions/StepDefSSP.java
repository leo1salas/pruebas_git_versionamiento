package co.com.indra.telefonicaec.definitions;

import co.com.indra.telefonicaec.pages.PageDefault;
import cucumber.api.java.en.Given;

public class StepDefSSP {
	
	PageDefault paginaDefault;
	
	@Given("Ingreso a {string} con {string} clic en {string}")
	public void ingreso(String url,String puerto,String clic) {
		//instancio el default
		if (paginaDefault == null) {
			paginaDefault = new PageDefault();
		}
		//ingresar a la URL Movistar
		paginaDefault.irA(url,puerto);
		//dar clic en boton mi cuenta
		paginaDefault.clic(clic);
	}
	
	@Given("Ingreso cedula {string} en {string} clic en {string}")
	public void cedula(String cedula, String campoCedula, String clic) {
		// instancio el default
		paginaDefault = new PageDefault();
		//ingresar cedula
		paginaDefault.ingresarTextoProperties(campoCedula, cedula);
		//ingresar en continuar
		paginaDefault.clic(clic);
	}
	
	@Given("Ingreso codigo {string} en {string} clic en {string}")
	public void codigo(String codigo, String campoCodigo, String clic) {
		// instancio el default
		paginaDefault = new PageDefault();
		//ingresar codigo
		paginaDefault.ingresarTextoProperties(campoCodigo, codigo);
		//ingresar en continuar
		paginaDefault.clic(clic);
	}
	
	@Given("Initssp {string} y canal {string}")
    public void init(String namefeature, String canal) {
        // instancio el default

	 paginaDefault.establescoCanal(canal);

	 paginaDefault.agregaMapaJson("1.Bloque",namefeature);

    }

}
