package co.com.indra.telefonicaec.definitions;


import java.util.Map;

import co.com.indra.telefonicaec.entities.Reporte;
import co.com.indra.telefonicaec.objects.ReporteController;
import co.com.indra.telefonicaec.pages.PageDefault;
import co.com.indra.telefonicaec.utilities.ControllerUtil;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Stepdefs {

	ControllerUtil controllerUtil;
    PageDefault page;
    Map<String, String> mapaParametros=null;
    //private static HashMap<Integer,String> scenarios;
        
	PageDefault paginaDefault;
	
	String testID;
	String groupName;
	String featureScenario;
	String featureName;
	String rawFeatureName;	

	@Given("Estoy en la URL {string}")
	public void estoyEnLaUrl(String url) {
		paginaDefault = new PageDefault();
		paginaDefault.irA(url);
	}

	/*@Given("Prueba uno")
	public void givenPruebaUno() {
		paginaDefault = new PageDefault();
		System.out.println("Inicia");
		
		Reporte reporte = new Reporte("nombreFeature", "nombreScenario", "tags", "menaje", true);
		ReporteController.guardarEnReporte(reporte);

	}*/

	@Given("Prueba dos {string}")
	public void givenPruebaDos(String valorObtenido) {
		System.out.println("Antes");
		String r = paginaDefault.consultarPropiedadBD(valorObtenido);
				
		System.out.println("Se trae " + r);
	}
    @When("esperar carga pantalla")
    public void esperarCargaPantalla() {
        paginaDefault.esperarCargaPantalla();

    }

	@When("Validar cargando")
	public void validarCargando() {
		paginaDefault.validarCargando();
	}

	@When("Clic en boton por texto {string}")
	public void clicBotonTexto(String textoBoton) {
		paginaDefault.clicBotonTexto(textoBoton);
	}

	@When("Clic en link por texto {string}")
	public void clicLinkTexto(String textoBoton) {
		paginaDefault.clicLinkTexto(textoBoton);
	}

	@Given("Estoy en la URL {string} con {string} y {string}")
	public void estoyEnLaUrlPuerto(String url, String puerto, String pathUrl) {
		paginaDefault.irA(url, puerto, pathUrl);
	}

	@Given("Valido estilo {string} campo {string}")
	public void validoEstiloCampo(String estilo, String objeto) {
		paginaDefault.validoEstiloCampo(objeto, estilo);
	}

	@Given("Ingreso {string} en el campo {string}")
	public void ingresoEnElCampoPropertie(String texto, String objeto) {
		paginaDefault.ingresarTextoProperties(objeto, texto);
	}

	@Given("Login en {string} {string} {string} con user {string} en {string} pass {string} en {string} clic en {string}")
	public void loginEn(String urlIp, String puerto, String path, String usuario, String campoUsuario,
			String contrasenia, String campoContrasenia, String clic) {
		// instancio el default
		paginaDefault = new PageDefault();
		// voy a la url login
		paginaDefault.irA(urlIp, puerto, path);
		// ingreso el usuario
		paginaDefault.ingresarTextoProperties(campoUsuario, usuario);
		// ingreso la contrasenia
		paginaDefault.ingresarTextoProperties(campoContrasenia, contrasenia);
		// clic en boton
		paginaDefault.clic(clic);
	}

	@Given("Login {string} con {string} user {string} en {string} pass {string} en {string} clic en {string}")
	public void login(String url, String puerto, String usuario, String campoUsuario, String contrasenia,
			String campoContrasenia, String clic) {
		// instancio el default
		if (paginaDefault == null) {
			paginaDefault = new PageDefault();
		}
		// voy a la url login
		paginaDefault.irA(url, puerto);
		// ingreso el usuario
		paginaDefault.ingresarTextoProperties(campoUsuario, usuario);
		// ingreso la contrasenia
		paginaDefault.ingresarTextoProperties(campoContrasenia, contrasenia);
		// clic en boton
		paginaDefault.clic(clic);
	}

	@Then("Logueo exitoso")
	public void logueoExitoso() {
		System.out.println("Fin Login");
	}

	@Given("Estoy en {string} ingreso {string} en {string} y paso a {string} luego a {string}")
	public void busquedaCuentaCliente(String url, String cuentaCliente, String campoCuenta, String urlDos,
			String urlTres) {
		paginaDefault.irA(url);
		paginaDefault.ingresarTexto(campoCuenta, cuentaCliente);
		paginaDefault.irA(urlDos);
		paginaDefault.irA(urlTres);
	}

	@Given("Doy clic en {string}")
	public void doyClicEn(String objeto) {
		paginaDefault.esperarDisplayed(objeto);
		paginaDefault.clic(objeto);
	}

	@Given("Selecciono el campo {string} que esta en iframe {string}")
	public void clicEnFrame(String texto, String frame) {
		paginaDefault.cambiarIframe(frame);
		paginaDefault.clic(texto);
	}

	@Given("Ingreso {string} en el campo {string} que esta en iframe {string}")
	public void clicEnFrame(String texto, String objeto, String frame) {
		paginaDefault.cambiarIframe(frame);
		paginaDefault.ingresarTextoProperties(objeto, texto);
	}

	@Given("Valido que el campo {string} tenga valor de {string}")
	public void validarValorCampo(String campo, String valor) {
		paginaDefault.validarValorCampo(campo, valor);
	}

	@Given("Cambio al frame {string}")
	public void switchFrame(String frame) {
		paginaDefault.cambiarIframe(frame);
	}

	@When("cambiar frame interno")
	public void cambiarFrameInterno() {
		paginaDefault.cambiarFrameInterno();

	}
	
	@When("cargar servicios y caracteristicas")
	public void cargarserviciosycaracteristicas() {
		paginaDefault.esperarHabilitarServicioCaracteristica();

	}
	
	@When("cambiar a frame principal")
	public void cambiarFramePrincipal() throws Exception {
		paginaDefault.cambiarIframePrincipal();
	}

	@Given("Doy clic en {string} forzado")
	public void doyClicEnForzado(String objeto) {
		paginaDefault.clicForzado(objeto);
	}

	@Given("Guardo el atributo {string} de {string} como {string}")
	public void guardo_el_atributo_de_como(String atributo, String objeto, String guardar) {
		paginaDefault.obtenerGuardarString(objeto, atributo, guardar);
	}

	@Given("Esperar a que el campo {string} esté visible")
	public void esperarAQueElCampoEsteVisible(String objeto) {
		paginaDefault.esperarDisplayed(objeto);
	}
	////////// CAMBIOS XLS

	@Given("Ingreso {string} en el combo {string}")
	public void ingresoValorCombo(String texto, String objeto) {
		paginaDefault.ingresoValorCombo(objeto, texto);
	}
	
	@Given("Ingreso {string} en el combo ssp {string}")
	public void ingresoValorCombossp(String texto, String objeto) {
		paginaDefault.ingresoValorCombossp(objeto, texto);
	}

	@Given("Ingreso valor {string} en el campo {string} y {string}")
	public void ingresoEnElCampoXls(String posicion, String objeto, String propiedad) {
		Boolean exsite = paginaDefault.vetificarExisteRegistro(posicion, propiedad);
		if (exsite) {
			paginaDefault.ingresarValorXls(objeto, posicion, propiedad,"2.Equipo");
		} else {
			paginaDefault.finalize();
		}
	}

	@Given("Ingreso valor {string} en el campo {string} y {string} con NA {string}")
	public void ingresoEnElCampoXlsNA(String texto, String objeto, String propiedad, String valorNa) {
		Boolean exsite = paginaDefault.vetificarExisteRegistro(texto, propiedad);
		if (exsite) {
			paginaDefault.ingresarValorXlsNa(objeto, texto, propiedad, valorNa);
		} else {
			paginaDefault.finalize();
		}

	}

	@Given("Validar {string} modificado {string} columna {string}")
	public void validarModificado(String row, String propiedadModificado, String columna) {
		if (paginaDefault == null) {
			System.out.println("pageDefaultnull");
			paginaDefault = new PageDefault();
		}
		Boolean exsite = paginaDefault.vetificarExisteRegistro(row, columna);
		if (exsite) {
			System.out.println("registro existe");
			paginaDefault.validarModificado(row, propiedadModificado, columna);
		} else {
			System.out.println("finalizar pagina");
			paginaDefault.finalize();
		}

	}

	@Given("Valido que el campo {string} tenga valor {string} y {string} NA {string}")
	public void validarValorCampoXls(String campo, String valor, String propiedad, String validaNA) {
		paginaDefault.validarValorCampoXls(campo, valor, propiedad, validaNA);
	}
	
	@Given("Almaceno el campo {string} y su valor {string} y propiedad {string} NA {string} con {string}")
	public void almacenarValorCampoXls(String campo, String valor, String propiedad, String validaNA, String cuota) {
		paginaDefault.almacenarValorCampoXls(campo, valor, propiedad, validaNA,cuota);
	}
	
	@Given("Almaceno el campo {string} con numero de cuotas {string} y las propiedades {string} y {string}")
	public void almacenarCamposValidarPrecioConCuotas(String precio, String cuotas, String montoCuotas, String montoTotal) {
		paginaDefault.almacenarCamposMontoCuotaDelModelo(precio, cuotas, montoCuotas, montoTotal);
	}
	
	@Given("Almaceno valor {string} y propiedad {string} con numero de cuota {string} y la propidad {string}")
	public void almacenarCamposValidarPrecioConCuotaSSP(String rowxls, String propiedad, String montoCuota, String valorCuota) {
		paginaDefault.almacenarCamposValidarPrecioConCuotaSSP(rowxls, propiedad, montoCuota, valorCuota);
	}
	
	@Given("Valido que los campos tengan los valores esperados")
	public void validarValoresCuotas() {
		paginaDefault.validarValoresCuotas();
	}
	
	@Given("Consulto la tabla {string} con el id {string}")
    public void consultarTablaEntradaFlujo(String tabla, String id){
        mapaParametros=ControllerUtil.consultarTablaEntradaFlujo(tabla,id,featureName,featureScenario,groupName);
    }
	
	@When("selecionar contrato {string} ICCD {string} numero telefono {string}")
    public void selecionarContratoICCDNumeroTelefono(String contrato, String numeroSim, String numeroTelefono) {
        contrato=mapaParametros.get(contrato);
        numeroSim=mapaParametros.get(numeroSim);
        numeroTelefono=mapaParametros.get(numeroTelefono);
        paginaDefault.selecionarContratoICCDNumeroTelefono(contrato, numeroSim, numeroTelefono);
    }
	
	@When("ingresar imei {string}")
    public void IngresarImei(String imei)throws Exception {
        imei = mapaParametros.get(imei);
        paginaDefault.ingresarImei(imei);

    } 

	 @When("validar crear cliente {string}")
	    public void validarCrearCliente(String cliente) {
	        cliente=mapaParametros.get(cliente);
	        paginaDefault.validarCrearCliente(cliente);
	    }
	 
	 @Then("cambio a miga de pan validar")
	    public void migaPanRevision() {
		 	paginaDefault.migaPanRevision();
	    }
	 
	 @Then("clic boton final reservar activo")
	    public void clicBotonFinalProcesoReservarActivo() {
		 	paginaDefault.clicBotonFinalProcesoReservarActivo();
	    }
	 
	 @Then("clic boton final configurar contrato")
	    public void clicBotonFinalProcesoConfigurarContrato() {
		 	paginaDefault.clicBotonFinalProcesoConfigurarContrato();
	    }
	 
	 @Then("clic boton final generar factura")
	    public void clicBotonFinalProcesoGenerarFactura() {
	        paginaDefault.clicBotonFinalProcesoGenerarFactura();
	    }
	 
	 @Then("clic boton final enviar")
	    public void clicBotonFinalEnviar() {
	        paginaDefault.clicBotonFinalEnviar();
	    }
	 
	 @Then("extraer Numero orden de venta")
	    public void extraerNumeroordendeventa() throws Exception {
	        paginaDefault.extraerNumeroordendeventa();
	    }
	 
	 @Then("Volver a la Orden")
	    public void volveralaOrden() {
		 paginaDefault.volveralaOrden();
	    }
	 
	 @Then("Entrada de Orden")
	    public void entradadeOrden() throws InterruptedException {
		 	paginaDefault.entradadeOrden();
	    }
	 
	 @Then("Seleccionar la orden anterior")
	    public void seleccionarlaOrdenAnterior() throws Exception {
		 	paginaDefault.seleccionarlaOrdenAnterior();
	    }
	 
	 @Then("Verificar Numero de Cuenta y Guardar URL")
	    public void verificarNumerodeCuentayGuardarURL() throws Exception {
	        paginaDefault.verificarNumerodeCuentayGuardarURL();
	    }
	 
	 @Then("Guardar resultado en BD flujo {string}")
	    public void guardarResultadoBD() throws Exception {
	        paginaDefault.actualizarResultadoFlujo(groupName,featureName,featureScenario, mapaParametros.get("id"));
	    }
	 
	 @Given("Ingresar imei {string} del equipo {string}")
		public void ingresarImeiEquipo(String imei,String nombreEquipo) throws Exception {
		 imei = mapaParametros.get(imei);
		 paginaDefault.validarEquipoIngresarImei(nombreEquipo, imei);
		}
	 
	 @Given("Validar tipo plan {string}")
		public void validatTipolLan(String plan) {
		 paginaDefault.validarTipoPlan(plan);
		}
	 
	 @Given("cambiar limite credito {string}")
	    public void limiteCredito(String credito) throws Exception {
	        paginaDefault.limiteCredito(credito);
	    }
	 
	 @Given("generar documento")
	    public void generarDocumento() {
	        paginaDefault.generarDocumento();
	    }
	 
	 @When("ingreso Canal de distribucion {string}")
	    public void igresarCanalDistribucion(String valor) throws InterruptedException {
	        valor=mapaParametros.get(valor);
	        paginaDefault.ingresarCanalDistribucion(valor);

	    }
	 
	 @When("seleciono combo orden venta {string} para {string}")
	    public void selecionarComboOrdenVenta(String valor, String telefono) {
	        telefono=mapaParametros.get(telefono);
	        paginaDefault.selecionarComboOrdenVenta(valor, telefono);

	    }
	 
	 @When ("Doy clic en el plan {string}")
	 public void busquedaPlanRenovacion (String texto) {
		 paginaDefault.busquedaPlanParaRenovacion(texto);
	 }
	 
	 
	 @Given("Selecciono el plan anadido")
	    public void seleccionarElPlanAnadido() {
	        paginaDefault.seleccionarPlanAnadido();
	    }
	 
	 @Given("Ingreso {string} en el campo servicio de busqueda")
		public void ingresoPlanEnElCampoSB(String texto) {
		 	//texto=mapaParametros.get();
			paginaDefault.ingresarTextoPlanNC(texto);
		}
	 
	 @Given("Ingreso plan familiar {string} en el campo servicio de busqueda")
		public void ingresoPlanFamiliarEnElCampoSB(String texto) {
		 	//texto=mapaParametros.get();
			paginaDefault.ingresarTextoPlanFamiliarNC(texto);
		}
	 
	 	/**
		 * Metodo que captura el nombre del tag, feature y scenario
		 * @param scenario
		 */
		public void obtieneNameScenario(Scenario scenario){
			
			featureScenario = scenario.getName();		
			groupName = "";
			for(String tag : scenario.getSourceTagNames()){
				groupName = groupName + " " + tag.substring(0);
	            System.out.println("Tag: " + tag);
	        }
							
		    rawFeatureName = scenario.getId().split(";")[0].replace("-"," ");
		    featureName = rawFeatureName.substring(0, 1).toUpperCase() + rawFeatureName.substring(1);
		    
		    System.out.println("Complete Scenario is :-" +featureScenario);
			System.out.println("Group Name is:- "+ groupName);
			System.out.println("Feature Name is:- "+ featureName);
			System.out.println("============================================================================================");
		    
		}
		

	 @When("espero habilitar servicios y caracteristicas")
	    public void esperarHabilitarServiciosCaracteristica() {
	        paginaDefault.esperarHabilitarServiciosyCaracteristicas();

	    }
	 

	 @Given("Init {string} y canal {string}")
	    public void init(String namefeature, String canal) {
	        // instancio el default

		 paginaDefault.establescoCanal(canal);

		 paginaDefault.agregaMapaJson("1.Bloque",namefeature);

	    }


	 @Given("Doy clic en combo cuotas {string}")
	    public void doyClicEnComboCuotas(String cuotas) {
	        cuotas=mapaParametros.get(cuotas);
	        paginaDefault.doyClicEnComboCuota(cuotas);
	    }
	 
	 @Given("Doy clic en las cuotas ssp sincuota {string}") 
	 	public void doyClicEnLasCuotasSSPsincuota(String cuotas) {
		 paginaDefault.doyClicEnLasCuotassincuota(cuotas);
	 }
	 
	 @Given("Doy clic en las cuotas ssp una {string}") 
	 	public void doyClicEnLasCuotasSSPuna(String cuotas) {
		 paginaDefault.doyClicEnLasCuotasuna(cuotas);
	 }
	 
	 @Given("Doy clic en las cuotas ssp tres {string}") 
	 	public void doyClicEnLasCuotasSSPtres(String cuotas) {
		 paginaDefault.doyClicEnLasCuotastres(cuotas);
	 }
	 
	 @Given("Doy clic en las cuotas ssp seis {string}") 
		 public void doyClicEnLasCuotasSSPseis(String cuotas) {
		 paginaDefault.doyClicEnLasCuotasseis(cuotas);
	 }
	 
	 @Given("Doy clic en las cuotas ssp doce {string}") 
	 	public void doyClicEnLasCuotasSSPdoce(String cuotas) {
		paginaDefault.doyClicEnLasCuotasdoce(cuotas);
	 }
	 
	 @Given("Doy clic en las cuotas ssp diesciocho {string}") 
	 	public void doyClicEnLasCuotasSSPdiesciocho(String cuotas) {
		 paginaDefault.doyClicEnLasCuotasdiesciocho(cuotas);
 }

}
