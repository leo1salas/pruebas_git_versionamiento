package co.com.indra.telefonicaec.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AsignarParametrosSalidaInDTO {
	
	private static final long serialVersionUID = 1L;
	
	private String id; 
	private String nombreFeature;
	private String tagsfeature;
	private Date fecha;	
	private String nombrescenario;
    private String parametrosJson;
    private boolean fallascenario=false;
    
    public String getId() {
		return id;
	}
    
	public void setId(String id) {
		this.id = id;
	}
	
    public String getNombreFeature() {
		return nombreFeature;
	}
    
	public void setNombreFeature(String nombreFeature) {
		this.nombreFeature = nombreFeature;
	}
	
	public String getTagsfeature() {
		return tagsfeature;
	}
	
	public void setTagsfeature(String tagsfeature) {
		this.tagsfeature = tagsfeature;
	}
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {		
		this.fecha = fecha;
	}
	
	public String getNombrescenario() {
		return nombrescenario;
	}
	
	public void setNombrescenario(String nombrescenario) {
		this.nombrescenario = nombrescenario;
	}
	
	public String getParametrosJson() {
		return parametrosJson;
	}
	
	public void setParametrosJson(String parametrosJson) {
		this.parametrosJson = parametrosJson;
	}
	
	public Boolean getFallascenario() {
		return fallascenario;
	}
	
	public void setFallascenario(Boolean fallascenario) {
		this.fallascenario = fallascenario;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
