package co.com.indra.telefonicaec.objects;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.com.indra.telefonicaec.constants.ConstantesBD;
import co.com.indra.telefonicaec.entities.Reporte;
import co.com.indra.telefonicaec.pages.PageDefault;
import co.com.indra.telefonicaec.utilities.OperacionesBD;
import co.com.indra.telefonicaec.utilities.PropertiesLoader;

public class ReporteController {
	
	static Logger LOGGER = LoggerFactory.getLogger(PageDefault.class);
	static OperacionesBD operacionesBD = new OperacionesBD();
	static PropertiesLoader properties = PropertiesLoader.getInstance();
	
	public static void guardarEnReporte(Reporte reporte) {
		insertarFilaReporte(reporte);
	}
	
	private static void insertarFilaReporte(Reporte reporte) {
		
		List<Object> parametros = new ArrayList<>();
		
		parametros.add(reporte.getFecha());
		parametros.add(reporte.getCanal());
		parametros.add(reporte.getNombreFeature());
		parametros.add(reporte.getNombreScenario());
		parametros.add(reporte.getTagsFeature());
		parametros.add(reporte.getMensaje());
		parametros.add(reporte.getfallaScenario());
		
		LOGGER.debug(reporte.getMensaje());
		
    	operacionesBD.abrirConexionBD();
    	
    	operacionesBD.ejecutarInsert(ConstantesBD.INSERT_REPORTE, parametros);
    		
		operacionesBD.cerrarStatement();
		
	}
}
