package co.com.indra.telefonicaec.objects;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.com.indra.telefonicaec.utilities.ActionsUtil;



public final class DatosNegocio {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DatosNegocio.class);
	private DatosNegocio() {
	    throw new IllegalStateException("Utility class");
	  }
	
	private static final HashMap<String, String> datosDelNegocio = new HashMap<String, String>();
	
	static {
		Calendar now = Calendar.getInstance();
		dataPut("thismm()", String.valueOf(now.get(Calendar.MONTH) + 1));
		dataPut("thisyyyy()", String.valueOf(now.get(Calendar.YEAR)));
		now.add(Calendar.MONTH, 1);
		dataPut("nextmm()", String.valueOf(now.get(Calendar.MONTH) + 1));
		now = Calendar.getInstance();
		dataPut("nextyyyy()", String.valueOf(now.get(Calendar.YEAR)));
		dataPut("today()", "01/01/2019");
		Date date = now.getTime();  
		now.add(Calendar.DATE,3);
		Date today3 = now.getTime();
		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");    
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd"); 
		SimpleDateFormat format3 = new SimpleDateFormat("yyyyMMddHHmm"); 
		String inActiveDate1 = null;
		String inActiveDate2 = null;
		String inActiveDate3 = null;
		dataPut("today--()", "2019-01-01");
		dataPut("today--(3)", "2019-01-01"); 
		
		try {
		    inActiveDate1 = format1.format(date);
		    inActiveDate2 = format2.format(date);
		    inActiveDate3 = format2.format(today3);
		} catch (Exception e) {
			LOGGER.error("Excepcion dato today(): ", e);
		}
		
		dataPut("today()", inActiveDate1);
		dataPut("today--()", inActiveDate2);
		dataPut("today--(3)", inActiveDate3);
		
		ActionsUtil.objetosPut("script",By.xpath("//script[contains(.,'"+inActiveDate3+"')]"));
		         
		dataPut("currentpath",System.getProperty("user.dir"));
		dataPut("numerounico()", "201901010000");
		         
		String ahoraDateTime = null;
		try {
			ahoraDateTime = format3.format(date);
			dataPut("numerounico()", ahoraDateTime);
		} catch (Exception e) {
			LOGGER.error("Excepcion dato ahoraDateTime(): ", e);
		}
	}
	
	public static String dataGet(String keyData) {
		String retorno = datosDelNegocio.get(ActionsUtil.textoMinusculasSinEspacios(keyData));
		if (retorno == null)
			retorno = keyData;
		return retorno;
	}

	public static void dataPut(String key, String value){
		datosDelNegocio.put(ActionsUtil.textoMinusculasSinEspacios(key),value);
	}
	
}
