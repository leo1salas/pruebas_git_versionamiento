package co.com.indra.telefonicaec.objects;

import org.openqa.selenium.By;

import co.com.indra.telefonicaec.utilities.ActionsUtil;

public class ObjetosPlmLogin {
	public ObjetosPlmLogin() {
	
	ActionsUtil.objetosPut("username",     			By.name("user"));
	ActionsUtil.objetosPut("password",     			By.name("pass"));
	ActionsUtil.objetosPut("iniciarsesion",     	By.id("loginButton"));
	ActionsUtil.objetosPut("login", 				By.id("login_button"));
	ActionsUtil.objetosPut("navegacion",     		By.cssSelector(".nc-search-form-wrapper"));
	ActionsUtil.objetosPut("nombredelacuentadelcliente",     	By.xpath("//*[@data-attr-id='-1']/..//input"));
//	ActionsUtil.objetosPut("boton.busqueda",     	By.xpath("//button[@class='gwt-Button button_action_id_9138004618113289307_9137007409413676336 TableCtrl-button nc-search-button parctrl-submit-form-button']")); 
	ActionsUtil.objetosPut("boton.busqueda",     	By.xpath("//button[@type='button' and contains(text(),'Búsqueda')]")); 
	ActionsUtil.objetosPut("productos",     		By.xpath("//*[@accesskey='p']"));
	ActionsUtil.objetosPut("iframeroe",     	    By.name("iframe_roe"));
//	ActionsUtil.objetosPut("check",     		    By.xpath("//input[@name='161']"));
	ActionsUtil.objetosPut("check",     		    By.xpath("//div[@class='box_selection roe-widget plans']//div[1]/table/tbody/tr/td/input"));
	ActionsUtil.objetosPut("equipos",     		    By.xpath("//*[@type='checkbox' and @tabindex='-1']/..//input"));
//	ActionsUtil.objetosPut("equipos",     		    By.xpath("//tr[@class='roe-table-row focus']//input[@name='161']"));
	ActionsUtil.objetosPut("modelo",     		    By.xpath("//input[@class='search-filter__type-input']"));
	ActionsUtil.objetosPut("agregar",     		    By.xpath("//a[@class='roe-table-cell add']"));
	ActionsUtil.objetosPut("ok",     		        By.cssSelector(".ok-button"));	
	ActionsUtil.objetosPut("precio",     		    By.xpath("//a[@class='roe-table-cell roe-hardware-area-price']//input[@class='priceRenderer']")); 
	ActionsUtil.objetosPut("precioempresas",     	By.xpath("//a[@class='roe-table-cell roe-hardware-area-price']//input[@class='priceRenderer']"));
	ActionsUtil.objetosPut("error",     		    By.cssSelector(".hw-search-warning"));
	ActionsUtil.objetosPut("serviciodebusqueda",    By.xpath("//input[@placeholder='Servicio de búsqueda']"));
	ActionsUtil.objetosPut("plan",     		        By.xpath("value=//label[@class='top-tree__input']"));
	ActionsUtil.objetosPut("planf",     		    By.xpath("//div[@class='roe-widget-content wrapper_selection']//tr[6]/td/input"));
	ActionsUtil.objetosPut("planfamiliar",     		By.name("235"));
	ActionsUtil.objetosPut("plangrupoe1plana",      By.xpath("//div[@class='roe-widget-content wrapper_selection']//tr[4]/td/input"));
	ActionsUtil.objetosPut("plangrupoe2plana",      By.xpath("//div[@class='roe-widget-content wrapper_selection']//tr[9]/td/input"));
	ActionsUtil.objetosPut("plangrupoe3plana",      By.name("232"));
	ActionsUtil.objetosPut("plangrupoe4plana",      By.xpath("//div[@class='roe-widget-content wrapper_selection']//tr[15]/td/input"));
	ActionsUtil.objetosPut("plangrup"
			+ "oe5plana",      By.name("240"));
	ActionsUtil.objetosPut("checkgrupo2plana",      By.xpath("//div[@class='roe-widget-content wrapper_selection']//tr/td/input"));
	ActionsUtil.objetosPut("prepagotodoenuno",      By.xpath("//input[@type='checkbox' and @tabindex='-1']"));
	ActionsUtil.objetosPut("planmovistar",      	By.xpath("//input[@type='checkbox' and @tabindex='-1']"));
	ActionsUtil.objetosPut("miplancontrolfamiliar", By.xpath("//input[@type='checkbox' and @tabindex='-1']"));
	ActionsUtil.objetosPut("planempresas",      	By.xpath("//input[@type='checkbox' and @tabindex='-1']"));
	ActionsUtil.objetosPut("pagoencuotas",     		By.xpath("//div[@class='roe-widget-area selected-hardware-offers']/div[@class='offering-editor-orderitem-owidget']/div[@class='offer_characteristic_wrapper']/div//input[@class='checkbox']"));
	ActionsUtil.objetosPut("combocuotas",     		By.xpath("//div[@class='characteristicValues characteristic-invalid']//div//select"));
	ActionsUtil.objetosPut("combocuotasselected",	By.xpath("//table[@class='parameters']//tbody//tr[@class='characteristic-item force_enabled']//td[@class='cell value']//div[@class='characteristicValues']//div//select"));
	ActionsUtil.objetosPut("listarenovacion",       By.xpath("//div[@class='offering-editor-hardware-gwidget']//div[@class='roe-table offering-editor-generic-cwidget']//tr[2]/td[2]/div/div/select/option[contains(text(),'Primera vez')]"));
	ActionsUtil.objetosPut("primeravez",      		By.xpath("//div[@class='offering-editor-hardware-gwidget']//tr[4]//td[2]//div[1]//div[1]//select[1]/option[contains(text(),'Primera vez')]"));
	ActionsUtil.objetosPut("plangrupo2plana",      	By.xpath("//div[@class='roe-widget-content wrapper_selection']//tr[8]/td/input"));	
	ActionsUtil.objetosPut("plangrupo2plan",      	By.xpath("//td[2]//div[1]//div[3]//div[2]//table[1]//tbody[1]//tr[3]//td[1]//input[1]"));
	ActionsUtil.objetosPut("plangrupo3plan",        By.xpath("//div[@class='roe-widget-content wrapper_selection']//tr[2]//td[1]//input[1] "));
	ActionsUtil.objetosPut("campocliente",      	By.xpath("//tr[@class='gwt-row nc-row-rolled-down']//div[contains(text(),'ID De Cuenta De Cliente')]/../..//input"));
//	ActionsUtil.objetosPut("busqueda",      		By.xpath("//*[@class='gwt-Button button_action_id_9138004618113289307_9137007409413676336 TableCtrl-button nc-search-button parctrl-submit-form-button']"));
//	ActionsUtil.objetosPut("otecel1",      			By.xpath("//a[contains(text(),'OTECEL OTECEL1')]"));
	ActionsUtil.objetosPut("otecel1",      			By.xpath("//td[2]/div[1]/div[1]/a[1]"));
	ActionsUtil.objetosPut("plangrupo4plan",      	By.xpath("//body/div[@class='wizard_layout']/div[@class='content_wrapper open_right']/div/div[@class='center_col']/div[@class='offering_col_overview']/table/tbody/tr/td[@class='offeringCenterRight']/table[@class='roe-table']/tbody/tr/td/div[@class='box_selection roe-widget plans']/div/div[@class='roe-widget-content wrapper_selection']/table[@class='roe-table']/tbody[@class='roe-table-body']/tr[2]/td[1]"));
	ActionsUtil.objetosPut("plangrupo5plan",      	By.xpath("//body/div[@class='wizard_layout']/div[@class='content_wrapper open_right']/div/div[@class='center_col']/div[@class='offering_col_overview']/table/tbody/tr/td[@class='offeringCenterRight']/table[@class='roe-table']/tbody/tr/td/div[@class='box_selection roe-widget plans']/div/div[@class='roe-widget-content wrapper_selection']/table[@class='roe-table']/tbody[@class='roe-table-body']/tr[7]/td[1]"));
	ActionsUtil.objetosPut("equipomontodecuota",      			By.xpath("//tr[@class='characteristic-item force_enabled']//td[4]//span[1]"));
	ActionsUtil.objetosPut("equipomontototalnofacturado",      	By.xpath("//div[@class='offering-editor-hardware-gwidget']//tr[4]//td[4]//span[1]"));
	ActionsUtil.objetosPut("plangrupo3planm",        By.xpath("//div[@class='offering_col_overview']//div[2]//table[1]//tbody[1]//tr[10]//td[1]//input[1]"));
	ActionsUtil.objetosPut("plangrupo2planm",        By.xpath("//td[2]//div[1]//div[3]//div[2]//table[1]//tbody[1]//tr[1]//td[1]//input[1]"));
	ActionsUtil.objetosPut("plangrupo3planp",        By.xpath("//div[@class='roe-widget-content wrapper_selection']//tr[2]//td[1]//input[1]"));
	ActionsUtil.objetosPut("tablaequipointerno", 	 By.xpath("//div[@class='roe-widget-area selected-hardware-offers']//tr[@class='characteristic-item force_enabled']/td"));
	ActionsUtil.objetosPut("inputgenerico", 		 By.xpath(".//input[@class='refsel_input']"));
	ActionsUtil.objetosPut("recalcular",             By.xpath("//div[@class='text_box roe-warning roe-warning-major server-item']//*[@class='roe_link_text'][contains(text(),'Recalcular')]"));
	ActionsUtil.objetosPut("panelizquierdaentradaorden",		 By.xpath("//div[@title='Entrada de orden']"));
	ActionsUtil.objetosPut("panelizquierdo",         			 By.xpath("//button[@class='gwt-Button left-panel-collapse-button collapsed' "  + "or @class='left-panel-collapse-button collapsed gwt-Button' " + "or @class='collapsed left-panel-collapse-button gwt-Button']"));
	ActionsUtil.objetosPut("panelizquierdoresumen",              By.xpath("//div[@title=' Resumen']"));
    ActionsUtil.objetosPut("panelizquierdoentradaorden",         By.xpath("//div[@title='Entrada de orden']"));
    ActionsUtil.objetosPut("tablainferiorcuentafacturacionpago", By.xpath("//div[@class='ui-widget-overlay-under-wrapper']//tr[@class='gwt-row nc-row-rolled-down gwt-last-row']/td"));
    ActionsUtil.objetosPut("tablasuperiorcuentafacturacionpago", By.xpath("//div[@class='ui-widget-overlay-under-wrapper']//tr[@class='gwt-row nc-row-rolled-down']/td"));
    ActionsUtil.objetosPut("textreadonly",   		By.xpath(".//div[@class=\"nc-field-text-readonly\"]"));
    ActionsUtil.objetosPut("checkgrupo4plana",      By.xpath("//div[@class='roe-widget-content wrapper_selection']//tr[2]/td/input"));
    ActionsUtil.objetosPut("checkgrupo5plana",      By.xpath("//div[@class='roe-widget-content wrapper_selection']//tr[3]/td/input"));

	//  SSPCONTADO
	ActionsUtil.objetosPut("mimovistar",      		By.xpath("//div[@class='nc-header__user-bar-image nc-header__user-bar_enter']"));
	ActionsUtil.objetosPut("campocedula",      		By.xpath("//input[@name='documentId']"));
	ActionsUtil.objetosPut("continuar",      		By.xpath("//button[@class='nc-button taButton nc-button_width_l nc-button_type_primary jsButtonSubmit']"));
	ActionsUtil.objetosPut("campocodigo",      		By.xpath("//input[@name='oneTimePassword']"));
	ActionsUtil.objetosPut("iniciarsesionssp",      By.xpath("//button[@class='nc-button taButton nc-button_type_primary nc-button_width_l jsButtonSubmitOneTimePassword']"));
	ActionsUtil.objetosPut("busquedaplan",      	By.xpath("//input[@placeholder='¿Qué plan está buscando?']"));
	ActionsUtil.objetosPut("enter",      			By.xpath("//i[@class='nc-form-search__submit-icon']"));	
	ActionsUtil.objetosPut("loquiero",      		By.xpath("//a[@class='nc-button taButton nc-button_type_primary nc-button_width_wide jsSelectPlanButton']"));
	ActionsUtil.objetosPut("comprarplanconcelular", By.xpath("//button[@class='nc-button taButton nc-button_type_primary jsBuyPlanWithDevice']"));	
	ActionsUtil.objetosPut("loquieromodelo",      	By.xpath("//a[@class='nc-button taButton nc-button_width_xs jsSelectDeviceButton nc-device__select-action-button']"));	
	ActionsUtil.objetosPut("comprar",      			By.xpath("//div[@class='nc-device-price-characteristics__section']/div[@class='nc-device-price-characteristics__plan-list jsDeviceItemPlanList']/div[1]/div[2]/button[1]"));
	ActionsUtil.objetosPut("preciocontadocuotassp", By.xpath("//div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div/span[1]"));
	ActionsUtil.objetosPut("preciocontadossp", 		By.xpath("//div[@class='nc-order-summary__content-data nc-order-summary__content-data-container']//span[@class='nc-order-summary__price-val'][contains(text(),'57,00')]"));
	//ActionsUtil.objetosPut("combocuotasspbtn",      By.xpath("//*[@id='ui-id-1-button']"));
	//ActionsUtil.objetosPut("combocuotasspbtn1cuota",       By.xpath("//*[@id='ui-id-8-button']"));
	//ActionsUtil.objetosPut("combocuotasspbtn3cuota",       By.xpath("//*[@id='ui-id-15-button']"));	
	//ActionsUtil.objetosPut("combocuotasspsincuotas",       By.xpath("//*[@id='ui-id-7']"));
	//ActionsUtil.objetosPut("combocuotassp1cuotas",         By.xpath("//*[@id='ui-id-12']"));
	//ActionsUtil.objetosPut("combocuotassp3cuotas",         By.xpath("//*[@id='ui-id-20']"));
	ActionsUtil.objetosPut("buscarmodelo",      		   By.xpath("//input[@placeholder='Buscar']"));
	ActionsUtil.objetosPut("loquieromodelorenovacion",     By.xpath("//a[@class='nc-button taButton nc-button_width_xs jsSelectDeviceButton nc-device__select-action-button']"));
	//ActionsUtil.objetosPut("loquieromodeloiphone",         By.xpath("//a[@class='nc-button taButton nc-button_width_xs jsSelectDeviceButton nc-device__select-action-button'] "));
	ActionsUtil.objetosPut("enterrenovacion",         	   By.xpath("//i[@class='nc-form-search__submit-icon']"));
	ActionsUtil.objetosPut("combocuotasspbtn",      	   By.xpath("//span[@class='ui-selectmenu-button ui-widget ui-state-default ui-corner-all']"));
	ActionsUtil.objetosPut("combocuotasspsincuotas",       By.xpath("//li[@class='ui-menu-item' and contains(text(),'Sin cuotas')]"));
	//ActionsUtil.objetosPut("combocuotasspbtn6cuotar",       By.xpath("//span[@id='ui-id-1-button']"));
	ActionsUtil.objetosPut("combocuotassp1cuotas",       	By.xpath("//li[@class='ui-menu-item' and contains(text(),'1 cuotas')]"));
	ActionsUtil.objetosPut("combocuotassp3cuotas",       	By.xpath("//li[@class='ui-menu-item' and contains(text(),'3 cuotas')]"));
	ActionsUtil.objetosPut("combocuotassp6cuotasr",       	By.xpath("//li[@class='ui-menu-item' and contains(text(),'6 cuotas')]"));
	//ActionsUtil.objetosPut("combocuotasspbtn12cuotar",      By.xpath("//span[@id='ui-id-8-button']"));
	ActionsUtil.objetosPut("combocuotassp12cuotasr",       	By.xpath("//li[@class='ui-menu-item' and contains(text(),'12 cuotas')]"));
	//ActionsUtil.objetosPut("combocuotasspbtn18cuotar",      By.xpath("//span[@id='ui-id-15-button']"));
	ActionsUtil.objetosPut("combocuotassp18cuotasr",       	By.xpath("//li[@class='ui-menu-item' and contains(text(),'18 cuotas')]"));
	//ActionsUtil.objetosPut("combocuotasspbtn6cuota",      	By.xpath("//*[@id='ui-id-1-button']"));
	//ActionsUtil.objetosPut("combocuotasspbtn12cuota",       By.xpath("//*[@id='ui-id-8-button']"));
	//ActionsUtil.objetosPut("combocuotasspbtn18cuota",       By.xpath("//*[@id='ui-id-15-button']"));
	//ActionsUtil.objetosPut("combocuotassp6cuotas",          By.xpath("//*[@id='ui-id-4']"));
	//ActionsUtil.objetosPut("combocuotassp12cuotas",         By.xpath("//*[@id='ui-id-12']"));
	//ActionsUtil.objetosPut("combocuotassp18cuotas",         By.xpath("//*[@id='ui-id-20']"));
	ActionsUtil.objetosPut("vermasplanes",                  By.xpath("//button[@class='nc-button taButton nc-button_type_link jsMorePlans']"));
	ActionsUtil.objetosPut("eliminarorden",         		By.xpath("//button[@class='nc-button taButton nc-button_type_remove jsDeletePackageButton']"));
	
	
	}
}
	