package co.com.indra.telefonicaec.pages;

import java.io.File;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.com.indra.telefonicaec.constants.ConstantesBD;
import co.com.indra.telefonicaec.constants.ConstantesGlobales;
import co.com.indra.telefonicaec.constants.ConsultasDinamicasConstans;
import co.com.indra.telefonicaec.constants.FormatConstans;
import co.com.indra.telefonicaec.dto.AsignarParametrosSalidaInDTO;
import co.com.indra.telefonicaec.entities.Reporte;
import co.com.indra.telefonicaec.enums.ProcesadoEnum;
import co.com.indra.telefonicaec.enums.TablaEnum;
import co.com.indra.telefonicaec.objects.DatosNegocio;
import co.com.indra.telefonicaec.objects.ObjetosPlmLogin;
import co.com.indra.telefonicaec.utilities.ActionsUtil;
import co.com.indra.telefonicaec.utilities.ControllerUtil;
import co.com.indra.telefonicaec.utilities.DriverManagerFactory;
import co.com.indra.telefonicaec.utilities.ExcelUtils;
import co.com.indra.telefonicaec.utilities.MacroUtils;
import co.com.indra.telefonicaec.utilities.MathUtil;
import co.com.indra.telefonicaec.utilities.OperacionesBD;
import co.com.indra.telefonicaec.utilities.PropertiesLoader;
import co.com.indra.telefonicaec.utilities.ReportesUtil;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

/**
 * Clase que define las funciones para automatizar las pruebas
 * en Netkraken
 * 
 * @author Indra
 *
 */
public class PageDefault {

	//static By objetoToAction = By.xpath("/html/body");
//	private static final Logger LOGGER = LoggerFactory.getLogger(PageDefault.class);
//	static PropertiesLoader properties = PropertiesLoader.getInstance();
//	static OperacionesBD operacionesBD = new OperacionesBD();
	List<Object> parametros = new ArrayList<Object>();
	static WebDriver driver;
	DriverManagerFactory page;
	static ExcelUtils excel;
	static MacroUtils macro;
	static String usuario;
	static String contrasenia;
	static ArrayList<Map<String, String>> litValoresCuotas = new ArrayList<Map<String, String>>();
	static ArrayList<Map<String, String>> litValoresMontoCuota = new ArrayList<Map<String, String>>();
	static private Reporte reporte = new Reporte();
	static private Boolean almacenarValoresEnMapaLuegoDeCuota = true;
	private By objetoToAction = By.xpath("/html/body");
    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerUtil.class);
    static PropertiesLoader properties = PropertiesLoader.getInstance();
//    private WebDriver driver;
//    private DriverManagerFactory page;
    private Boolean esPrepago;
    private String numeroOrdenVenta; // Extracción orden de venta al crearse
    private String SubelementOrdenVenta; // Extracción orden de venta consulta final
    private String url; // Extracción orden de venta consulta final
    private String planActual;// para cambio de plan
    private String tipoCambioPlan; // para cambio de plan
    private static final String CASO_POS_PRE = "POS_PRE";// para cambio de plan
    private static final String CASO_PRE_POS = "PRE_POS";// para cambio de plan
    /** Número de teléfono asociado al proceso.*/
    private String numeroTelefono; 
    /** Número de cuenta facturación asociado al proceso.*/
    /**Categoria asociado al proceso.*/
    private String categoria;
    private String tipo_cuenta;
    private String nombre;
    private String ciclo;
    private String cuentaFacturacion;
    /** Nombre cliente asociado al proceso.*/
    private String nombreCliente;
    private String parametrosJson; //parametros con lso que fue ejecutado el flujo
    private static String fechaSegundosMilisegundos;
    private static List<String> listaIdsProcesados;
    private String estadoOrden;
    private String tipoProceso;
    private String procesadoCuando;
    private String bdClientePrepago;
    private String bdClientePospago;
    private boolean indicadorInAdvance=false;
    private String bdClientePospagoInAdvance;
    private String bdNumeroCuentaCliente;
    private String bdPlan;
    private String urlVisionGeneral;
    private String urlActualizada;
    private Date fechaCreacion;
    
    
    
    private String nombrefeature;
    /**
     * interno o externo
     */
    private String bdTipoEquipo;
    static OperacionesBD operacionesBD = new OperacionesBD();

    private String numeroAntiguo; // Extracción número de telefono
    private String numeroNuevo; // Extracción número de telefono nuevo

    private String simCardInicial = "";

    private String nombreDelEquipoInterno = "";
    private String imeiDelEquipoIterno = "";
    private String nombreDelEquipoRenovacion = "";
    private String imeiDelEquipoRenovacion = "";

    private String simcardVieja;
    private String simcardNueva;
    private String sloActivados = "";
    private String nombrePlan = "";

    private String planNuevo = "";
    private String bdNumeroDocumento;
    private String bodegaDefecto;
    private String fechaEnvio;
    private String canalDistribucion;
    private String confirmadoPor;
    private String urlError;
	
    public PageDefault() {
		if (ActionsUtil.objetosIsEmpty()) {
			LOGGER.info("Inicialización de objetos");
			new ObjetosPlmLogin();			
		}
	}

	public By getObjetoToAction() {
		return objetoToAction;
	}

	public void setObjetoToAction(By objetoToCliked) {
		objetoToAction = objetoToCliked;
	}

	@Before
	public void beforeScenario(Scenario scenario) {
		reporte.setNombreFeature(scenario.getUri().substring(scenario.getUri().lastIndexOf("/")+1));
		reporte.setNombreScenario(scenario.getName());
		reporte.setTagsFeature(Arrays.toString(scenario.getSourceTagNames().toArray()));
		reporte.setfallaScenario("false");
		
		
		String url = properties.getProperty("nc.xls.url");
		String nombre = properties.getProperty("nc.xls.nombre");
		page = new DriverManagerFactory();
		driver = DriverManagerFactory.getDriver();
		
		
		if(fechaSegundosMilisegundos==null){
	        Calendar fechaActual= Calendar.getInstance();
	        SimpleDateFormat format= new SimpleDateFormat(FormatConstans.FORMATO_FECHA_HORA_PEGADO);
	        fechaSegundosMilisegundos=format.format(fechaActual.getTime());
	        fechaSegundosMilisegundos=fechaSegundosMilisegundos.replace(":","");
	        ReportesUtil.setFechaSegundosMiligesundos(fechaSegundosMilisegundos);
	        ActionsUtil.setFechaSegundosMiligesundos(fechaSegundosMilisegundos);
	       
        }
		reporte.setFecha(fechaSegundosMilisegundos);
		
		ControllerUtil.fechaCreacion=Calendar.getInstance().getTime();
		
		if(ControllerUtil.listaIdsProcesados==null){
			ControllerUtil.listaIdsProcesados= new ArrayList<>();
        }		
		
		if(ControllerUtil.mapaJson==null){
			ControllerUtil.mapaJson= new HashMap<>();			
        }	
		
		if (macro == null) {
			// leo macro
			String urlmacro = properties.getProperty("nc.macro.url");
			String nombremacro = properties.getProperty("nc.macro.nombre");
			cargarMacro(urlmacro, nombremacro);
			// cambio valores de macro a xls nuestro
			modificarXlsPorMacro(url);
		}
		if (excel == null) {
			cargarXls(url, nombre);
		}
	}

	private void modificarXlsPorMacro(String url) {
		List<String> nombresReales = macro.getValuesByRowNum(8, 0);
		List<String> nombresSsp = macro.getValuesByRowNum(8, 1);
		Map<String, String> mapaEquipos = new HashMap<String, String>();
		for (int i = 0; i < nombresSsp.size(); i++) {
			mapaEquipos.put(nombresSsp.get(i), nombresReales.get(i));
		}
		macro.modificarXlsx(new File(url), mapaEquipos);
	}

	private void cargarMacro(String urlmacro, String nombremacro) {
		try {
			macro = new MacroUtils(urlmacro, nombremacro);
		} catch (Exception e) {
			LOGGER.error("Error leyendo el archivo xls: ", e);
		}

	}

	@After
	public void afterScenario() {
		// ActionsUtil.takeSnapShot(driver, "evidence.png");
		// EmailUtil.sendImage(System.getProperty("user.dir") +
		// "\\target\\evidences\\" + "evidence.png");
		ActionsUtil.obtenerMensajeErrorBandeja(driver,Boolean.FALSE);
		//cerrar session
		ActionsUtil.clickLinkTexto(driver, "Cerrar sesión");
		//cerrar el driver web
		finalize();
	}

	public void finalize() {
		if (page != null) {
			page.finalize();
			
		}else {
			driver.quit();
		}
	}

	public void sharedObjet(String opcion) {
		String nombreObjeto = (ActionsUtil.textoMinusculasSinEspacios(opcion));
		By byObjeto = ActionsUtil.getObjeto(nombreObjeto);
		setObjetoToAction(byObjeto);
	}

	public void irA(String url) {
		String urlActualizada = ActionsUtil.updateUrlWithBaseUrlIfDefined(url);
		ActionsUtil.goToWebSide(driver, urlActualizada);
	}

	public void irA(String url, String puerto) {
		url = properties.getProperty(url);
		puerto = properties.getProperty(puerto);
		String urlActualizada = ActionsUtil.updateUrlWithBaseUrlIfDefined(url + ":" + puerto);
		ActionsUtil.goToWebSide(driver, urlActualizada);
		agregaMapaJson("5.Url", urlActualizada);
	}

	public void validarUrl(String urlIp, String puerto, String path) {
		urlIp = properties.getProperty(urlIp);
		puerto = properties.getProperty(puerto);
		path = properties.getProperty(path);
		String urlActualizada = ActionsUtil.updateUrlWithBaseUrlIfDefined(urlIp + ":" + puerto + path);
		String urlActual = driver.getCurrentUrl();
		if (!urlActualizada.equals(urlActual)) {
			ActionsUtil.goToWebSide(driver, urlActualizada);
		}

	}

	public void irA(String url, String puerto, String path) {
		url = properties.getProperty(url);
		puerto = properties.getProperty(puerto);
		path = properties.getProperty(path);
		String urlActualizada = ActionsUtil.updateUrlWithBaseUrlIfDefined(url + ":" + puerto + path);
		ActionsUtil.goToWebSide(driver, urlActualizada);
	}

	public void cambiarFrameInterno() {
		ActionsUtil.switchFrameRoe(driver);

	}

	public void esperarHabilitarServicioCaracteristica() {
		ActionsUtil.esperarHabilitarServicioCaracteristica(driver);
		ActionsUtil.cargandoFrameInterno(driver);
	}
	
	public void esperarHabilitarServiciosyCaracteristicas() {
		ActionsUtil.esperarHabilitarServicioCaracteristica(driver);
	}
	

	public void cambiarIframe(String posicion) {

		int indexTab;
		try {
			indexTab = Integer.parseInt(posicion);
		} catch (Exception e) {
			indexTab = 0;
		}
		if (indexTab == 0) {
			switch (ActionsUtil.textoMinusculasSinEspacios(posicion)) {
			case "principal":
				cambiarIframePrincipal();
				return;
			case "cero":
				indexTab = 0;
				break;
			case "uno":
				indexTab = 1;
				break;
			case "dos":
				indexTab = 2;
				break;
			default:
				indexTab = 0;
				break;
			}
		}
		if (indexTab != 0) {
			ActionsUtil.switchFrame(driver, indexTab);
		} else {
			sharedObjet(posicion);
			By element = getObjetoToAction();
			ActionsUtil.switchFrame(driver, element);
		}
	}

	public void cambiarIframePrincipal() {
		driver.switchTo().parentFrame();
	}

	public void ingresarTexto(String objeto, String texto) {
		sharedObjet(objeto);
		ActionsUtil.setTextField(driver, getObjetoToAction(), texto);
	}

	public void ingresarTextoProperties(String objeto, String texto) {
		texto = properties.getProperty(texto);
		sharedObjet(objeto);
		ActionsUtil.setTextField(driver, getObjetoToAction(), texto);
		//agregaMapaJson("4.Plan", texto);
		
	}

	/**
	 * Metodo de ejemplo para consultar una propiedad suelta de BD por nombre
	 * 
	 * @param texto
	 * @return
	 */
	public String consultarPropiedadBD(String texto) {
		if (texto != null) {
			operacionesBD.abrirConexionBD();
			parametros.add(texto);
			ResultSet rs = operacionesBD.ejecutarSelect(ConstantesBD.SELECT_PROPIEDAD_POR_NOMBRE, parametros);
			try {
				if (rs.next()) {
					texto = rs.getString(1);
				}
			} catch (SQLException e) {
				texto = null;
			} finally {
				if (rs != null)
					try {
						rs.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
			}
			operacionesBD.cerrarStatement();
			// operacionesBD.cerrarConexion();
		}
		return texto;
	}

	public void clic(String objeto) {
		sharedObjet(objeto);
		ActionsUtil.clic(driver, getObjetoToAction());

	}

	public void clicForzado(String objeto) {
		sharedObjet(objeto);
		ActionsUtil.ejecutarScript(driver, "arguments[0].click();", getObjetoToAction());
	}

	public void obtenerGuardarString(String objeto, String atributo, String datokey) {
		sharedObjet(objeto);
		String datoValue = ActionsUtil.getAttribute(driver, getObjetoToAction(), atributo);
		if (datoValue.isEmpty()) {
			datoValue = ActionsUtil.getTextAttribute(driver, getObjetoToAction());
		}
		DatosNegocio.dataPut(datokey, datoValue);
		LOGGER.info("Se guardo el dato: \"" + datokey + "\" con el valor: \"" + datoValue + "\"");
	}

	public Object getValuesForGivenKey(String jsonArrayStr, String key) {
		JSONArray jsonArray = new JSONArray(jsonArrayStr);
		Object x = jsonArray.query("/0/b");
		return x;
	}

	public void validarValorCampo(String campo, String valor) {
		Boolean resp = false;
		LOGGER.info("PageDefault compara campo: " + campo + " valor: " + valor);
		valor = properties.getProperty(valor);
		sharedObjet(campo);
		resp = ActionsUtil.compareAtributo(driver, getObjetoToAction(), "value", valor);
	}

	public void validoEstiloCampo(String objeto, String estilo) {
		estilo = properties.getProperty(estilo);
		sharedObjet(objeto);
		ActionsUtil.validarEstilo(driver, getObjetoToAction(), estilo);
		
		guardarMensajeEnInforme(reporte, true);

	}

	public void esperarDisplayed(String objeto) {
		sharedObjet(objeto);
		ActionsUtil.existsElement(driver, getObjetoToAction());
	}
	/// CAMBIOS XLS

	public String valorXls(String iterador, String propiedad) {
		return obtenerValorXls(iterador, properties.getProperty(propiedad));
	}

	public void ingresarValorXls(String objeto, String texto, String propiedad) {
		texto = obtenerValorXls(texto, properties.getProperty(propiedad)).toLowerCase();
		sharedObjet(objeto);
		ActionsUtil.setTextField(driver, getObjetoToAction(), texto);
	}
	
	public void ingresarValorXls(String objeto, String texto, String propiedad,String key) {
		
		texto = obtenerValorXls(texto, properties.getProperty(propiedad)).toLowerCase();
		sharedObjet(objeto);
		
		agregaMapaJson(key, texto);
		
		ActionsUtil.setTextField(driver, getObjetoToAction(), texto);
		
	}

	public void validarValorCampoXls(String campo, String valor, String propiedad, String validaNA) {
		String columna = properties.getProperty(propiedad);
		Boolean resp = false;
		valor = obtenerValorXls(valor, columna);
		if (validaNA != null && !validaNA.isEmpty() && validaNA.equals("1")) {
			if (valor == null || valor.isEmpty() || valor.equalsIgnoreCase("NA")) {
				finalize();
			}
		}
		try {
			double x = Double.parseDouble(valor);
			DecimalFormat df = new DecimalFormat("#,###.00");
			String formatted = df.format(x);
			if (formatted != null) {
				valor = formatted;
			}
			/////////// Depurar!!!!!!!!!!!
		} catch (Exception ex) {
		}
		sharedObjet(campo);
		resp=ActionsUtil.compareAtributo(driver, getObjetoToAction(), "value", valor);
				
		guardarMensajeEnInforme(reporte, resp);
	}

	public String leerValorCelda(int rowIndex, int colIndex) {
		if (excel != null) {
			try {
				return excel.getValue(rowIndex, colIndex);
			} catch (Exception e) {
				LOGGER.error("Error leyendo el archivo xls: ", e);
				return null;
			}
		} else {
			LOGGER.error("No se ha cargado ningun xls para leer.");
			return null;
		}
	}

	public Boolean vetificarExisteRegistro(String posicion, String propiedad) {
		Boolean existe = Boolean.FALSE;
		if (excel != null) {
			try {
				propiedad = properties.getProperty(propiedad);
				String valor1 = excel.getValue(Integer.parseInt(posicion), Integer.parseInt(propiedad));
				existe = Boolean.TRUE;
				if (valor1==null || valor1.isEmpty()) {
					existe = Boolean.FALSE;
					LOGGER.error("Error leyendo el archivo xls");
				}
			} catch (Exception e) {
				existe = Boolean.FALSE;
				LOGGER.error("Error leyendo el archivo xls: ", e);
			}
		} else {
			LOGGER.error("No se ha cargado ningun xls para leer");
			existe = Boolean.FALSE;
		}
		return existe;
	}

	private String obtenerValorXls(String a, String b) {
		return leerValorCelda(Integer.parseInt(a), Integer.parseInt(b));
	}

	private void cargarXls(String url, String nombre) {
		try {
			excel = new ExcelUtils(url, nombre);
		} catch (Exception e) {
			LOGGER.error("Error leyendo el archivo xls: ", e);
		}
	}

	public void ingresarValorXlsNa(String objeto, String texto, String propiedad, String valorNa) {
		String pos = texto;
		String grupo;
		texto = obtenerValorXls(pos, properties.getProperty(propiedad));
		String valorEquipo = obtenerValorXls(pos, properties.getProperty(valorNa));
		agregaMapaJson("2.Equipo", texto);
		grupo = obtenerValorXls(ConstantesGlobales.CABECERA_MEF_GRUPO, properties.getProperty(propiedad));
		agregaMapaJson("3.Grupo", grupo);
		
		if (valorEquipo != null && !valorEquipo.equalsIgnoreCase("NA")) {
			agregaMapaJson("Mensaje","se esta validando no precios y se obtuvo un precio del MEF");
			reporte.setfallaScenario("true");
			guardarMensajeEnInforme(reporte, true);
			finalize();
		} 
		
		sharedObjet(objeto);
		ActionsUtil.setTextField(driver, getObjetoToAction(), texto);			
			
		
	}

	public void ingresoValorCombo(String objeto, String texto) {
		almacenarValoresEnMapaLuegoDeCuota = true;
		sharedObjet(objeto);
		//agregaMapaJson("Cuota",texto);	
		ControllerUtil.Numcuota=texto;
		try {			
			ActionsUtil.selectText(driver, getObjetoToAction(), texto);
		}catch(NoSuchElementException e) {
			almacenarValoresEnMapaLuegoDeCuota = false;	
			agregaMapaJson("Error_Cuota" + texto+"_NoDisponible",properties.getProperty("netkraker.equipo.numerocuota.nodisponible"));
			ActionsUtil.takeScreeShot(driver, "ImagenError_cuota_"+texto+".");
		}

	}
	
	public void ingresoValorCombossp(String objeto, String texto) {
		almacenarValoresEnMapaLuegoDeCuota = true;
		sharedObjet(objeto);
		try {
			WebElement combossp = driver.findElement(getObjetoToAction()); 
			ActionsUtil.selectText(driver, getObjetoToAction(), texto);
		}catch(NoSuchElementException e) {
			almacenarValoresEnMapaLuegoDeCuota = false;
		}

	}

	public void validarModificado(String row, String propiedadModificado, String columna) {
		// se lee el valor del properties para sacar la columna
		columna = properties.getProperty(columna);
		propiedadModificado = properties.getProperty(propiedadModificado);
		String valorXls = obtenerValorXls(row, columna);
		if (valorXls == null || valorXls.isEmpty() || !valorXls.equalsIgnoreCase(propiedadModificado)) {
			finalize();
		}
	}

	public void esperarXTiempo(int tiempo) {
		ActionsUtil.sleepSeconds(tiempo);
	}

	public void clicLinkTexto(String texto) {
		texto = properties.getProperty(texto);
		By link = By.xpath("//a[contains(text(), '" + texto + "')]");
		ActionsUtil.esperarPoderClicBy(driver, link);
		ActionsUtil.clic(driver, link);
	}

	public void clicBotonTexto(String texto) {
		texto = properties.getProperty(texto);
		WebDriverWait wait = new WebDriverWait(driver, 150);
		By boton = By.xpath("//button[text()='" + texto + "']");
		ActionsUtil.esperarPoderClicBy(driver, boton);
		ActionsUtil.clic(driver, boton);
	}

	public void validarCargando() {
		ActionsUtil.cargandoFrameInterno(driver);
	}

	public void esperarCargaPantalla() {
		System.out.println("Inicia espera");
		By cargando = By.xpath("//span[@class=\"nc-loading-text\"]");
		ActionsUtil.invisibleListaBy(driver, cargando);
		System.out.println("Fin espera");
	}
		
	/**
	 * Funcion que almacena los valores recibidos en un map para posteriormente comprarlos
	 * 
	 * @param campo : Campo de la pagina NetCraker
	 * @param valor : Fila del archivo xls MEF
	 * @param propiedad : Columna del archivo xls MEF
	 * @param validaNA : 1 si valida que el resultado de valor sea null o vacio o NA
	 */
	public void almacenarValorCampoXls(String campo, String valor, String propiedad, String validaNA, String cuota ) {
		
		//Se debe ejecutar la función si se pudo seleccionar un valor en el combo
		if(almacenarValoresEnMapaLuegoDeCuota) {
			String columna = properties.getProperty(propiedad);
			String grupo;
			Map<String, String> mapa = new HashMap<String, String>();
			
			
			valor = obtenerValorXls(valor, columna);
			if (validaNA != null && !validaNA.isEmpty() && validaNA.equals("1")) {
				if (valor == null || valor.isEmpty() || valor.equalsIgnoreCase("NA")) {
					finalize();
				}
			}
			
			valor = MathUtil.formatStringToDoubleFormat(valor);
			
			sharedObjet(campo);
			String val1 = ActionsUtil.getAttribute(driver, getObjetoToAction(), "value");
					
			mapa.put(ConstantesGlobales.OBTENIDO, val1);
			mapa.put(ConstantesGlobales.ESPERADO, valor);
			mapa.put(ConstantesGlobales.NUM_CUOTA, cuota);	
			litValoresCuotas.add(mapa);
			
			grupo = obtenerValorXls(ConstantesGlobales.CABECERA_MEF_GRUPO, columna);
			agregaMapaJson("3.Grupo", grupo);
		}
	}
				
	/**
	 * Funcion que toma los valores en xpath de netkraken para calcular el valor de un equipo
	 * segun el numero de cuotas y almacena los valores en un map para posteriormente comprarlos
	 * 
	 * @param precio : Precio total del equipo en Netkraken
	 * @param cuotas : Numero de cuotas en que se va a pagar el equipo
	 * @param montoCuotas : El valor de la cuota, tomado de Netkraken
	 * @param montoTotal : Precio total del equipo no facturado, tomado de Netkraken
	 */
	public void almacenarCamposMontoCuotaDelModelo(String precio, String cuotas, String montoCuotas, String montoTotal) {
		Map<String, String> mapa = new HashMap<>();
		//Se debe ejecutar la función si se pudo seleccionar un valor en el combo
		if(almacenarValoresEnMapaLuegoDeCuota) {
			sharedObjet(precio);
			String precioReal = ActionsUtil.getAttribute(driver, getObjetoToAction(), "value");
			
			sharedObjet(montoCuotas);
			String montoCuotasReal = ActionsUtil.getText(driver, getObjetoToAction());
					
			sharedObjet(montoTotal);
			String montoTotalReal = ActionsUtil.getText(driver, getObjetoToAction());
					
			Double precioSobreCuotas = MathUtil.divide2DoubleNumbers(precioReal,cuotas);
			String precioSobreCuotasStr = MathUtil.formatStringToDoubleFormat(precioSobreCuotas.toString());
					
			mapa.put(ConstantesGlobales.VALOR_MODELO, precioReal);
			mapa.put(ConstantesGlobales.MONTO_TOTAL_NO_FACTURADO, montoTotalReal.replace("$",""));
			
			mapa.put(ConstantesGlobales.MONTO_CUOTA_MODELO_CALCULADO, precioSobreCuotasStr.toString());
			mapa.put(ConstantesGlobales.MONTO_CUOTA_MODELO, montoCuotasReal.replace("$", ""));
			
			mapa.put(ConstantesGlobales.NUM_CUOTA, cuotas);
			
			litValoresMontoCuota.add(mapa);
		}
	}
	
	public void almacenarCamposValidarPrecioConCuotaSSP(String rowxls, String propiedad, String montoCuota, String valorCuota) {
		//Se debe ejecutar la función si se pudo seleccionar un valor en el combo
		if(almacenarValoresEnMapaLuegoDeCuota) {
			Map<String, String> mapa = new HashMap<>();
			
			String columna = properties.getProperty(propiedad);
			String valor = obtenerValorXls(rowxls, columna);
			
			if (valor != null) {
				Double precioSobreCuotas = MathUtil.divide2DoubleNumbers(valor,montoCuota);
				String precioSobreCuotasStr = MathUtil.formatStringToDoubleFormat(precioSobreCuotas.toString());
				
				sharedObjet(valorCuota);
				String valorCuotaReal = ActionsUtil.getText(driver, getObjetoToAction());
				
				mapa.put(ConstantesGlobales.OBTENIDO, precioSobreCuotasStr);
				mapa.put(ConstantesGlobales.ESPERADO, valorCuotaReal);		
				litValoresCuotas.add(mapa);
			}else {
				System.out.println(properties.getProperty("netkraker.propiedad.noencontrada") + propiedad);			
				//ActionsUtil.guardarMensajeEnInforme(reporte, properties.getProperty("netkraker.propiedad.noencontrada") + propiedad, false);
			}
		}
	}
	

	public void validarValoresCuotas() {
		ControllerUtil.validarValoresCuotas(reporte, litValoresCuotas, litValoresMontoCuota);		
	}

	
	 private void llenarMapa(ResultSet rs, Map<String, String> mapaEntradaBD) {
	        try {
	            
	            ResultSetMetaData rsmd = rs.getMetaData();
	            String nombreColumna=null;
	            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
	                nombreColumna=rsmd.getColumnName(i);
	                mapaEntradaBD.put(nombreColumna, rs.getString(nombreColumna));
	             }
	        } catch (SQLException e) {
	            LOGGER.error("Se presento un error al llenar el mapa", e);
	        }

	    }
	    
	    
	    /**
	     * Metodo encargado de finalizar el navegador
	     */
	    public void cerrarNavegador() {
	      driver.quit();
	    }
	    	
		/**
	     * Método para cargar los valores iniciales de ejecucion de feature
	     * @param tabla
	     * @param id
	    */
	    public Map<String, String> consultarTablaEntradaFlujo(String tabla, String id, String nombreFeature, String nombreFlujo, String nombreTag) {
	        Map<String, String> mapaEntradaBD= new HashMap<>();
	        operacionesBD.abrirConexionBD();
	        String consulta=null;
	        List<Object> params= new ArrayList<>();
	        params.add(Long.parseLong(id));
	        if(TablaEnum.OTC_T_ALTAS_LINEAS.name().equals(tabla)){
	            consulta=ConsultasDinamicasConstans.SELECT_OTC_T_ALTAS_LINEAS_ID;
	            
	            ResultSet rs = operacionesBD.ejecutarConsulta2(consulta, params);
		        try {
		             if (rs != null && rs.next()) {
		                 llenarMapa(rs,mapaEntradaBD);
		                }
		             else{
		                 operacionesBD.cerrarStatement();
		                 cerrarNavegador();
		             }
		        } catch (SQLException e) {
		            LOGGER.error("Se presento un error al consultar las propiedades", e);
		        } finally {
		            if (rs != null)
		                try {
		                    rs.close();
		                } catch (SQLException e) {
		                    LOGGER.error("Error cerrando el resultSet al init", e);
		                }
		        }
		        operacionesBD.cerrarStatement();
		        
	        }else if(TablaEnum.OTC_T_PARAMETRO.name().equals(tabla)){
	            //consulta=ConsultasDinamicasConstans.SELECT_OTC_T_ACTIVACION_SLO_ID;
	        	mapaEntradaBD.put("feature", nombreFeature);
	            
	        }
	        
	        String procesado=mapaEntradaBD.get("procesado");
	        properties.setProperty("isProcesado", "false");
	        if(procesado!=null && procesado.equalsIgnoreCase(ProcesadoEnum.PROCESADO.name())){
	            LOGGER.info("Se finaliza el flujo ya que el registro ya fue procesado");
	            properties.setProperty("isProcesado", "true");
	            cerrarNavegador();
	        }
	        
	        //se guarda el resultado
	        guardarResultadoFlujo(nombreTag,nombreFeature,nombreFlujo, mapaEntradaBD.get("id"));
	        
	        return mapaEntradaBD;
	        
	    }
	    
	    private void buscarPrimerResultado(boolean hacerClic) {
	        // TODO mover a la clase ObjetosPlmLogin.java
	        ActionsUtil.sleepSeconds(1);
	        By primerResultado = By.xpath("//*[@id=\"nc_refsel_list_row_0\"]/*[@class=\"refsel_name\"]");
	        WebElement resultadoBusqueda = driver.findElement(primerResultado);
	        WebDriverWait wait = new WebDriverWait(driver, 30);
	        ActionsUtil.espererarDesparecerCargando(driver);
	        wait.until(ExpectedConditions.elementToBeClickable(resultadoBusqueda));
	        if (hacerClic) {
	            resultadoBusqueda.click();
	        }
	    }
	    
	    private void equipoInterno(String nombreEquipo, Boolean selecionarCantidadCopias) {
	        WebElement inputModelo = driver.findElement(By.xpath("//div[@class=\"search-filter__type\"]/input"));
	        inputModelo.clear();
	        inputModelo.sendKeys(nombreEquipo);
	        WebElement botonSiguiente = driver
	                .findElement(By.xpath("//*[@id=\"Celulares\"]/div/div[4]/table/tbody/tr/td[4]/img"));

	        String parar = null;
	        do {
	            parar = botonSiguiente.getAttribute("aria-disabled");
	            List<WebElement> filas = driver.findElements(By.xpath("//*[@id=\"Celulares\"]/div/div"));
	            for (WebElement selenideElement : filas) {
	                if (selenideElement.isDisplayed() && selenideElement.getText().contains(nombreEquipo)) {
	                    List<WebElement> filas2 = selenideElement.findElements(By.xpath("./div/table/tbody/tr"));
	                    for (WebElement selenideElement1 : filas2) {
	                        if (selenideElement1.isDisplayed() && selenideElement1.getText().contains(nombreEquipo)) {
	                            (selenideElement1.findElement(By.xpath("./td[3]/a"))).click();
	                            ActionsUtil.cargandoFrameInterno(driver);
	                            if (selecionarCantidadCopias) {
	                                By botonOk = By.xpath("//span[@class='ui-button-text'][contains(text(),'OK')]");
	                                ActionsUtil.esperarPoderClicBy(driver, botonOk);
	                                driver.findElement(botonOk).click();
	                            }
	                            return;
	                        }
	                    }
	                }
	            }
	            botonSiguiente.click();
	        } while ("false".equals(parar));
	    }
	    
	    public void identificarPlanActual() {
	        esPrepago = ActionsUtil.validarPlanPrepago(ActionsUtil.planActual(driver));
	        nombrePlan = ActionsUtil.nombrePlanActual(driver);
	    }
	    
	    /**
	     * @param contrato
	     * @param numeroSim
	     * @param numeroTelefono
	     */
	    public void selecionarContratoICCDNumeroTelefono(String contrato, String numeroSim, String numeroTelefono) {

	        By filas = By.xpath("//tr[@class='characteristic-item force_enabled']");
	        ActionsUtil.esperarVisibleListaBySegundo(driver, filas, 120);

	        List<WebElement> filaElemento = driver.findElements(filas);
	        String duracionContrato = properties.getProperty("texto.duracion.contrato");
	        String sim = properties.getProperty("texto.iccid");
	        String telefono = properties.getProperty("texto.numero.telefono");

	        for (WebElement webElement : filaElemento) {
	            if (!esPrepago) {
	                if (webElement.getText().contains(duracionContrato)) {
	                    Select selectContrato = new Select(webElement.findElement(By.xpath(".//select")));
	                    selectContrato.selectByVisibleText(contrato);
	                }
	            }
	            if (webElement.getText().contains(sim)) {
	                WebElement input = webElement.findElement(By.xpath(".//input[@class='refsel_input']"));
	                input.clear();
	                input.sendKeys(numeroSim);
	                buscarPrimerResultado(true);
	                ActionsUtil.cargandoFrameInterno(driver);
	                simCardInicial=input.getAttribute("value");
	            }
	            if (webElement.getText().contains(telefono)) {
	                WebElement input = webElement.findElement(By.xpath(".//input[@class='refsel_input']"));
	                if(numeroTelefono==null || numeroTelefono.isEmpty()) {
	                    numeroTelefono=input.getAttribute("value");
	                }else {
	                    input.clear();
	                    ActionsUtil.cargandoFrameInterno(driver);
	                    input.sendKeys(numeroTelefono);
	                    buscarPrimerResultado(true);
	                    ActionsUtil.cargandoFrameInterno(driver);
	                }
	                this.numeroTelefono=numeroTelefono;
	            }
	        }
	        boolean existeICCD = false;
	        boolean existeNumeroTelefono = false;
	        boolean existeDuracionContrato = false;

	    }
	    
	    public void cambiarFramePrincipal() {
	        ActionsUtil.switchToDefaultContent(driver);

	    }
	    
/*	    public void cambiarFrameInterno() {
	        ActionsUtil.switchFrameRoe(driver);

	    } */
	    
	    private String obtenerImeiExterno() {
	        cambiarFramePrincipal();
	        String urlGeneradorImei = properties.getProperty("paht.generador.imei");
	        ActionsUtil.abriNuevaPestanya(driver, urlGeneradorImei);
	        
	        By botonBy = By.xpath("//input[@type='button']");
	        ActionsUtil.clic(driver, botonBy);
	        By textBy = By.xpath("//input[@id='imei_num']");
	        WebElement textElemento = driver.findElement(textBy);
	        String imeiValor=textElemento.getAttribute("value");
	        ActionsUtil.cerrarUltimaRegrearPrimeraPestanya(driver);
	        cambiarFrameInterno();
	        
	        return imeiValor;
	    }
	    
	    private void validarIMEIExterno(String imei) {
	        WebElement label;
	        List<WebElement> filas = driver.findElements(By.xpath("//div[@class='roe-widget-area selected-hardware-offers']"
	                + "//*[@class='characteristic-item force_enabled']"));
	        String texto = properties.getProperty("texto.imei.equipo.externo");
	        for (WebElement selenideElement : filas) {
	            label = selenideElement.findElement(By.xpath(".//span"));
	            if (texto.equals(label.getText())) {
	                WebElement input = selenideElement.findElement(By.xpath(".//input"));
	                input.clear();
	                if(imei==null || imei.isEmpty()) {
	                    imei=obtenerImeiExterno();
	                }
	                imeiDelEquipoIterno=imei;
	                input.sendKeys(imei);
	                WebElement boton = selenideElement
	                        .findElement(By.xpath(".//a[@class='bt_bt tfnecu-validate-imei-button']"));
	                boton.click();
	                ActionsUtil.cargandoFrameInterno(driver);
	                break;
	            }
	        }
	    }
	    
	    private String obtenerImeiInterno(String equipo) throws Exception {
	        cambiarFramePrincipal();
	        WebElement label = ActionsUtil.obtenerElementoSpanTexto(driver,"Bodegas");
	        String linkBodegas=ActionsUtil.obteberLinkElemento(ActionsUtil.parent(label));
	        ActionsUtil.abriNuevaPestanya(driver, linkBodegas);
	        
	        String nombreBodegaCentral = properties.getProperty("nombre.bodega.central");
	        String nombreBodegaActual = properties.getProperty("nombre.bodega.actual");
	        
	        By modalInputBy=By.xpath("//div[@class='ui-dialog-content ui-widget-content']//input[@type='text']");

	        ActionsUtil.filtrarIrBodega(driver, nombreBodegaActual);
	        String imeiInterno=ActionsUtil.filtrarEquipoObtenerImeiInterno(driver,modalInputBy,equipo);
	        
	        if(imeiInterno==null) {
	            driver.get(linkBodegas);
	            ActionsUtil.filtrarIrBodega(driver, nombreBodegaCentral);
	            imeiInterno=ActionsUtil.filtarEquipoObtenerImeiMoverEquipo(driver,modalInputBy,equipo,nombreBodegaActual);
	        }
	        
	        ActionsUtil.cerrarUltimaRegrearPrimeraPestanya(driver);
	        cambiarFrameInterno();
	        return imeiInterno;
	    }
	    
	    public void validarIMEIInterno(String nombreEquipo, String imei) throws Exception {
	        String textoIMEI = properties.getProperty("texto.imei");
	        WebElement input = obtenerElementoEspecificoTabla("tablaequipointerno","inputgenerico",textoIMEI+"*");
	        
	        if (imei == null || imei.isEmpty()) {
	            imei = obtenerImeiInterno(nombreEquipo);
	        }
	        input.clear();
	        input.sendKeys(imei);
	        ActionsUtil.cargandoFrameInterno(driver);
	        buscarPrimerResultado(Boolean.TRUE);
	        ActionsUtil.cargandoFrameInterno(driver);
	        imeiDelEquipoIterno = imei;

	    }
	    
	    public void validarImei(String imei) throws Exception {
	    	String textoIMEI = properties.getProperty("texto.imei");
	    	String nombreEquipo = devuelveValorMapaJson("2.Equipo");
	    	
	    	if (imei == null || imei.isEmpty()) {
	            textoIMEI = obtenerImeiInterno(nombreEquipo);
	        
	    	}
	    	
	    	WebElement input = obtenerElementoEspecificoTabla("tablaequipointerno","inputgenerico",textoIMEI+"*");
	        input.clear();
	        input.sendKeys(imei);
	        ActionsUtil.cargandoFrameInterno(driver);
	        buscarPrimerResultado(Boolean.TRUE);
	        ActionsUtil.cargandoFrameInterno(driver);
	    }
	    
/*	    public void setObjetoToAction(By objetoToCliked) {
	        objetoToAction = objetoToCliked;
	    } */
	    
/*	    public void sharedObjet(String opcion) {
	        String nombreObjeto = (ActionsUtil.textoMinusculasSinEspacios(opcion));
	        By byObjeto = ActionsUtil.getObjeto(nombreObjeto);
	        setObjetoToAction(byObjeto);
	    } */
	    
	    private void localizarPagoCuotasInterno(String coutas) {
	        String texto = properties.getProperty("texto.pago.cuotas");
	        WebElement checkCuotas = driver.findElement(By.xpath(
	                "//div[@class=\"roe-widget-area selected-hardware-offers\"]//table[@class=\"parameters\"]//span[contains(text(),'"
	                        + texto + "')]"));
	        WebElement parent = ActionsUtil.parent(checkCuotas);
	        WebElement inputCuotas = parent.findElement(By.xpath("./input[@class='checkbox']"));
	        inputCuotas.click();
	        ActionsUtil.cargandoFrameInterno(driver);
	        WebElement elementCuota = driver.findElement(By.xpath(
	                "//div[@class=\"roe-widget-area selected-hardware-offers\"]//table[@class=\"parameters\"]//tr[@class=\"characteristic-item force_enabled\"]//select"));

	        Select selectCuota = new Select(elementCuota);
	        selectCuota.selectByVisibleText(coutas);
	        ActionsUtil.cargandoFrameInterno(driver);
	    }
	    
	    public WebElement obtenerElementoEspecificoTabla(final String nombreByTabla,
	            final String nombreByElemento, final String referencia)
	            throws Exception {
	        sharedObjet(nombreByTabla);
	        WebElement filaColumna = ActionsUtil.obtenerElementoReferenciaTabla(
	                driver, getObjetoToAction(), referencia);
	        sharedObjet(nombreByElemento);
	        return filaColumna.findElement(getObjetoToAction());
	    }
	    
	    public void validarEquipoIngresarImeiNumeroCuotas(String nombreEquipo, String imei, String numeroCuota) throws Exception {
	        nombreDelEquipoRenovacion = nombreEquipo;
	        imeiDelEquipoRenovacion = imei;

	        String equipoExterno = properties.getProperty("texto.equipo.externo");
	        nombreDelEquipoInterno=nombreEquipo;
	        if (equipoExterno.equals(nombreEquipo)) {
	            bdTipoEquipo=equipoExterno;
	            // TODO mejorar xphat
	            By agregar = By.xpath("//*[@id=\"Celulares\"]/div/div[1]/div/table/tbody/tr[1]/td[3]/a");
	            ActionsUtil.esperarPoderClicBy(driver, agregar);
	            ActionsUtil.clic(driver, agregar);
	            ActionsUtil.cargandoFrameInterno(driver);
	            validarIMEIExterno(imei);
	        } else {
	            bdTipoEquipo="Equipo Interno";
	            equipoInterno(nombreEquipo, false);
	            validarIMEIInterno(nombreEquipo,imei);
	            if (!esPrepago && !"0".equals(numeroCuota)) {
	                localizarPagoCuotasInterno(numeroCuota);
	            }
	        }
	    }
	    
	    public void validarEquipoIngresarImei (String nombreEquipo, String imei) throws Exception {
	    	 
	    		bdTipoEquipo="Equipo Interno";
	            equipoInterno(nombreEquipo, false);
	            validarIMEIInterno(nombreEquipo,imei);
	            
	    }
	    
	   /* public By getObjetoToAction() {
	        return objetoToAction;
	    }*/
	    
	    public void validarRenovacionDownpayment(final String cuotas,
	            final String valorRenovacion, final String cuotaInicial)
	            throws Exception {
	        if (valorRenovacion != null && !valorRenovacion.isEmpty()) {
	            String textoRenovacion = properties.getProperty("texto.renovacion");
	            WebElement elemento = obtenerElementoEspecificoTabla(
	                    "tablaequipointerno", "selectgenerico",
	                    textoRenovacion + "*");
	            Select elementoSelect = new Select(elemento);
	            elementoSelect.selectByVisibleText(valorRenovacion);
	            ActionsUtil.cargandoFrameInterno(driver);
	        }

	        if (!esPrepago && !"0".equals(cuotas)) {
	            String numeroCuotaHabilitar = properties
	                    .getProperty("numero.cuota.habilita.inicial");
	            if (cuotaInicial != null && !cuotaInicial.isEmpty()
	                    && (numeroCuotaHabilitar.contains(cuotaInicial))) {
	                sharedObjet("cuotainicial");
	                WebElement input = driver.findElement(getObjetoToAction());
	                input.sendKeys(cuotaInicial);
	                input.sendKeys(Keys.TAB);
	                ActionsUtil.cargandoFrameInterno(driver);
	            }

	        }
	    }
	    
	    public void validarCrearCliente(String cliente) {
	        switch (cliente) {
	        case "0":
	            break;
	        case "1":
	            /* pestaña - Facturación y pago */
	            migaPanFacturacionPago();
	            existeCuentaFacturacion();
	            break;
	        default:
	            // throw new Exception("no esta contemplada la opcion de cliente: " + cliente);
	        }

	    }
	    
	    public void migaPanFacturacionPago() {
	        String texto = properties.getProperty("miga.pan.facturacion.pago");
	        localizarMigaDePan(texto);
	    }
	    
	    private void localizarMigaDePan(String migaPan) {
	        List<WebElement> pestania = driver.findElements(By.xpath("//a[@class=\"gwt-InlineHyperlink roe-pathList\"]"));
	        for (WebElement selenideElement : pestania) {
	            if (migaPan.equals(selenideElement.getText())) {
	                selenideElement.click();
	                ActionsUtil.cargandoFrameInterno(driver);
	                if (!"act".equals(ActionsUtil.parent(selenideElement).getAttribute("class"))) {
	                    selenideElement.click();
	                    ActionsUtil.cargandoFrameInterno(driver);
	                }
	                return;
	            }
	        }
	    }
	    
	    public void existeCuentaFacturacion() {
	        ActionsUtil.sleepSeconds(2);
	        By selectError = By.xpath("//select[@class='input-errorHighlight']");
	        WebElement selectElement = ActionsUtil.validacionInmediata(driver, selectError);
	        if (selectElement != null) {
	            botonNuevaCuentaFacturacion();
	            System.out.println("datos del select");
	            ActionsUtil.cargandoFrameInterno(driver);
	            selectElement = driver.findElement(selectError);
	            Select select = new Select(selectElement);
	            for (WebElement elemento : select.getAllSelectedOptions()) {
	                System.out.println(elemento.getText());
	            }
	            select.selectByIndex(1);
	            ActionsUtil.cargandoFrameInterno(driver);
	        }
	    }
	    
	    public void botonNuevaCuentaFacturacion() {

	        String texto = properties.getProperty("boton.crear");
	        String xpahtBotonCrear = "//div[@class=\"ui-widget-overlay-under-wrapper\"]//button[contains(text(),'" + texto
	                + "')]";
	        String botonNuevaCuentaFacturacion = null;
	        if (esPrepago) {
	            botonNuevaCuentaFacturacion = "boton.nueva.cuenta.facturacion.prepago";
	        } else {
	            botonNuevaCuentaFacturacion = "boton.nueva.cuenta.facturacion.pospago";
	        }

	        clicBotonTexto(botonNuevaCuentaFacturacion);
	        ActionsUtil.cargandoFrameInterno(driver);
	        By boton = By.xpath(xpahtBotonCrear);
	        ActionsUtil.esperarPoderClicBy(driver, boton);
	        ActionsUtil.clic(driver, boton);
	        ActionsUtil.cargandoFrameInterno(driver);

	    }
	    
	    public void migaPanRevision() {
	        String texto = properties.getProperty("miga.pan.revision");
	        localizarMigaDePan(texto);
	    }
	    
	    public void clicBotonFinalProcesoReservarActivo() {
	        String texto = properties.getProperty("boton.final.reservar.activos");
	        clicBotonFinalProceso(texto);
	        ActionsUtil.cargandoFrameInterno(driver);
	    }
	    
	    private List<Object> asignarParametrosResultadoFlujo(String nombreTag,String nombreFeature, String nombreFlujo, String idFLujoTabla) {
	    	AsignarParametrosSalidaInDTO inDTO = new AsignarParametrosSalidaInDTO();
	    	//inDTO.setId(ConsultasDinamicasConstans.NEXT_AUTOMATIZACIONES);
	    	boolean falla = false;
	    	inDTO.setFecha(fechaCreacion);
	    	inDTO.setNombreFeature(nombreFeature);
	    	inDTO.setNombrescenario(nombreFlujo);
	    	inDTO.setTagsfeature(nombreTag);
	    	inDTO.setParametrosJson(null);
	    	inDTO.setFallascenario(falla);
	    		    	
	        return ActionsUtil.asignarParametrosSalida(inDTO);
	    }
	    
	    public void clicBotonFinalProceso(String nombreBoton) {
	        WebElement botonFinalProceso = botonFinalesProceso(nombreBoton);
	        if (botonFinalProceso != null && !botonFinalProceso.getAttribute("class").contains("disabled")) {
	            botonFinalProceso.click();
	        } else {
//	           System.out.println();
	            System.err.println("no encontro el boton:" + nombreBoton);
	        }

	    }
	    
	    private void actualizarRegistroProcesado(String nombreFlujo, String idFLujoTabla) {
	        operacionesBD.abrirConexionBD();
	        String consulta=null;
	        if(nombreFlujo.equals("SUSPENCION_LINEA")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_SUSPENDER_REANUDAR_ID;
	        }
	        else if(nombreFlujo.equals("TRANSFERENCIA_BENEFICIARIO")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_TRASNFERENCIA_BENEFICIARIO_ID;
	        }
	        else if(nombreFlujo.equals("ACTIVACION_SLO")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_ACTIVACION_SLO_ID;
	        }
	        else if(nombreFlujo.equals("ALTAS_LINEAS")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_ALTAS_LINEAS_ID;
	        }
	        else if(nombreFlujo.equals("BAJA_ABONADO")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_BAJA;
	        }
	        else if(nombreFlujo.equals("CAMBIO_NUMERO")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_CAMBIO_NUMERO_ID;
	        }
	        else if(nombreFlujo.equals("CAMBIO_PLAN")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_CAMBIO_PLAN_ID;
	        }
	        else if(nombreFlujo.equals("CAMBIO_SIM")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_CABMIO_SIM;
	        }
	        else if(nombreFlujo.equals("CREAR_CLIENTE")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_CREAR_CLIENTE_ID;
	        }
	        else if(nombreFlujo.equals("FAC_EQUIPO_SIN_LINEA")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_FAC_EQUIPO_SIN_LINEA_ID;
	        }
	        else if(nombreFlujo.equals("RENOVACION_EQUIPO")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_RENOVACION_EQUIPO_ID;
	        }
	        else if(nombreFlujo.equals("FACTURAS_MISCELANEAS")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_FACTURAS_MISCELANEAS_ID;
	        }
	        else if(nombreFlujo.equals("GENERACION_DISPUTAS")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_GENERACION_DISPUTAS_ID;
	        }
	        operacionesBD.updateTablasEntrada(consulta,ProcesadoEnum.PROCESADO.name(), Long.parseLong(idFLujoTabla));
	        
	        
	    }
	    
	    public WebElement botonFinalesProceso(String botonFinalReservarActivos) {
	        By listaBotones = By.xpath(
	                "//li[@class='mobile-contracts__list-item']//div[@class='contracts__button-wrap'][not(contains(@style, 'display: none'))]//button");

	        List<WebElement> elemento = driver.findElements(listaBotones);

	        if (!elemento.isEmpty()) {
	            ActionsUtil.esperarVisibleBy(driver, listaBotones);
	        }

	        for (WebElement selenideElement : elemento) {
	            if (botonFinalReservarActivos.equals(selenideElement.getText())) {
	                return selenideElement;
	            }
	        }
	        // sino existe el boton con el texto
	        return null;
	    }
	    
	    public void clicBotonFinalProcesoConfigurarContrato() {
	        String texto = properties.getProperty("boton.final.configurar.contrato");
	        if (!esPrepago) {
	        	ActionsUtil.sleepSeconds(1);
	            clicBotonFinalProceso(texto);
	            /* Modo de gestión de contratos */
	            ActionsUtil.botonOkConfirmar(driver);
	            ActionsUtil.cargandoFrameInterno(driver);
	        }
	    }
	    
	    public void clicBotonFinalProcesoGenerarFactura() {
	        String texto = properties.getProperty("boton.final.generar.factura");
	        clicBotonFinalProceso(texto);
	        ActionsUtil.cargandoFrameInterno(driver);
	        /* boton ok */
	        ActionsUtil.botonOkConfirmar(driver);
	        ActionsUtil.cargandoFrameInterno(driver);
	        /* confirmar */
	        ActionsUtil.botonOkConfirmar(driver);
	        ActionsUtil.cargandoFrameInterno(driver);
	    }
	    
	    public void clicBotonFinalEnviar() {

	        By botonEnviar = By.xpath("//button[@class='mobile-contracts__submit-button']");
	        ActionsUtil.esperarPoderClicBy(driver, botonEnviar);
	        ActionsUtil.clic(driver, botonEnviar);
	        ActionsUtil.cargandoFrameInterno(driver);
	        ActionsUtil.botonOkConfirmar(driver);
	        ActionsUtil.cargandoFrameInterno(driver);

	        WebElement elemento = ActionsUtil.validacionInmediata(driver, botonEnviar);
	        if (elemento != null) {
	            ActionsUtil.sleepSeconds(8);
	            elemento.click();
	            ActionsUtil.cargandoFrameInterno(driver);
	            ActionsUtil.botonOkConfirmar(driver);
	            ActionsUtil.cargandoFrameInterno(driver);
	        }
	    }
	    
	    public void extraerNumeroordendeventa() throws Exception {
	        By orndeVenta = By.xpath("//div[@class=\"roe-widget-header\"]//h1[contains(text(),'La orden de venta')]");
	        ActionsUtil.esperarVisibleBy(driver, orndeVenta);
	        String ordenVenta = driver.findElement(orndeVenta).getText();
	        LOGGER.info(ordenVenta);
	        numeroOrdenVenta = ordenVenta.substring(19, 29);
	        LOGGER.info(numeroOrdenVenta);
	        driver.switchTo().defaultContent();
	        By ocultarBorradorTicket = By.xpath("//button[@class=\"gwt-Button ticket-panel-collapse-button expanded\"]");
	        ActionsUtil.clic(driver, ocultarBorradorTicket);
	        }
	    
	    public void volveralaOrden() {
	        ActionsUtil.sleepSeconds(1);
	        cambiarFrameInterno();
	        ActionsUtil.sleepSeconds(1);
	        By optionVolverOrden = By.xpath("//a[contains(text(),\"Volver a la orden\")]");
	        ActionsUtil.esperarPoderClicBy(driver, optionVolverOrden);
	        ActionsUtil.clic(driver, optionVolverOrden);
	        ActionsUtil.sleepSeconds(1);
	        }
	    
	    public void entradadeOrden() throws InterruptedException {
	        abrirPanelIzquierdo();
	        sharedObjet("panelizquierdaentradaorden");
	        ActionsUtil.esperarPoderClicBy(driver, getObjetoToAction());
	        ActionsUtil.clic(driver, getObjetoToAction());
	        ActionsUtil.sleepSeconds(1);
	    }
	    
	    private void abrirPanelIzquierdo() {
	        ActionsUtil.sleepSeconds(1);
	        driver.switchTo().defaultContent();
	        ActionsUtil.sleepSeconds(1);
	        ActionsUtil.ocultarMensajeNotificacionAutomatico(driver);
	        sharedObjet("panelizquierdo");
	        ActionsUtil.esperarPoderClicBy(driver, getObjetoToAction());
	        ActionsUtil.clic(driver, getObjetoToAction());
	        ActionsUtil.sleepSeconds(1);
	    }	
	    
	    public WebDriver getDriver() {
	        return driver;
	        }
	    
	    public void seleccionarlaOrdenAnterior() throws Exception {
	        List<WebElement> items = driver
	                    .findElements(By.xpath("//div[@class=\"ui-pager-gwt\"]//div[@class=\"gwt-HTML\"]"));
	        if (!items.isEmpty()) {
	          JavascriptExecutor executor = (JavascriptExecutor) getDriver();
	          executor.executeScript("arguments[0].scrollIntoView(true);", items.get(0));
	            }

	        List<WebElement> filas = driver.findElements(By.xpath("//table[@class=\"GF5QP-ADOM TableCtrl\"]"));
	        WebElement referencia = null;
	        for (WebElement selenideElement : filas) {
	          if (selenideElement.getText().contains(numeroOrdenVenta)) {
	            referencia = selenideElement.findElement(By.xpath("//a[contains(text(),'" + numeroOrdenVenta + "')]"));
	            JavascriptExecutor executor = (JavascriptExecutor) getDriver();
	            ActionsUtil.sleepSeconds(1);
	            executor.executeScript("arguments[0].click();", referencia);
	            return;
	                }
	            }
	            throw new Exception("El numero " + numeroOrdenVenta + " No esta ");
	        }
	    
	    public void verificarNumerodeCuentayGuardarURL() throws Exception {
	        String elementOrdenVenta = driver.findElement(By.xpath("//div[@class=\"gwt-Label nc-uip-page-caption\"]"))
	                .getText();
	        SubelementOrdenVenta = elementOrdenVenta.substring(16, 26);
	        System.out.println(SubelementOrdenVenta);

	        if (numeroOrdenVenta.equals(SubelementOrdenVenta)) {
	            url = driver.getCurrentUrl();
	            System.out.println(url);
	        } else {
	            throw new Exception("La orden de venta no es igual");
	        }
	        
	        guardarMensajeEnInforme(reporte,true);
	    }
	    
	    public void mostarOperador() {
	        // TODO elimiar al final de la evidencias
	        String usuario = properties.getProperty("nc.user");
	        By usuarioBy = By.xpath("//li[@class='separate']/a[contains(text(),'" + usuario + "')]");
	        ActionsUtil.clic(driver, usuarioBy);
	    }
	    
	    public void guardarResultadoFlujo(String nombreTag, String nombreFeature, String nombreFlujo, String idFLujoTabla) {
	        //se guarda por BD
	    	  ActionsUtil.setIdEjecucion(idFLujoTabla);
	          operacionesBD.abrirConexionBD();
	          List<Object> parametros= asignarParametrosResultadoFlujo(nombreTag,nombreFeature, nombreFlujo, idFLujoTabla);
	          operacionesBD.insertarRegistroAutomatizacion(parametros);
	          actualizarRegistroProcesado(nombreFlujo, idFLujoTabla);
	          listaIdsProcesados.add(idFLujoTabla);
	          ReportesUtil.setListaIdsProcesados(listaIdsProcesados);
	          
	      }

		public void validarTipoPlan(String plan) {
			esPrepago = ActionsUtil.validarPlanPrepago(plan);
			
		}
		
		public void ingresarImei(String imei) throws Exception {
	        imeiDelEquipoRenovacion = imei;
	        bdTipoEquipo="Equipo Interno";
	        validarImei(imei);
	       

	    }

		public void limiteCredito(String credito) {
	        sharedObjet("recalcular");
	        WebElement linkRecalcular=null;
	        try {
	            linkRecalcular = driver.findElement(getObjetoToAction());
	        }catch (Exception e) {
	        }
	        if(linkRecalcular!=null) {
	            cambiarFramePrincipal();
	            ActionsUtil.abriNuevaPestanya(driver, urlVisionGeneral);
	            ActionsUtil.clicPanelIzquiedoExpandir(driver);
	            By optionEntradaOrden = By.xpath("//div[@title=' Resumen']");
	            WebElement element=driver.findElement(optionEntradaOrden);
	            JavascriptExecutor executor = (JavascriptExecutor) getDriver();
	            executor.executeScript("arguments[0].click();", element);
	            ActionsUtil.sleepSeconds(1);
	            ActionsUtil.cilcElementoBottonTexto(driver, "Editar");
	            WebElement input = driver
	                    .findElement(By.xpath("//tr[@class='gwt-row nc-row-rolled-down gwt-last-row gwt-bottom-line']"
	                            + "//td[@class='nc-table-ingrid-cell-edit-available nc-parctrl-cell']//input[@type='text']"));
	            executor.executeScript("arguments[0].scrollIntoView(true);", input);
	            input.clear();
	            input.sendKeys(credito);
	            ActionsUtil.clic(driver,
	                    By.xpath("//div[@class='nc-toolbar-bottom']//div[@class='nc-toolbar-part nc-toolbar-part-left']"
	                            + "//button[text()='Guardar']"));
	            ActionsUtil.visibleElementoBottonTexto(driver, "Editar");
	            ActionsUtil.cerrarUltimaRegrearPrimeraPestanya(driver);
	            cambiarFrameInterno();
	            ActionsUtil.sleepSeconds(2);
	            linkRecalcular=driver.findElement(getObjetoToAction());
	            linkRecalcular.click();
	            ActionsUtil.sleepSeconds(2);
	        }
	            
	        
	    }
	
		public void generarDocumento() {
	        if (!esPrepago) {
	        	ActionsUtil.sleepSeconds(1);
	            ActionsUtil.clicElementoATexto(driver, "Generar documento");
	            ActionsUtil.cargandoFrameInterno(driver);
	 
	            // se comentra mientras se alla una solucion al firmar
	            /*
	            ActionsUtil.clicElementoATexto(driver, "Firmar");
	            ActionsUtil.cargandoFrameInterno(driver);
	            WebElement elemento = driver
	                    .findElement(By.xpath("//select[@class='gwt-ListBox nc-field-list-value nc-field-container']"));
	            Select select = new Select(elemento);
	            select.selectByVisibleText("Manual");
	            WebElement input=driver.findElement(By.xpath("//input[@class='gwt-TextBox nc-field-text-input gwt-TextBox-readonly']"));
	            ((JavascriptExecutor) driver).executeScript(
	                    "arguments[0].setAttribute('type', 'file')",input);
	            ((JavascriptExecutor) driver).executeScript(
	                    "arguments[0].removeAttribute('readonly','readonly')",input);
	            cargarArchivoElemento("D:\\Desktop\\eliminar\\Pdf_prueba.pdf",input);
	            ActionsUtil.clic(driver, By.xpath("//button[@type='button'][contains(text(),'Guardar')]"));
	            // update files
	            ActionsUtil.espererarDesparecerBy(driver, By.xpath(
	                    "//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front no-close JQPopup ui-draggable']"));
	                    */
	        }
	    }

		public void cargarArchivoElemento(String ruta, WebElement element) {
	        ActionsUtil.sleepSeconds(3);
	        element.sendKeys(ruta);
	    }
	
		public void ingresarCanalDistribucion(String valor) throws InterruptedException {
	        // TODO mover a la clase ObjetosPlmLogin.java
	        By canalDistribucion = By.xpath(
	                "//td[@class=\"nc-composite-layout-item-horizontal\"]//tr[@class=\"gwt-row nc-row-rolled-down\"][1]//div[@class=\"refsel_single\"]//input[@type=\"text\"]");
	        By ubicacion = By.xpath("//input[@id=\"id_refsel591865409_input\"]");
	        WebDriverWait wait = new WebDriverWait(driver, 10);
	        ActionsUtil.espererarDesparecerCargando(driver);
	        ActionsUtil.sleepSeconds(1);
	        wait.until(ExpectedConditions.elementToBeClickable(canalDistribucion));
	        ActionsUtil.invisibleListaBy(driver, ubicacion);

	        ActionsUtil.setTextField(driver, canalDistribucion, valor);
	        buscarPrimerResultado(true);
	        urlVisionGeneral=driver.getCurrentUrl();
	    }
		
		public void selecionarComboOrdenVenta(String valor, String telefono) {
	        valor = properties.getProperty(valor);
	        String ID = properties.getProperty("combo.orden.venta.id");

	        By radio = By.xpath("//span[text()='" + valor + "']/..//input[@type=\"radio\"]");
	        ActionsUtil.esperarPoderClicBy(driver, radio);
	        ActionsUtil.clic(driver, radio);

	        if (ID.equals(valor)) {
	            // TODO completar para cuando es por id

	            if (ID.equals(valor)) {
	                By productosFiltrados = By.xpath("//img[@id=\"button_9150048146013085219\"]");
	                ActionsUtil.esperarPoderClicBy(driver, productosFiltrados);
	                ActionsUtil.clic(driver, productosFiltrados);

	                // Ingresar el número de telefono en Filtros
	                By filtros = By.xpath("//textarea[@class=\"nc-memo-field\"]");
	                ActionsUtil.esperarPoderClicBy(driver, filtros);
	                ActionsUtil.setTextField(driver, filtros, telefono);

	                // Realizar clic en el botón Buscar Productos
	                By buscarProductos = By.xpath(
	                        "//button[@class=\"gwt-Button button_action_id_9150323480013185188_9150323595013185242_compositepopup_2 TableCtrl-button ParCtrl-editButton\"]");
	                ActionsUtil.esperarPoderClicBy(driver, buscarProductos);
	                ActionsUtil.clic(driver, buscarProductos);

	                // Realizar clic en el botón Cerrar
	                esperarCargaPantalla();
	                By botonCerrar = By.xpath(
	                        "//button[@class=\"gwt-Button button_action_id_9150323480013185188_9150323595013185248_compositepopup_2 TableCtrl-button cancel-button\"]");
	                ActionsUtil.esperarPoderClicBy(driver, botonCerrar);
	                ActionsUtil.clic(driver, botonCerrar);

	            }
	        }
	    }
		
		public void actualizarResultadoFlujo(String nombreTag,String nombreFeature, String nombreFlujo, String idFLujoTabla) {
	        //se guarda por BD
	          operacionesBD.abrirConexionBD();
	          List<Object> parametros= asignarParametrosResultadoFlujo(nombreTag,nombreFeature, nombreFlujo, idFLujoTabla);
	          parametros.add(idFLujoTabla);
	          parametros.add(fechaSegundosMilisegundos);
	          operacionesBD.actualizarRegistroAutomatizacion(parametros);
	          actualizarRegistroProcesado(nombreFlujo, idFLujoTabla);
	          
	      }

		public void busquedaPlanParaRenovacion(String texto) {
			//busqueda de plan para SSP Renovación 
			texto = properties.getProperty(texto);
			List<WebElement> elementos = null;
			WebElement elemento = null;
			WebElement cargarmas= driver.findElement(By.xpath("//button[@class='nc-button taButton nc-button_width_wide nc-button_type_primary jsLoadMorePlansList']"));
			do {
				elementos=driver.findElements(By.xpath("//a[@data-plan-name='"+texto+"']"));
				if(elementos.size()>0) {
					elemento = elementos.get(0);
					break;
				}
				
				try {
					cargarmas.click();
				} catch (Exception E){
					break;
				}

			}while (elementos.size()==0);
			
			if(elemento!=null) {
				elemento.click();
			}
			
		}

		
		public void seleccionarPlanAnadido () {
			By by = By.xpath("//label[@class='top-tree__input']");
	        ActionsUtil.clic(driver, by);
	        ActionsUtil.cargandoFrameInterno(driver);
		}
	
		public void ingresarTextoPlanNC(String texto) {
			texto = properties.getProperty(texto);
			ActionsUtil.cargandoFrameInterno(driver);
	        By input = By.xpath("//input[@class='search-filter__search-input__overview']");
	        ActionsUtil.esperarVisibleBy(driver, input);
	        ActionsUtil.setTextField(driver, input, texto.split(" ")[0]);

	        List<WebElement> filas = driver.findElements(By.xpath(
	                "//div[@class='roe-widget-content wrapper_selection']//td[@class='roe-table-cell name']/span/span"));
	        for (WebElement item_fila : filas) {
	            if (item_fila.isDisplayed() && item_fila.getText().startsWith(texto)) {
	                item_fila.click();
	                ActionsUtil.cargandoFrameInterno(driver);
	                break;
	            }
	        }
	        bdPlan=texto;
	        agregaMapaJson("4.Plan", texto);
		}

		public void ingresarTextoPlanFamiliarNC(String texto) {
			texto = properties.getProperty(texto);
			ActionsUtil.cargandoFrameInterno(driver);
	        By input = By.xpath("//input[@class='search-filter__search-input__overview']");
	        ActionsUtil.esperarVisibleBy(driver, input);
	        ActionsUtil.setTextField(driver, input, texto);

	        List<WebElement> filas = driver.findElements(By.xpath(
	                "//div[@class='roe-widget-content wrapper_selection']//td[@class='roe-table-cell name']/span/span"));
	        for (WebElement item_fila : filas) {
	            if (item_fila.isDisplayed()) {
	                item_fila.click();
	                ActionsUtil.cargandoFrameInterno(driver);
	                break;
	            }
	        }
	        bdPlan=texto;
	        agregaMapaJson("4.Plan", texto);
		}
		
		public void agregaMapaJson(String key, String value) {	
			ControllerUtil.agregaMapaJson(key, value);
		}
		
		public void establescoCanal(String canal) {	
			reporte.setCanal(canal);
		}
		
		public void doyClicEnComboCuota(String valor) {
	        if(valor.equalsIgnoreCase("1 cuota")){
	        sharedObjet("combocuotassp1cuota");
	        ActionsUtil.clic(driver, getObjetoToAction());
	        ActionsUtil.sleepSeconds(3);
	        
	        }
	        
	        if(valor.equalsIgnoreCase("3 cuotas")){
	            sharedObjet("combocuotassp3cuota");
	            ActionsUtil.clic(driver, getObjetoToAction());
	            ActionsUtil.sleepSeconds(3);

	         }
	        
	        if(valor.equalsIgnoreCase("6 cuotas")){
	            sharedObjet("combocuotassp6cuota");
	            ActionsUtil.clic(driver, getObjetoToAction());
	            ActionsUtil.sleepSeconds(3);

	         }
	        
	        if(valor.equalsIgnoreCase("12 cuotas")){
	            sharedObjet("combocuotassp12cuota");
	            ActionsUtil.clic(driver, getObjetoToAction());
	            ActionsUtil.sleepSeconds(3);

	            }
	        
	        if(valor.equalsIgnoreCase("18 cuotas")){
	            sharedObjet("combocuotassp18cuota");
	            ActionsUtil.clic(driver, getObjetoToAction());
	            ActionsUtil.sleepSeconds(3);

	            }
	        
	        else{
	            sharedObjet("combocuotasspsincuotas");
	            ActionsUtil.clic(driver, getObjetoToAction());
	            ActionsUtil.sleepSeconds(3);
	        }
	        
		}
		
		public void doyClicEnLasCuotassincuota(String cuotas) {
			sharedObjet("combocuotasspsincuotas");
	        ActionsUtil.clic(driver, getObjetoToAction());
	        ActionsUtil.sleepSeconds(3);
			
		}
		
		public void doyClicEnLasCuotasuna(String cuotas) {
			sharedObjet("combocuotassp1cuotas");
	        ActionsUtil.clic(driver, getObjetoToAction());
	        ActionsUtil.sleepSeconds(3);
			
		}
		
		public void doyClicEnLasCuotastres(String cuotas) {
			sharedObjet("combocuotassp3cuotas");
	        ActionsUtil.clic(driver, getObjetoToAction());
	        ActionsUtil.sleepSeconds(3);
			
		}

		public void doyClicEnLasCuotasseis(String cuotas) {
			sharedObjet("combocuotassp6cuotasr");
	        ActionsUtil.clic(driver, getObjetoToAction());
	        ActionsUtil.sleepSeconds(3);
			
		}
		

		public void guardarMensajeEnInforme(Reporte reporte,boolean estado) {
			ControllerUtil.guardarMensajeEnInforme(reporte, estado);
		}

		public void doyClicEnLasCuotasdoce(String cuotas) {
			sharedObjet("combocuotassp12cuotasr");
	        ActionsUtil.clic(driver, getObjetoToAction());
	        ActionsUtil.sleepSeconds(3);
			

		}
		
		public void doyClicEnLasCuotasdiesciocho(String cuotas) {
			sharedObjet("combocuotassp18cuotasr");
	        ActionsUtil.clic(driver, getObjetoToAction());
	        ActionsUtil.sleepSeconds(3);
			
		}
		
		public String devuelveValorMapaJson(String key) {	
			
		return ControllerUtil.devuelveValorMapaJson(key);
			
		}
		
}
