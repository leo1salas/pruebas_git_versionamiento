/**
 * 
 */
package co.com.indra.telefonicaec.utilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.com.indra.telefonicaec.bdconnection.ConnectionUtils;
import co.com.indra.telefonicaec.constants.ConsultasDinamicasConstans;
//import io.cucumber.core.logging.LoggerFactory;

/**
 * @author Harold
 *
 */
public class OperacionesBD {

	private PreparedStatement statement;
	static Connection conexion = null;
	static Logger LOGGER = LoggerFactory.getLogger(OperacionesBD.class);
	
	public void abrirConexionBD(){
		try {
			conexion = ConnectionUtils.getInstance().getConnectionBack();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ResultSet ejecutarSelect(String consulta, List<Object> parametros) {
		return ejecutarConsulta(consulta, parametros);
	}
	
	public int ejecutarInsert(String consulta,List<Object> parametros) {
		
		if(conexion==null){
			abrirConexionBD();
		}
		try {
			statement = conexion.prepareStatement(consulta);
			statement = insertarParametrosConsulta(parametros);
			
			return statement.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return -1;

	}
	
	public ResultSet ejecutarConsulta(String consulta, List<Object> parametros) {

		if(conexion==null){
			abrirConexionBD();
		}
		try {
			statement = conexion.prepareStatement(consulta);			
			statement = insertarParametrosConsulta(parametros);
			
			return statement.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public ResultSet ejecutarConsulta2(String consulta, List<Object> parametros) {

        if (conexion == null) {
            abrirConexionBD();
        }
        try {
            statement = conexion.prepareStatement(consulta);
            if (parametros != null) {
                int cont = 0;
                for (Object object : parametros) {
                    if (object instanceof Integer) {
                        statement.setInt(++cont, Integer.parseInt(object.toString()));
                    } else if (object instanceof String) {
                        statement.setString(++cont, object.toString());
                    } else if (object instanceof Double) {
                        statement.setDouble(++cont, Double.parseDouble(object.toString()));
                    } else if (object instanceof Boolean) {
                        statement.setBoolean(++cont, Boolean.getBoolean(object.toString()));
                    } else if (object instanceof Long) {
                        statement.setLong(++cont, Long.parseLong(object.toString()));
                    }
                }
            }
            return statement.executeQuery();
        } catch (SQLException e) {
            LOGGER.error("Se presento un error al ejecutar la consulta ", e);
        }

        return null;
    }
	
	private PreparedStatement insertarParametrosConsulta(List<Object> parametros) throws SQLException {
			
		try {
			if (parametros != null) {
				int cont = 0;
				for (Object object : parametros) {
					if (object instanceof Integer) {
						statement.setInt(++cont, Integer.parseInt(object.toString()));
					} else if (object instanceof String) {
						statement.setString(++cont, object.toString());
					} else if (object instanceof Double) {
						statement.setDouble(++cont, Double.parseDouble(object.toString()));
					} else if (object instanceof Boolean) {
						statement.setBoolean(++cont, Boolean.getBoolean(object.toString()));
					} else if (object instanceof Long) {
						statement.setLong(++cont, Long.parseLong(object.toString()));
					}
				}
			}		
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
		}
		
		return statement;
	}

	public void cerrarStatement() {
		try {
			if (statement != null && !statement.isClosed()) {
				statement.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void cerrarConexion() {
		try {
			if (conexion != null) {
				conexion.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	 public int updateTablasSalida(String urlAntes, String urlError,String idEjecucion, String ejecucion) {

	        if (conexion == null) {
	            abrirConexionBD();
	        }
	        try {
	            statement = conexion.prepareStatement(ConsultasDinamicasConstans.UPDATE_FLUJOS_AUTOMATICOS_CLIENTE_URL_ERROR_WARNING);
	            statement.setString(1, urlError);
	            statement.setString(2, urlError);
	            statement.setString(3, idEjecucion);
	            statement.setString(4, ejecucion);
	            return statement.executeUpdate();
	        } catch (Exception e) {
	            LOGGER.error("Error insertando en la tabla ", e);
	            return 0;
	        } finally {
	            cerrarStatement();
	        }
	        
	    }
	 
	 public int updateTablasEntrada(String consulta, String name, Long parseLong) {
	        if (conexion == null) {
	            abrirConexionBD();
	        }
	        try {
	            statement = conexion.prepareStatement(consulta);
	            statement.setString(1, name);
	            statement.setLong(2, parseLong);
	            return statement.executeUpdate();
	        } catch (Exception e) {
	            LOGGER.error("Error insertando en la tabla ", e);
	            return 0;
	        } finally {
	            cerrarStatement();
	        }
	    }
	 
	 public int insertarRegistroAutomatizacion(List<Object> parametros) {
	        if (conexion == null) {
	            abrirConexionBD();
	        }
	        StringBuilder sb = new StringBuilder();
	        sb.append(ConsultasDinamicasConstans.INSER_FLUJOS_AUTOMATICOS);
	        sb.append(ConsultasDinamicasConstans.ABRIR);
	        for (Object object : parametros) {
	            if (object instanceof String) {
	                if (object.toString().contains("NEXTVAL")) {
	                    sb.append(object + ",");
	                } else {
	                    sb.append("'" + object + "'" + ",");
	                }
	            } else if (object instanceof Date) {
	                sb.append("'" + object + "'" + ",");
	            } else {
	                sb.append(object + ",");
	            }
	        }
	        String insert = sb.toString().substring(0, sb.toString().length() - 1) + ConsultasDinamicasConstans.CERRAR;
	        try {
	            statement = conexion.prepareStatement(insert);
	            return statement.executeUpdate();
	        } catch (Exception e) {
	            LOGGER.error("Error insertando en la tabla ", e);
	            return 0;
	        } finally {
	            cerrarStatement();
	        }
	    }
	 
	 public int actualizarRegistroAutomatizacion(List<Object> parametros) {
	        if (conexion == null) {
	            abrirConexionBD();
	        }
	        try {
	            statement = conexion.prepareStatement(ConsultasDinamicasConstans.UPDATE_FLUJOS_AUTOMATICOS_ALL);
	            int cont = 1;
	            for (int i = 0; i < parametros.size(); i++) {
	                if (parametros.get(i) instanceof String) {
	                    statement.setString(cont, parametros.get(i) != null && parametros.get(i).toString() != null && !parametros.get(i).toString().isEmpty() ?
	                            parametros.get(i).toString() : null);
	                } else if (parametros.get(i) instanceof Long) {
	                    statement.setLong(
	                            cont, parametros.get(i) != null && parametros.get(i).toString() != null && !parametros.get(i).toString().isEmpty() ? Long.parseLong(parametros.get(i).toString()) : null);
	                } else if (parametros.get(i) instanceof Date) {
	                    statement.setDate(
	                            cont, parametros.get(i) != null && parametros.get(i).toString() != null && !parametros.get(i).toString().isEmpty()
	                            ? new java.sql.Date(((Date) parametros.get(i)).getTime()) : null);
	                } else {
	                    statement.setString(cont, null);
	                }
	                cont++;
	            }

	            return statement.executeUpdate();
	        } catch (Exception e) {
	            LOGGER.error("Error insertando en la tabla ", e);
	            return 0;
	        } finally {
	            cerrarStatement();
	        }
	    }

	
}
