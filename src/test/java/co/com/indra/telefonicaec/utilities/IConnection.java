
package co.com.indra.telefonicaec.utilities;

import java.sql.Connection;


public interface IConnection {

	public Connection getConnectionBack() throws Exception;
}
