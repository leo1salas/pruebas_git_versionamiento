package co.com.indra.telefonicaec.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Generador {
    
    private static String sDirectorioTrabajo = System.getProperty("user.dir");
    
    private static String TAG = "|row_index|";
    private static String ID = "|ID|";
    private static String TAG_NAME_TABLE = "When Init";
    
    public static void processFile(String taskTemplate, List<String> data) {
        FileWriter fichero = null;
        PrintWriter printWriter = null;
        BufferedReader bufferedReader = null;
        String cadena;
        String cadenaTmp;
        FileReader f;
        int seq = 1;
        
        System.out.println("Procesando: "+taskTemplate);
        
        // Elimina el archivo actual.
        removeFile(sDirectorioTrabajo+"/src/test/resources/salida/"+taskTemplate);
        
        try {
            f = new FileReader(sDirectorioTrabajo+"/src/test/resources/auto_confprecioequipo/templates/"+taskTemplate);
            bufferedReader = new BufferedReader(f);
            
            File fileTemp = new File(sDirectorioTrabajo+"/src/test/resources/salida/"+taskTemplate);
            if(!fileTemp.exists()) {
                if(!fileTemp.createNewFile()) {
                    throw new RuntimeException("Archivo no eliminado");
                }
            }
            
            fichero = new FileWriter(sDirectorioTrabajo+"/src/test/resources/salida/"+taskTemplate);
            printWriter = new PrintWriter(fichero);
            while ((cadena = bufferedReader.readLine()) != null) {
                printWriter.println(cadena);
                seq=1;
                if(cadena.contains(TAG)) {
                    if(data != null && data.size() > 0) {
                        for(String dataTemp : data) {
                            //OJO adicionar espacios a la izquierda
                        	cadenaTmp = cadena.replace(" ","");
                        	if (cadenaTmp.contains(ID)){
                        		printWriter.println("|"+seq+"|"+dataTemp+"|");
                        		seq=seq+1;
                        	}else {
                        		printWriter.println("|"+dataTemp+"|");
                        	}
                        }                       
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Excepción al buscar el archivo en processFile: " + e.getMessage());

        } catch (IOException e) {
            System.out.println("Excepción en processFile: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Excepción en processFile: " + e.getMessage());
        }finally {
            if (null != bufferedReader) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    System.out.println("Excepción bufferedReader en processFile: " + e.getMessage());
                }
            }
            if (null != fichero) {
                try {
                    fichero.close();
                } catch (IOException e) {
                    System.out.println("Excepción fichero en processFile: " + e.getMessage());
                }
            }
            
            if (null != printWriter) {
                printWriter.close();
            }
        }
    }
    
    private static void removeFile(String file) {
        File fileTemp = new File(file);
        if(fileTemp.exists()) {
            if(!fileTemp.delete()) {
                throw new RuntimeException("Archivo no eliminado");
            }
        }
    }
    
    public static List<String[]> getFiles(String file) {
        
        List<String[]> taskProcess = new ArrayList<String[]>();
        
        try (Stream<Path> walk = Files.walk(Paths.get(sDirectorioTrabajo+"/src/test/resources/cucumber_template/"+file))) {
            List<String> result = walk.filter(Files::isRegularFile)
                    .map(x -> x.toString()).collect(Collectors.toList());

            for(String fileTemp:result) {
                
                String tableName = getTableName(fileTemp);
                if(tableName != null) {
                    String[] fileTempArray = fileTemp.split("cucumber_template");
                    fileTemp = fileTempArray[1].substring(1, fileTempArray[1].length());
                    taskProcess.add(new String[] { tableName,  fileTemp});  
                    System.out.println("Archivo: "+tableName + " - "+fileTemp);
                }
            }

        } catch (IOException e) {
            System.out.println("Excepción en getFiles: " + e.getMessage());
        }
        
        return taskProcess;
    }
    
    private static String getTableName(String path) {
        BufferedReader bufferedReader = null;
        String cadena;
        FileReader fileReader = null;
        //Given Init "OTC_T_CAMBIO_NUMERO" "<ID_PARAM>"
        try {
            fileReader = new FileReader(path);
            bufferedReader = new BufferedReader(fileReader);
            while ((cadena = bufferedReader.readLine()) != null) {
                if(cadena.contains(TAG_NAME_TABLE)) {
                    String[] cadenaTemp = cadena.trim().split("\\\"");
                    return cadenaTemp[1].trim();
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Excepción al encontrar el archivo en getTableName: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Excepción en getTableName: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Excepción en getTableName: " + e.getMessage());
        }finally {
            if (null != bufferedReader) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    System.out.println("Excepción con bufferedReader en getTableName: " + e.getMessage());
                }
            }
            
            if (null != fileReader) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    System.out.println("Excepción con fileReader en getTableName: " + e.getMessage());
                }
            }
        }
        return null;
    }
    
}
