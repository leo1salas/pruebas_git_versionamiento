package co.com.indra.telefonicaec.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class MacroUtils {
	static PropertiesLoader properties = PropertiesLoader.getInstance();
	OPCPackage fis = null;
	FileOutputStream fout = null;
	XSSFWorkbook wbk = null;
	XSSFWorkbook wbkResult = null;
	XSSFSheet sh = null;
	XSSFCell Cell = null;
	XSSFRow row1 = null;
	DataFormatter formatter = null;
	public String excelPath = null;
	Sheet sheet = null;

	public MacroUtils(String excelPath) throws Exception {
		init(excelPath);
		sheet = wbk.getSheetAt(0);
	}

	public void modificarXlsx(File excelFile, Map<String, String> mapaEquipos) {
		try {
			FileInputStream file = new FileInputStream(excelFile);
			XSSFWorkbook xlsxOriginal = new XSSFWorkbook(file);
			XSSFSheet sheet = xlsxOriginal.getSheetAt(0);
			XSSFRow fila = null;
			XSSFCell celda = null;
			XSSFCell celdassp = null;
			String valor = null;
			//iniciar el for en 3 para 20191009 - PlantillaEquipos.xlsx
			//iniciar el for en 7 para MEF.xlsx
			for (int i = 7; i < sheet.getLastRowNum(); i++) {
				fila = sheet.getRow(i);
				if (fila != null) {
					celda = fila.createCell(Integer.parseInt(properties.getProperty("nc.xls.constante.inicio.equipo")));
					celdassp = fila.getCell(Integer.parseInt(properties.getProperty("nc.xls.constante.inicio.equipo.nombressp")));
					valor = mapaEquipos.get(celdassp.getStringCellValue());
					if (valor != null) {
						celda.setCellValue(valor);
					}
				}
			}

			file.close();
			FileOutputStream output = new FileOutputStream(excelFile);
			xlsxOriginal.write(output);
			output.close();
			xlsxOriginal.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error modificando archivo");
		}

	}
	
	public MacroUtils(String excelPath, String sheetName) throws Exception {
		init(excelPath);
		sheet = wbk.getSheet(sheetName);
	}

	void init(String excelPath) throws IOException, InvalidFormatException {
		this.excelPath = excelPath;
		fis = OPCPackage.open(new File(excelPath));	
		//fis = new FileInputStream(new File(excelPath));
		wbk = new XSSFWorkbook(fis);
		fis.close();
	}

	public int getRowCount() throws Exception {
		int RowCount = sheet.getLastRowNum() + 1;
		return RowCount;
	}

	public String getValue(int rowIndex, int colIndex) {
		if (sheet != null) {
			Row row = sheet.getRow(rowIndex);
			Cell cell = row.getCell(colIndex);
			if (cell.getCellType().equals(CellType.NUMERIC)) {
				double value=cell.getNumericCellValue();
				return String.valueOf(value);
			}else {
				return cell.getStringCellValue();
			}
		}
		return "";
	}

	public double getNumericValue(int rowIndex, int colIndex) {
		if (sheet != null) {
			Row row = sheet.getRow(rowIndex);
			Cell cell = row.getCell(colIndex);
			System.out.println("CellType: " + cell.getCellType());
			return cell.getNumericCellValue();
		}
		return -1;
	}

	
	
	public boolean setCellData(String sheetName, String colName, int rowNum, String data) {
		try {
			fis = OPCPackage.open(new File(excelPath));
			wbk = new XSSFWorkbook(fis);

			if (rowNum <= 0)
				return false;

			int index = wbk.getSheetIndex(sheetName);
			int colNum = -1;
			if (index == -1)
				return false;

			sh = wbk.getSheetAt(index);

			XSSFRow row = sh.getRow(3);
			for (int i = 0; i < row.getLastCellNum(); i++) {
				if (row.getCell(i).getStringCellValue().trim().equals(colName))
					colNum = i;
			}
			if (colNum == -1)
				return false;

			sh.autoSizeColumn(colNum);
			row = sh.getRow(rowNum);// changed now from rowNum-1
			if (row == null)
				row = sh.createRow(rowNum);

			Cell = row.getCell(colNum);
			if (Cell == null)
				Cell = row.createCell(colNum);

			Cell.setCellValue(data);

			fout = new FileOutputStream(excelPath);
			wbk.write(fout);
			fout.close();

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void saveFile(String Path) {
		try {
			FileOutputStream fileOut = new FileOutputStream(Path);
			wbk.write(fileOut);
			fileOut.close();
		} catch (Exception e) {

		}
	}

	public File getLatestFileFromDir(String dirPath) {
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			return null;
		}
		File lastModifiedFile = files[0];
		for (int i = 1; i < files.length; i++) {
			if (lastModifiedFile.lastModified() < files[i].lastModified()) {
				lastModifiedFile = files[i];
				System.out.println(lastModifiedFile);
			}
		}
		return lastModifiedFile;

	}

	public void readExcel(String filename) {
		Workbook wbs;

		FileInputStream fis = null;
		try {
			fis = new FileInputStream(filename);
		    wbs = new XSSFWorkbook(fis);
			ArrayList<ArrayList<ArrayList<Cell>>> excel = new ArrayList<ArrayList<ArrayList<Cell>>>();
			for (int i = 0; i < wbs.getNumberOfSheets(); i++) {
				ArrayList<ArrayList<Cell>> sheetData = new ArrayList<ArrayList<Cell>>();
				Sheet sheet = wbs.getSheetAt(i);
				System.out.println("sheet name: " + sheet.getSheetName());
				Iterator<Row> itR = sheet.rowIterator();
				while (itR.hasNext()) {
					Row cRow = itR.next();
					ArrayList<Cell> cellList = new ArrayList<Cell>();
					Iterator<Cell> itC = cRow.cellIterator();
					while (itC.hasNext()) {
						Cell cCell = itC.next();
						cellList.add(cCell);
					}
					sheetData.add(cellList);
				}
				excel.add(sheetData);
			}
			//System.out.println(printExcel(excel));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static Workbook getExcel(String filename) {
		Workbook wbs;

		FileInputStream fis = null;
		try {
			fis = new FileInputStream(filename);
			if (filename.charAt(filename.length() - 1) == 'x') {
				wbs = new XSSFWorkbook(fis);
			} else {
				wbs = new HSSFWorkbook(fis);
			}
			return wbs;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public static ArrayList<String> getValuesByRowName(Workbook wbs, String sheetName,
			String rowName/* , int maxColIndex */) {
		ArrayList<String> out = new ArrayList<String>();
		Sheet sheet = wbs.getSheet(sheetName);
		if (sheet != null) {
			Iterator<Row> itR = sheet.rowIterator();
			while (itR.hasNext()) {
				Row cRow = itR.next();
				if (cRow.getCell(0).toString().equals(rowName)) {
					for (int i = 1; i < cRow.getPhysicalNumberOfCells(); i++) {
						out.add(cRow.getCell(i).toString());
					}
				}
			}
		}
		return out;
	}
	
	public ArrayList<String> getValuesByRowNum(int  rownum, int colIndex) {
		ArrayList<String> out = new ArrayList<String>();
		if (sheet != null && colIndex > -1) {
			int counter = 0;
			Iterator<Row> itR = sheet.rowIterator();
			while (itR.hasNext()) {
				Row row = itR.next();
				if (counter > rownum) {
					 if(row.getCell(colIndex) != null){
					out.add(row.getCell(colIndex).toString());
					 }
				}
				counter++;
			}
		}
		return out;
	}
	
	public static ArrayList<String> getValuesByRowIndex(Workbook wbs, String sheetName,
			int rowIndex/* , int maxColIndex */) {
		ArrayList<String> out = new ArrayList<String>();
		Sheet sheet = wbs.getSheet(sheetName);
		if (sheet != null) {
			Row cRow = sheet.getRow(rowIndex);
			for (int i = 0; i < cRow.getPhysicalNumberOfCells(); i++) {
				out.add(cRow.getCell(i).toString());
			}
		}
		return out;
	}

	public static ArrayList<String> getValuesByColName(Workbook wbs, String sheetName, String colName, int rowIndex) {
		ArrayList<String> out = new ArrayList<String>();
		int colIndex = getColIndex(wbs, sheetName, colName, rowIndex);
		Sheet sheet = wbs.getSheet(sheetName);
		if (sheet != null && colIndex > -1) {
			int counter = 0;
			Iterator<Row> itR = sheet.rowIterator();
			while (itR.hasNext()) {
				Row row = itR.next();
				if (counter > rowIndex) {
					out.add(row.getCell(colIndex).toString());
				}
				counter++;
			}
		}
		return out;
	}

	public static int getRowIndex(Workbook wbs, String sheetName, String rowName) {
		return getRowIndex(wbs, sheetName, rowName, 0);
	}

	public static int getRowIndex(Workbook wbs, String sheetName, String rowName, int colTitleIndex) {
		return getRowIndex(wbs, sheetName, rowName, 0, 0, -1);
	}

	public static int getRowIndex(Workbook wbs, String sheetName, String rowName, int colTitleIndex, int startIndex,
			int endIndex) {
		int counter = 0;
		Sheet sheet = wbs.getSheet(sheetName);
		if (sheet != null) {
			Iterator<Row> itR = sheet.rowIterator();
			while (itR.hasNext()) {
				Row cRow = itR.next();
				if (cRow.getPhysicalNumberOfCells() > 0 && cRow.getCell(colTitleIndex).toString().equals(rowName)
						&& counter >= startIndex) {
					return counter;
				}
				counter++;
				if (endIndex > -1 && counter > endIndex) {
					break;
				}
			}
		}
		return -1;
	}

	public static ArrayList<Integer> getRowIndexes(Workbook wbs, String sheetName, String rowName) {
		ArrayList<Integer> out = new ArrayList<>();
		int counter = 0;
		Sheet sheet = wbs.getSheet(sheetName);
		if (sheet != null) {
			Iterator<Row> itR = sheet.rowIterator();
			while (itR.hasNext()) {
				Row cRow = itR.next();
				if (cRow.getPhysicalNumberOfCells() > 0 && cRow.getCell(0).toString().equals(rowName)) {
					out.add(new Integer(counter));
				}
				counter++;
			}
		}
		return out;
	}

	public static int getColIndex(Workbook wbs, String sheetName, String colName, int rowIndex) {
		int counter = 0;
		Sheet sheet = wbs.getSheet(sheetName);
		if (sheet != null) {
			Row row = sheet.getRow(rowIndex);
			Iterator<Cell> itR = row.cellIterator();
			while (itR.hasNext()) {
				Cell cell = itR.next();
				if (cell.toString().equals(colName)) {
					return counter;
				}
				counter++;
			}
		}
		return -1;
	}

	public static String printRow(ArrayList<Cell> row) {
		String out = "";
		for (Cell c : row) {
			out += c.toString() + ", ";
		}
		return out;
	}

	public static String printSheet(ArrayList<ArrayList<Cell>> sheet) {
		String out = "";
		for (ArrayList<Cell> row : sheet) {
			out += printRow(row) + "\n";
		}
		return out;
	}

	public static String printExcel(ArrayList<ArrayList<ArrayList<Cell>>> excel) {
		String out = "";
		for (ArrayList<ArrayList<Cell>> sheet : excel) {
			out += printSheet(sheet);
		}
		return out;
	}

}
