/**
 * 
 */
package co.com.indra.telefonicaec.utilities;

import java.io.Serializable;
import java.util.List;

/**
 * @author Harold
 *
 */
public class OperacionesBDInDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String consulta;
	private List<Object> parametros;
	
	
	public OperacionesBDInDTO(){
		
	}
	/**
	 * @param consulta
	 * @param conexion
	 * @param parametros
	 */
	public OperacionesBDInDTO(String consulta,  List<Object> parametros) {
		super();
		this.consulta = consulta;
		this.parametros = parametros;
	}
	/**
	 * @return the consulta
	 */
	public String getConsulta() {
		return consulta;
	}
	/**
	 * @param consulta the consulta to set
	 */
	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}
	
	/**
	 * @return the parametros
	 */
	public List<Object> getParametros() {
		return parametros;
	}
	/**
	 * @param parametros the parametros to set
	 */
	public void setParametros(List<Object> parametros) {
		this.parametros = parametros;
	}
	
	
	
	
}
