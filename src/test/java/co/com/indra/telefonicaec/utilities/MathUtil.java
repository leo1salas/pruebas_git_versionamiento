package co.com.indra.telefonicaec.utilities;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

public class MathUtil {

	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = BigDecimal.valueOf(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public static String formatStringToDoubleFormat(String valor) {
		try {
			double x = Double.parseDouble(valor);
			DecimalFormat df = new DecimalFormat("#,###.00");
			String formatted = df.format(x);
			if (formatted != null) {
				valor = formatted;
			}			
		} catch (Exception ex) {
		}
		return valor;
	}
	
	public static Double stringToDouble(String num) {
		DecimalFormat df = new DecimalFormat();
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		
		Double resultado = null;
		
		if(num.contains(",")) {		
			symbols.setDecimalSeparator(',');
		}
		else{
			symbols.setDecimalSeparator('.');
		}
		df.setDecimalFormatSymbols(symbols);
		
		try {
			resultado = df.parse(num).doubleValue();
		} catch (ParseException e) {			
			e.printStackTrace();
		}
				
        return resultado;	
	}
	
	public static Double divide2DoubleNumbers(String num1, String num2) {
		Double resultado = null;
		
		Double numD1 = stringToDouble(num1);
		Double numD2 = stringToDouble(num2);
		
		if(numD1 != null && numD2 != null) {
			resultado = numD1 / numD2;
		}
		
		return resultado;
	}
}
