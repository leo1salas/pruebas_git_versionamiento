package co.com.indra.telefonicaec.utilities;

import java.text.DecimalFormat;

public class testExcel{
	
	static OperacionesBD operacionesBD = new OperacionesBD();
	
	public static void main(String arg[]) {
		String valor="29.23";
		double x=Double.parseDouble(valor);
		DecimalFormat dffff = new DecimalFormat("#,###.00");
		String formatted = dffff.format(x); 
		if (formatted !=null) {
			valor = formatted;
			System.out.println(valor);
		}	
	}
	
}