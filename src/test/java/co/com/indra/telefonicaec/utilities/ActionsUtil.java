package co.com.indra.telefonicaec.utilities;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.io.FileUtils;
import org.hamcrest.CoreMatchers;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.com.indra.telefonicaec.constants.ConstantesGlobales;
import co.com.indra.telefonicaec.dto.AsignarParametrosSalidaInDTO;
import co.com.indra.telefonicaec.entities.Reporte;
import co.com.indra.telefonicaec.objects.ReporteController;
import co.com.indra.telefonicaec.pages.PageDefault;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import net.masterthought.cucumber.Reportable;
import net.masterthought.cucumber.presentation.PresentationMode;
import net.masterthought.cucumber.sorting.SortingMethod;

public class ActionsUtil {

	// Expresiones Regulares para los features \"([^\"]*)\" (\\d+) \"(.*?)\"
	
    private static final long MIL_MILISEGUNDOS = 1000L;
    private static final long MILISEGUNDOS_MINUTO = 60000L;
    private static final long DOS_MIL_MILISEGUNDOS = 2000L;
    private static final long CIEN_MILISEGUNDOS = 100L;
    private static final long DIEZ_MILISEGUNDOS = 10L;
    static OperacionesBD operacionesBD = new OperacionesBD();
    static String idEjecucion;
    static String ejecucion;
    

	private ActionsUtil() {
		throw new IllegalStateException("Utility class");
	}
	
	static String fechaSegundosMiligesundos;
	static HashMap<String, By> objetosPage = new HashMap<String, By>();
	static PropertiesLoader properties = PropertiesLoader.getInstance();
	private static final Logger LOGGER = LoggerFactory.getLogger(ActionsUtil.class);
	private static final long TIMEOUTS = (getProperty("webdriver.timeouts.implicitlywait")) != null
			? Long.parseLong(getProperty("webdriver.timeouts.implicitlywait"))
			: 2000L;
	private static final long PAGE_LOAD_TIMEOUTS = (getProperty("webdriver.timeouts.pageLoadTimeout")) != null
			? Long.parseLong(getProperty("webdriver.timeouts.pageLoadTimeout"))
			: 40000L;
	private static final long SET_SCRIPT_TIMEOUTS = (getProperty("webdriver.timeouts.setScriptTimeout")) != null
			? Long.parseLong(getProperty("webdriver.timeouts.setScriptTimeout"))
			: 60000L;
			
	private static final long ELEMENT_TO_BE_CLICKABLE = 60L;
	
	public static By getObjeto(String nombreObjeto) {
		By retorno = objetosPage.get(nombreObjeto);
		String valueContains = "Objeto no mapeado";
		if (retorno == null)
			valueContains = nombreObjeto;
		assertThat("Objeto no mapeado", CoreMatchers.equalTo(valueContains));
		return retorno;
	}

	public static void objetosPut(String key, By value) {
		String validacionKey = "Objeto mapeado en objectsmap";
		By valueKey = objetosPage.get(key);
		if (valueKey != null) {
			validacionKey = "El objeto " + key + " ya fue mapeado: " + valueKey;
			objetosPage = new HashMap<String, By>();
		}
		assertThat("Objeto mapeado en objectsmap", CoreMatchers.equalTo(validacionKey));
		objetosPage.put(key, value);
	}

	public static boolean objetosIsEmpty() {
		return objetosPage.isEmpty();
	}

	public static void setProperty(String property, String url) {
		properties.setProperty(property, url);
	}

	public static String getProperty(String property) {
		return properties.getProperty(property);
	}

	public static void highlightElement(WebDriver driver, By by) {
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
		for (int second = 0; second <= 60; second++) {
			try {
				driver.findElement(by);
				if (driver.findElement(by).isDisplayed())
					break;
			} catch (Exception e) {
				sleepMiliseconds(1);
			}
			sleepMiliseconds(100);
		}
		modificarEstilo(driver, by, "border: 3px solid green;");
		driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
	}

	private static void modificarEstilo(WebDriver driver, By by, String modifyStyle) {
		WebElement element = driver.findElement(by);
		String originalStyle = element.getAttribute("style");
		String comandoExecute = "arguments[0].setAttribute('style', arguments[1]);";
		modifyStyle = getEstiloModificado(driver, by, modifyStyle);
		for (int i = 0; i < 2; i++) {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript(comandoExecute, element, modifyStyle);
			sleepMiliseconds(15);
			js.executeScript(comandoExecute, element, originalStyle);
			sleepMiliseconds(5);
		}
	}

	private static String getEstiloModificado(WebDriver driver, By by, String modifyStyle) {
		final String ATRIBUTO_ESTILO = "border:";
		WebElement element = driver.findElement(by);
		String originalStyle = element.getAttribute("style");
		if (originalStyle.contains(ATRIBUTO_ESTILO)) {
			int indexInicio = 7 + originalStyle.indexOf(ATRIBUTO_ESTILO);
			String aux = originalStyle.substring(indexInicio);
			int indexFin = indexInicio + aux.indexOf(';');
			String strInicial = originalStyle.substring(0, originalStyle.indexOf(ATRIBUTO_ESTILO));
			String strMedio = modifyStyle.replace(";", "");
			String strFinal = originalStyle.substring(indexFin);
			modifyStyle = strInicial + strMedio + strFinal;
		} else {
			modifyStyle = modifyStyle + " " + originalStyle;
		}
		return modifyStyle;
	}

	public static void goToWebSide(WebDriver driver, String text) {
		driver.manage().timeouts().implicitlyWait(240, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(240, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(240, TimeUnit.SECONDS);
		String currentURL = driver.getCurrentUrl();
		if (!text.isEmpty() && !text.equals(currentURL)) {
			for(int i=0;i<3;i++) {
				try {
					driver.navigate().to(text);
					break;
				}catch(Exception e) {
					LOGGER.info("Exception NAVIGATE TO " +text+" Error:"+e);
				}
			}
		}
		driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
		driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUTS, TimeUnit.MILLISECONDS);
		driver.manage().timeouts().setScriptTimeout(SET_SCRIPT_TIMEOUTS, TimeUnit.MILLISECONDS);
	}

	public static void refresh(WebDriver driver) {
		driver.navigate().refresh();
	}

	public static void presionarTecla(WebDriver driver, By by, String tecla) {
		WebElement element = driver.findElement(by);
		List<WebElement> elementsToClicked = driver.findElements(by);
		for (WebElement element_ : elementsToClicked) {
			try {
				if (element_.isDisplayed()) {
					element = element_;
					break;
				}
			} catch (Exception e) {
				sleepMiliseconds(1);
			}
		}
		if (ActionsUtil.textoMinusculasSinEspacios(tecla).equalsIgnoreCase("f5")) {
			element.sendKeys(Keys.F5);
		} else if (ActionsUtil.textoMinusculasSinEspacios(tecla).equalsIgnoreCase("backspace")) {
			element.sendKeys(Keys.BACK_SPACE);
		} else if (ActionsUtil.textoMinusculasSinEspacios(tecla).equalsIgnoreCase("inicio")) {
			element.sendKeys(Keys.HOME);
		} else if (ActionsUtil.textoMinusculasSinEspacios(tecla).equalsIgnoreCase("fin")) {
			element.sendKeys(Keys.END);
		} else if (ActionsUtil.textoMinusculasSinEspacios(tecla).equalsIgnoreCase("suprimir")) {
			element.sendKeys(Keys.DELETE);
		} else if (ActionsUtil.textoMinusculasSinEspacios(tecla).equalsIgnoreCase("flechaizquierda")) {
			element.sendKeys(Keys.ARROW_LEFT);
		} else if (ActionsUtil.textoMinusculasSinEspacios(tecla).equalsIgnoreCase("flechaderecha")) {
			element.sendKeys(Keys.ARROW_RIGHT);
		} else if (ActionsUtil.textoMinusculasSinEspacios(tecla).equalsIgnoreCase("tab")) {
			element.sendKeys(Keys.TAB);
		} else if (ActionsUtil.textoMinusculasSinEspacios(tecla).equalsIgnoreCase("avanzarpagina")) {
			element.sendKeys(Keys.PAGE_DOWN);
			sleepSeconds(1);
		} else if (ActionsUtil.textoMinusculasSinEspacios(tecla).equalsIgnoreCase("retrocederpagina")) {
			element.sendKeys(Keys.PAGE_UP);
			sleepSeconds(1);
		} else if (ActionsUtil.textoMinusculasSinEspacios(tecla).equalsIgnoreCase("")) {
		} else {
			assertThat("Tecla no mapeado", CoreMatchers.equalTo(tecla));
		}
	}

	public static boolean existsElement(WebDriver driver, By objeto) {
		try {
			driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
			WebElement element = driver.findElement(objeto);
			driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
			if (element.isDisplayed())
				return true;
		} catch (Exception e) {
			System.out.print("EL ELEMENTO NO FUE ENCONTRADO...." +  objeto);
			return false;
		}
		return false;
	}

	public static boolean existsElement(WebElement element) {
		try {
			if (element.isDisplayed())
				return true;
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public static void noExiste(WebDriver driver, By objeto) {
		boolean existAny = false;
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
		List<WebElement> elementsThatNotExists = driver.findElements(objeto);
		driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
		for (WebElement element : elementsThatNotExists) {
			if (existsElement(element)) {
				existAny = true;
				String modifyStyle = getEstiloModificado(driver, objeto, "border: 3px solid green;");
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, modifyStyle);
				break;
			}
		}
		assertFalse("Se encontro el elemento " + objeto, existAny);
	}

	public static String textoMinusculasSinEspacios(String texto) {
		String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
		String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
		for (int i = 0; i < original.length(); i++) {
			texto = texto.replace(original.charAt(i), ascii.charAt(i));
		}
		texto = texto.replaceAll("\t|\n| ", "");
		texto = texto.toLowerCase();
		return texto;
	}

	public static void setTextField(WebDriver driver, By by, String text) {
		if (!text.isEmpty()) {
			highlightElement(driver, by);
			WebElement element = driver.findElement(by);
			element.clear();
			element.sendKeys(text);
		}
	}

	public static void setTextFieldSlowly(WebDriver driver, By by, String text) {
		if (!text.isEmpty()) {
			highlightElement(driver, by);
			WebElement element = driver.findElement(by);
			element.clear();
			for (int i = 0; i < text.length(); i++) {
				String character = text.substring(i, i + 1);
				element.sendKeys(character);
				sleepMiliseconds(5);
			}
		}
	}

	public static String[][] getTable(WebDriver driver, By by) {
		highlightElement(driver, by);
		WebElement tableElement = driver.findElement(by);
		List<WebElement> trCollection = tableElement.findElements(By.tagName("tr"));
		String[][] tabla = new String[999][999];
		int rowNum = 0;
		int colNum;
		int colMax = 0;
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
		for (WebElement trElement : trCollection) {
			List<WebElement> tdCollection = trElement.findElements(By.tagName("td"));
			if (tdCollection.isEmpty()) {
				tdCollection = trElement.findElements(By.tagName("th"));
			}
			if (tdCollection.size() > colMax)
				colMax = tdCollection.size();
			colNum = 0;
			for (WebElement tdElement : tdCollection) {
				tabla[rowNum][colNum] = tdElement.getText();
				colNum++;
			}
			rowNum++;
		}
		assertTrue(rowNum > 0);
		assertTrue(colMax > 0);
		String[][] tablaReturn = new String[rowNum][colMax];
		for (int i = 0; i < rowNum; i++) {
			System.arraycopy(tabla[i], 0, tablaReturn[i], 0, colMax);
		}
		driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
		return tablaReturn;
	}

	public static String[][] getTableDiv(WebDriver driver, By by) {
		highlightElement(driver, by);
		WebElement element = driver.findElement(by);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
		List<WebElement> childCollection = element.findElements(By.xpath("./*"));
		String[][] tabla = new String[999][999];
		int rowNum = 0;
		int colNum;
		int colMax = 0;
		boolean containInfo = false;
		for (WebElement childElement : childCollection) {
			List<WebElement> grandChildCollection = childElement.findElements(By.xpath("./*"));
			if (grandChildCollection.size() > colMax)
				colMax = grandChildCollection.size();
			colNum = 0;
			containInfo = false;
			for (WebElement tdElement : grandChildCollection) {
				String aux = tdElement.getText();
				tabla[rowNum][colNum] = aux;
				colNum++;
				if (aux != null && !aux.isEmpty()) {
					containInfo = true;
				}
			}
			if (containInfo) {
				rowNum++;
			}
		}
		assertTrue(rowNum > 0);
		assertTrue(colMax > 0);
		String[][] tablaReturn = new String[rowNum][colMax];
		for (int i = 0; i < rowNum; i++) {
			System.arraycopy(tabla[i], 0, tablaReturn[i], 0, colMax);
		}
		driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
		return tablaReturn;
	}

	public static String getText(WebDriver driver, By by) {
		highlightElement(driver, by);
		WebElement element = driver.findElement(by);
		return element.getText();
	}

	public static String getTextIfDisplayed(WebDriver driver, By by) {
		boolean wasRead = false;
		String retorno = null;
		List<WebElement> elementsToClicked = driver.findElements(by);
		for (WebElement element : elementsToClicked) {
			try {
				if (element.isDisplayed()) {
					retorno = element.getText();
					wasRead = true;
					break;
				}
			} catch (Exception e) {
				sleepMiliseconds(1);
			}
		}
		assertTrue("No se clickeo ningún elemento " + by, wasRead);
		return retorno;
	}

	public static String getTextAttribute(WebDriver driver, By by) {
		highlightElement(driver, by);
		WebElement element = driver.findElement(by);
		return element.getAttribute("value");
	}

	public static String getAttribute(WebDriver driver, By by, String atributo) {
		highlightElement(driver, by);
		WebElement element = driver.findElement(by);
		String retorno = element.getAttribute(atributo);
		if (retorno == null)
			retorno = "";
		if (retorno.isEmpty())
			retorno = element.getCssValue(atributo);
		return retorno;
	}

	public static void clicParent(WebDriver driver, By by) {
		WebElement element = driver.findElement(by);
		try {
			element.findElement(By.xpath("..")).click();
			element.findElement(By.xpath("../..")).click();
		} catch (Exception e) {
			LOGGER.error("Excepcion Clic .. o ../..:", e);
		}
	}

	public static void clic(WebDriver driver, By by) {
		highlightElement(driver, by);
		WebElement element = driver.findElement(by);
		element.click();
	}

	public static void clic(WebDriver driver) {
		WebElement currentElement = driver.switchTo().activeElement();
		currentElement.click();
	}

	public static void clicIfDisplayed(WebDriver driver, By by) {
		boolean wasCicked = false;
		List<WebElement> elementsToClicked = driver.findElements(by);
		for (WebElement element : elementsToClicked) {
			try {
				if (element.isDisplayed()) {
					element.click();
					wasCicked = true;
					break;
				}
			} catch (Exception e) {
				sleepMiliseconds(1);
			}
		}
		assertTrue("No se clickeo ningún elemento " + by, wasCicked);
	}

	public static void selectText(WebDriver driver, By by, String option) {
		highlightElement(driver, by);
		WebElement element = driver.findElement(by);
		new Select(element).selectByVisibleText(option);
	}

	public static void selectIndex(WebDriver driver, By by, int indexOption) {
		highlightElement(driver, by);
		WebElement element = driver.findElement(by);
		new Select(element).selectByIndex(indexOption);
	}

	public static void selectContains(WebDriver driver, By by, String valueContains) {
		highlightElement(driver, by);
		WebElement element = driver.findElement(by);
		String valueComboBox = element.getText();
		assertThat(valueComboBox, CoreMatchers.containsString(valueContains));
		String[] values = valueComboBox.split("\n");
		int index = 0;
		for (int i = 0; i < values.length; i++) {
			if (values[i].contains(valueContains)) {
				index = i;
				break;
			}
		}

		Select select = new Select(element);
		select.selectByIndex(index);
		WebElement option = select.getFirstSelectedOption();
		String valorActual = option.getText();
		if (!valorActual.contains(valueContains)) {
			select = new Select(element);
			select.selectByIndex(index + 1);
			option = select.getFirstSelectedOption();
			valorActual = option.getText();
		}
		assertThat(valorActual, CoreMatchers.containsString(valueContains));
	}

	public static void selectValue(WebDriver driver, By by, String valueOption) {
		highlightElement(driver, by);
		WebElement element = driver.findElement(by);
		new Select(element).selectByValue(valueOption);
	}

	public static void compareText(WebDriver driver, By by, String valorEsperado) {
		String valorObtenido = getText(driver, by);
		valorEsperado = valorEsperado.replace("\\\"", "\"");
		LOGGER.info("Compara los valores valorEsperado: " + valorEsperado + " valorObtenido: " + valorObtenido);
		assertEquals(valorEsperado, valorObtenido);
	}

	public static void containsText(WebDriver driver, By by, String valorEsperado) {
		String valorObtenido = getText(driver, by);
		valorEsperado = valorEsperado.replace("\\\"", "\"");
		assertThat(valorObtenido, CoreMatchers.containsString(valorEsperado));
	}

	public static void compareTextStart(WebDriver driver, By by, String textStart) {
		String valorObtenido = getText(driver, by);
		assertThat(valorObtenido, CoreMatchers.startsWith(textStart));
	}

	public static void compareTextNotEmpty(WebDriver driver, By by) {
		String valorObtenido = getText(driver, by);
		assertThat(valorObtenido, !valorObtenido.isEmpty());
	}
	

	public static boolean compareAtributo(WebDriver driver, By by, String atributo, String valorEsperado) {
		String valorObtenido = getAttribute(driver, by, atributo);
		if (valorObtenido.isEmpty() || valorEsperado.isEmpty())
		{	
			ControllerUtil.agregaMapaJson("Valores Iniciales Vacios","ValorEsperado es: " + valorEsperado + " y ValorObtenido es:" + valorObtenido);
			assertEquals(valorEsperado, valorObtenido);
			return true; //falla es escenario
		}else
		{
			try {
				if (valorObtenido.equals(valorEsperado)) {
					System.out.println("validacion correcta de precios");
					ControllerUtil.agregaMapaJson("Validacion de Precio"," El valor esperado: " + valorEsperado + " es igual al valor obtenido: " + valorObtenido);
					assertThat(valorObtenido, CoreMatchers.containsString(valorEsperado));
					return false;// no falla es escenario
				}else {
					System.out.println("validacion no correcta de precios");
					ControllerUtil.agregaMapaJson("Validacion de Precio"," El valor esperado: " + valorEsperado + " No es igual al valor obtenido: " + valorObtenido);
					assertThat(valorObtenido, CoreMatchers.containsString(valorEsperado));
					return true; //falla es escenario
				}
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("validacion Erronea de precios");
				ControllerUtil.agregaMapaJson("Validacion de Precio","Se presento un error en la validacion de precios");
				//ControllerUtil.agregaMapaJson("Validacion de Precio:","El valor esperado: " + valorEsperado + "No es igual al valor obtenido: " + valorObtenido);
				return true;
			}
			
		}
	}

	public static void checkBox(WebDriver driver, By by, boolean checked) {
		highlightElement(driver, by);
		WebElement element = driver.findElement(by);
		if (checked) {
			if (!element.isSelected() && element.isEnabled()) {
				element.click();
			}
		} else {
			if (element.isSelected() && element.isEnabled()) {
				element.click();
			}
		}
	}

	public static void ejecutarScript(WebDriver driver, String script, By by) {
		WebElement element = driver.findElement(by);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(script, element);
	}

	public static void ejecutarScript(WebDriver driver, String script) {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(script);
	}

//	public static void waitForXElements(WebDriver driver, By by, String condicion, int cantidad) {
//		ScriptEngineManager manager = new ScriptEngineManager();
//		ScriptEngine se = manager.getEngineByName("JavaScript");
//		driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
//		boolean brakeLoop = false;
//		for (int second = 0; second <= 60; second++) {
//			try {
//				int cantObtenida = driver.findElements(by).size();
//				Object result = se.eval(cantObtenida + " " + condicion + " " + cantidad);
//				if (result.equals(true)) {
//					brakeLoop = true;
//				}
//			} catch (Exception e) {
//				sleepMiliseconds(1);
//			}
//			sleepMiliseconds(100);
//			if (brakeLoop)
//				break;
//		}
//		driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
//	}

	private static ExpectedCondition<Boolean> getConditionWait(By objeto, String atributo, String valor) {
		ExpectedCondition<Boolean> condicion = null;
		if (textoMinusculasSinEspacios(atributo).equals("text")) {
			condicion = ExpectedConditions.textToBePresentInElementLocated(objeto, valor);
		} else {
			if (valor.equals("")) {
				condicion = ExpectedConditions.attributeToBe(objeto, atributo, valor);
			} else {
				condicion = ExpectedConditions.attributeContains(objeto, atributo, valor);
			}
		}
		return condicion;
	}

	public static void waitForAttribute(WebDriver driver, By objeto, String atributo, String valor) {
		WebDriverWait wait = new WebDriverWait(driver, (long) (TIMEOUTS / 1000));
		wait.until(getConditionWait(objeto, atributo, valor));
	}

	public static void waitForClickable(WebDriver driver, By objeto) {
		WebDriverWait wait = new WebDriverWait(driver, (long) (TIMEOUTS / 1000));
		wait.until(ExpectedConditions.elementToBeClickable(objeto));
	}

	public static void waitForNotAttribute(WebDriver driver, By objeto, String atributo, String valor) {
		WebDriverWait wait = new WebDriverWait(driver, (long) (TIMEOUTS / 1000));
		wait.until(ExpectedConditions.not(getConditionWait(objeto, atributo, valor)));
	}

	public static void dragAndDrop(WebDriver driver, By byDraggable, By byDroppable) {
		highlightElement(driver, byDraggable);
		WebElement elementDrag = driver.findElement(byDraggable);
		highlightElement(driver, byDroppable);
		WebElement elementDrop = driver.findElement(byDroppable);
		Actions dragAndDrop = new Actions(driver);
		dragAndDrop.dragAndDrop(elementDrag, elementDrop).perform();
	}

	public static void dragAndDrop(WebDriver driver, By byElement, int x, int y) {
		highlightElement(driver, byElement);
		WebElement elementDrag = driver.findElement(byElement);
		Actions dragAndDrop = new Actions(driver);
		dragAndDrop.dragAndDropBy(elementDrag, x, y).perform();
	}

	public static void sleepSeconds(int sleep) {
		sleepMiliseconds(1000L * sleep);
	}

	public static void sleepMiliseconds(long timeMiliSeconds) {
		try {
			Thread.sleep(timeMiliSeconds);
		} catch (Exception e) {
			LOGGER.error("Excepcion Sleep:", e);
		}
	}

	public static int byShared(WebDriver driver, By objClass1, By objClass2) {
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
		int retorno = 0;
		for (int second = 0; second <= 60; second++) {
			boolean flagBreak = false;
			try {
				driver.findElement(objClass1);
				if (driver.findElement(objClass1).isDisplayed()) {
					flagBreak = true;
					retorno = 1;
				}
			} catch (Exception e) {
				sleepMiliseconds(1);
			}
			try {
				driver.findElement(objClass2);
				if (driver.findElement(objClass2).isDisplayed()) {
					flagBreak = true;
					retorno = 2;
				}
			} catch (Exception e) {
				sleepMiliseconds(1);
			}
			if (flagBreak) {
				break;
			}
			sleepMiliseconds(100);
		}
		driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
		return retorno;
	}

	public static int byShared(WebDriver driver, By objClass1, By objClass2, By objClass3) {
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
		int retorno = 0;
		for (int second = 0; second <= 60; second++) {
			boolean flagBreak = false;
			try {
				driver.findElement(objClass1);
				if (driver.findElement(objClass1).isDisplayed()) {
					flagBreak = true;
					retorno = 1;
				}
			} catch (Exception e) {
				sleepMiliseconds(1);
			}
			try {
				driver.findElement(objClass2);
				if (driver.findElement(objClass2).isDisplayed()) {
					flagBreak = true;
					retorno = 2;
				}
			} catch (Exception e) {
				sleepMiliseconds(1);
			}
			try {
				driver.findElement(objClass3);
				if (driver.findElement(objClass3).isDisplayed()) {
					flagBreak = true;
					retorno = 3;
				}
			} catch (Exception e) {
				sleepMiliseconds(1);
			}
			if (flagBreak) {
				break;
			}
			sleepMiliseconds(100);
		}
		driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
		return retorno;
	}

	//// Base URL
	private static String baseURL;

	public static String getBaseUrl() {
		return baseURL;
	}

	public static void setBaseUrl(String base) {
		try {
			new URL(base);
			baseURL = base;
		} catch (Exception e) {
			baseURL = null;
		}
	}

	public static String updateUrlWithBaseUrlIfDefined(final String startingUrl) {
		String baseUrl = baseURL;
		if ((baseUrl != null) && (!baseUrl.isEmpty())) {
			return replaceHost(startingUrl, baseUrl);
		} else {
			return startingUrl;
		}
	}

	private static String replaceHost(final String starting, final String base) {
		String updatedUrl = starting;
		try {
			URL startingUrl = new URL(starting);
			URL baseUrl = new URL(base);

			String startingHostComponent = hostComponentFrom(startingUrl.getProtocol(), startingUrl.getHost(),
					startingUrl.getPort());
			String baseHostComponent = hostComponentFrom(baseUrl.getProtocol(), baseUrl.getHost(), baseUrl.getPort());
			updatedUrl = starting.replaceFirst(startingHostComponent, baseHostComponent);
		} catch (Exception e) {
			LOGGER.error("Failed to analyse default page URL: Starting URL: {}, Base URL: {}", starting, base);
			LOGGER.error("URL analysis failed with exception:", e);
		}
		return updatedUrl;
	}

	private static String hostComponentFrom(final String protocol, final String host, final int port) {
		StringBuilder hostComponent = new StringBuilder(protocol);
		hostComponent.append("://");
		hostComponent.append(host);
		if (port > 0) {
			hostComponent.append(":");
			hostComponent.append(port);
		}
		return hostComponent.toString();
	}

	public static void curretCompareURL(WebDriver driver, String urlExpected) {
		String currenturl = driver.getCurrentUrl();
		urlExpected = urlExpected.split("\\?")[0];
		currenturl = currenturl.split("\\?")[0];
		currenturl = replaceHost(currenturl, urlExpected);
		assertEquals(urlExpected, currenturl);
	}

	public static void switchWindowsTab(WebDriver driver, int indexTab) {
		driver.getWindowHandles();
		Set<String> currentHandlers = driver.getWindowHandles();
		String[] handle = new String[999];
		int i = 0;
		for (String pestana : currentHandlers) {
			handle[i] = pestana;
			i++;
		}
		handle = Arrays.copyOf(handle, i);
		if ((indexTab > 99) || (indexTab > handle.length)) {
			driver.switchTo().window(handle[handle.length - 1]);
		} else {
			driver.switchTo().window(handle[indexTab - 1]);
		}
		driver.findElement(By.xpath("//*"));
	}

	public static void switchFrame(WebDriver driver, int indexTab) {
		driver.switchTo().frame(indexTab);
	}

	public static void switchFrame(WebDriver driver, String nameFrame) {
		driver.switchTo().frame(nameFrame);
	}

	public static void switchFrame(WebDriver driver, By objeto) {
		WebElement elemento = driver.findElement(objeto);
		driver.switchTo().frame(elemento);
	}

	public static void closeCurrentWindowsTab(WebDriver driver) {
		driver.getWindowHandles();
		Set<String> currentHandlers = driver.getWindowHandles();
		if (currentHandlers.size() > 1) {
			By by = By.tagName("body");
			ejecutarScript(driver, "window.close();", by);
			switchWindowsTab(driver, 1);
		}
	}

	public static void resolucion(WebDriver driver, String x, String y) {

		int ancho = Integer.parseInt(x);
		int alto = Integer.parseInt(y);
		Dimension d = new Dimension(ancho, alto);
		driver.manage().window().setSize(d);

	}

	public static void borrarCookies(WebDriver driver) {
		driver.manage().deleteAllCookies();
	}

	public static String getSubString(WebDriver driver, By by, int strInit, int strEnd) {
		return getText(driver, by).substring(strInit, strEnd);
	}

	public static String encrypt(String data) {
		byte[] keyBytes = getKeyBytes();
		byte[] byteData = getDataBytes(data);

		try {
			byte[] resultado = aesEncryptExternKey(byteData, keyBytes);
			return Base64.getEncoder().encodeToString(resultado);
		} catch (Exception e) {
			LOGGER.error("Error al encriptar Base64: ", e);
			return "";
		}
	}

	static byte[] getKeyBytes() {
		final int KEY_LENGHT = 16;
		String strKey = getProperty("segurity.cipher.key");
		int lenKey = strKey.length();
		if (lenKey > KEY_LENGHT)
			lenKey = KEY_LENGHT;
		byte[] keyBytes = new byte[KEY_LENGHT];
		try {
			System.arraycopy(strKey.getBytes(), 0, keyBytes, 0, lenKey);
		} catch (Exception e) {
			LOGGER.error("Error al copiar array de la clave de encriptación: ", e);
			return new byte[0];
		}
		return keyBytes;
	}

	static byte[] getDataBytes(String data) {
		final int DATA_MULTIPLE_LENGHT = 16;
		int lenData = data.length();
		if ((lenData % DATA_MULTIPLE_LENGHT) != 0)
			lenData += DATA_MULTIPLE_LENGHT - (lenData % DATA_MULTIPLE_LENGHT);
		byte[] byteData = new byte[lenData];
		try {
			System.arraycopy(data.getBytes(), 0, byteData, 0, data.length());
		} catch (Exception e) {
			LOGGER.error("Error al transferir el arreglo de los datos a cifrar: ", e);
			return new byte[0];
		}
		return byteData;
	}

	public static String decrypt(String data) {
		byte[] keyBytes = getKeyBytes();
		byte[] byteData = getDataBytes(data);
		try {
			byteData = Base64.getDecoder().decode(data);
		} catch (Exception e) {
			LOGGER.error("Error al decodificar la data en base64: ", e);
			return "";
		}

		try {
			byte[] resultado = aesDecryptExternKey(byteData, keyBytes);
			return new String(resultado, StandardCharsets.UTF_8);
		} catch (Exception e) {
			LOGGER.error("Error al desencriptar UTF-8: ", e);
			return "";
		}
	}

	public static byte[] aesEncryptExternKey(byte[] bytesFuente, byte[] key16B) {
		return aesEncryptExternKey(bytesFuente, key16B, new byte[16]);
	}

	public static byte[] aesEncryptExternKey(byte[] bytesFuente, byte[] key16B, byte[] iv16B) {
		Cipher cipher = null;
		byte[] resultado = null;
		SecretKeySpec keySpec = new SecretKeySpec(key16B, "AES");
		IvParameterSpec ivSpec = new IvParameterSpec(iv16B);

		try {
			cipher = Cipher.getInstance("AES/CBC/NoPadding");
		} catch (Exception e) {
			LOGGER.error("Error al crear el cifrador AES: ", e);
			return new byte[0];
		}
		try {
			cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
			resultado = cipher.doFinal(bytesFuente);
			return resultado;
		} catch (Exception e) {
			LOGGER.error("Error al cifrar AES: ", e);
			return new byte[0];
		}
	}

	public static byte[] aesDecryptExternKey(byte[] bytesFuente, byte[] key16B) {
		Cipher cipher = null;
		byte[] resultado = null;
		SecretKeySpec keySpec = new SecretKeySpec(key16B, "AES");
		IvParameterSpec ivSpec = new IvParameterSpec(new byte[16]);
		try {
			cipher = Cipher.getInstance("AES/CBC/NoPadding");
		} catch (Exception e) {
			LOGGER.error("Error al crear el decifrador AES: ", e);
			return new byte[0];
		}
		try {
			cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
			resultado = cipher.doFinal(bytesFuente);
			return resultado;
		} catch (Exception e) {
			LOGGER.error("Error al descifrar AES: ", e);
			return new byte[0];
		}
	}

	public static void takeSnapShot(WebDriver webdriver, String fileWithoutPath) {
		try {
 			TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
			File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
			String path = System.getProperty("user.dir") + "\\target\\evidences\\" + fileWithoutPath;
			File DestFile = new File(path);
			DestFile.getParentFile().mkdirs();
			FileUtils.copyFile(SrcFile, DestFile);
		} catch (Exception e) {
			LOGGER.error("Error tomado la foto para envio de pruebas: ", e);
		}

	}
	
	public static void takeScreeShot(WebDriver webdriver, String fileWithoutPath) {
		try {
 			TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
			File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
			String path = properties.getProperty("rutaArchivos") + fileWithoutPath + ".png";
			File DestFile = new File(path);
			DestFile.getParentFile().mkdirs();
			FileUtils.copyFile(SrcFile, DestFile);
		} catch (Exception e) {
			LOGGER.error("Error tomado la foto para envio de pruebas: ", e);
		}

	}
		

	public static void validarEstilo(WebDriver driver, By by, String estilo) {
		WebElement element = driver.findElement(by);
		String style = element.getAttribute("style");
		assertEquals(style, estilo);
//		assertFalse(style + " no contiene: " + estilo,style.trim().contains(estilo));
		System.out.println("EL MODELO NO FUE ENCONTRADO EN LA BUSQUEDA");
	}

	public static Reportable generarRepote() {
		try {
			String ruta = properties.getProperty("nc.reportes.ruta");
			File reportOutputDirectory = new File(ruta);
			List<String> jsonFiles = new ArrayList<>();
			jsonFiles.add(ruta + "/cukes.json");
			String buildNumber = properties.getProperty("nc.reportes.compilacion");
			String projectName = properties.getProperty("nc.reportes.nombre");
			Configuration configuration = new Configuration(reportOutputDirectory, projectName);
			configuration.setBuildNumber(buildNumber);
			// configuration.addClassifications("Browser", "Firefox");
			// configuration.addClassifications("Branch", "release/1.0");
			configuration.setSortingMethod(SortingMethod.NATURAL);
			configuration.addPresentationModes(PresentationMode.EXPAND_ALL_STEPS);
			configuration.setTrendsStatsFile(new File("target/test-classes/demo-trends.json"));

			ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
			return reportBuilder.generateReports();
		} catch (Exception e) {
			// se controla que si no genera reporte no se danie el proceso
			LOGGER.error("Se presento un error en la generación del reporte: ", e);
			return null;
		}
	}
	public static void esperarPoderClicBy(WebDriver driver, By xpaht) {
		ActionsUtil.sleepSeconds(1);
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_TO_BE_CLICKABLE);
        wait.until(ExpectedConditions.elementToBeClickable(xpaht));
    }

    public static void esperarPoderClicWebElement(WebDriver driver, WebElement elemento) {
    	ActionsUtil.sleepSeconds(1);
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_TO_BE_CLICKABLE);
        wait.until(ExpectedConditions.elementToBeClickable(elemento));
    }

    public static void esperarVisibleWebElement(WebDriver driver, WebElement elemento) {
    	ActionsUtil.sleepSeconds(1);
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_TO_BE_CLICKABLE);
        wait.until(ExpectedConditions.visibilityOf(elemento));
    }

    public static void esperarVisibleBy(WebDriver driver, By xpaht) {
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_TO_BE_CLICKABLE);
        wait.until(ExpectedConditions.visibilityOfElementLocated(xpaht));
    }

    public static void esperarVisibleListaBy(WebDriver driver, By xpaht) {
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_TO_BE_CLICKABLE);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(xpaht));
    }

    public static void esperarVisibleListaBySegundo(WebDriver driver, By xpaht, int segundos) {
        WebDriverWait wait = new WebDriverWait(driver, segundos);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(xpaht));
    }

    public static void espererarDesparecerCargando(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_TO_BE_CLICKABLE);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class=\"nc-loading-overlay\"]")));
        driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);

    }

    public static void cargandoFrameInterno(WebDriver driver) {

        try {
            long inicio = System.currentTimeMillis();
            driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
            WebElement cargando = driver.findElement(By.xpath("//div[@class='gwt-PopupPanel loader']"));
            driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
            long fin = System.currentTimeMillis();
            long tiempo = fin - inicio;
            System.out.println("cargandoFrameInterno:encontrado " + tiempo + " milisegundos");
            inicio = System.currentTimeMillis();
            WebDriverWait wait = new WebDriverWait(driver, ELEMENT_TO_BE_CLICKABLE);
            wait.until(ExpectedConditions.invisibilityOf(cargando));
            fin = System.currentTimeMillis();
            tiempo = fin - inicio;
            System.out.println("cargandoFrameInterno:invisible " + tiempo + " milisegundos");
        } catch (Exception e) {
            System.out.println("no se presento el cargando");
        }
        finally {
        	driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
        }

    }

    public static void espererarDesparecerBy(WebDriver driver, By xpaht) {
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_TO_BE_CLICKABLE);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(xpaht));
        driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);

    }

    public static void esperarInvisibleNoResultado(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_TO_BE_CLICKABLE);
        wait.until(ExpectedConditions
                .invisibilityOfElementLocated(By.xpath("//div[@class=\"nc-search-table-row-empty-content\"]")));
        driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);

    }

    public static void invisibleListaBy(WebDriver driver, By cargando) {
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 100);
        try {
            List<WebElement> elemento = driver.findElements(cargando);
            System.out.println("elementos antes localizado: " + elemento.size());
            long inicio = System.currentTimeMillis();
            wait.until(ExpectedConditions.invisibilityOfAllElements(elemento));
            long fin = System.currentTimeMillis();
            elemento = driver.findElements(cargando);
            System.out.println("elementos depues localizado: " + elemento.size());
            long tiempo = fin - inicio;
            System.out.println(tiempo + " segundos");

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERROR");
        }

        driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);

    }

    public static void switchFrameRoe(WebDriver driver) {
        driver.switchTo().defaultContent();
        WebElement frameInterno = driver.findElement(By.xpath("//iframe[@name='iframe_roe']"));
        driver.switchTo().frame(frameInterno);

    }

    public static void esperarHabilitarServicioCaracteristica(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, ELEMENT_TO_BE_CLICKABLE);
        wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath("//a[@class='gwt-InlineHyperlink roe-pathList']")));

    }
    
    /*
    public static void validarValoresCuotas(Reporte reporte, 
    		ArrayList<Map<String, String>> listValoresParaCuotas, ArrayList<Map<String, String>> litValoresMontoCuota,ArrayList<Map<String, String>> mapaJson) {
    	System.out.println(listValoresParaCuotas);
    	System.out.println(litValoresMontoCuota);
    	int cantidadIguales = 0;
    	
    	for (Map<String, String> entry : listValoresParaCuotas) {
    		
    		cantidadIguales = validarValoresMapa(reporte, entry, cantidadIguales, 
    			ConstantesGlobales.OBTENIDO, ConstantesGlobales.ESPERADO,
    			properties.getProperty("netkraker.equipo.montototal.diferentealesperado") +
				" Esperado : ", " obtenido : ");
    	}
    	
    	for (Map<String, String> entry : litValoresMontoCuota) {
    		
    		cantidadIguales = validarValoresMapa(reporte, entry, cantidadIguales, 
    				ConstantesGlobales.VALOR_MODELO, ConstantesGlobales.MONTO_TOTAL_NO_FACTURADO,
    				properties.getProperty("netkraker.equipo.montototal.nofacturado.diferente") +
    				" Monto no facturado : ", " monto calculado : ");
    		
    		cantidadIguales = validarValoresMapa(reporte, entry, cantidadIguales, 
    				ConstantesGlobales.MONTO_CUOTA_MODELO_CALCULADO, ConstantesGlobales.MONTO_CUOTA_MODELO,
    				properties.getProperty("netkraker.equipo.montototal.cuotas.diferente") +
    				" Monto de cuota : ", " monto del modelo : ");
    	}
    	    	    	
    	if(cantidadIguales != (listValoresParaCuotas.size() + litValoresMontoCuota.size())) {
    		assertTrue(false);
    	}
    }*/
    
    /*private static int validarValoresMapa(Reporte reporte, Map<String, String> entry, 
    		int cantidadIguales, String valor1, String valor2, String msg1, String msg2) {
    	
    	if(entry.containsKey(valor1) && entry.containsKey(valor2)) {
    		
			if(entry.get(valor1).equals(entry.get(valor2))) {
    			cantidadIguales++;
    			
    			if(entry.get(valor1).isEmpty() || entry.get(valor2).isEmpty()) {
    				assertTrue(entry.get(valor1).equals(entry.get(valor2)));
    			}else {
	    			assertThat(entry.get(valor1), CoreMatchers.containsString(entry.get(valor2)));
	    		}
    		}
    		else {
    			String mensaje = msg1 + entry.get(valor2) + msg2 + entry.get(valor1);
    			guardarMensajeEnInforme(reporte, mensaje, true);    			
    			
    		}
		}
    	return cantidadIguales;
    }*/
    
    /*public static void guardarMensajeEnInforme(Reporte reporte, String mensaje, Boolean pasaScenario) {
    	Reporte rep = new Reporte(reporte.getNombreFeature(), reporte.getNombreScenario(), reporte.getTagsFeature(), mensaje, pasaScenario);
		ReporteController.guardarEnReporte(rep);
    }*/
    
    public static void setFechaSegundosMiligesundos(String fechaSegundosMiligesundos) {
        ActionsUtil.fechaSegundosMiligesundos = fechaSegundosMiligesundos;
    }
    
    public static void cambiarUltimaPestanya(WebDriver driver) {
        ultimaPrimeraPestanya(driver, false);
    }
    
    public static void ultimaPrimeraPestanya(WebDriver driver, boolean cerrar) {
        String[] pestanyas = driver.getWindowHandles().toArray(new String[0]);
        if (pestanyas.length > 1) {
            int ultimo = pestanyas.length - 1;
            String ultima = pestanyas[ultimo];
            driver.switchTo().window(ultima);
            if (cerrar) {
                driver.close();
                String primera = pestanyas[0];
                driver.switchTo().window(primera);
            }
        }
    }
    
    public static void abriNuevaPestanya(WebDriver driver, String url) {
        ((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
        cambiarUltimaPestanya(driver);
        driver.get(url);
    }
    
    public static void switchToDefaultContent(WebDriver driver) {
        driver.switchTo().defaultContent();
    }
    
    public static void cerrarUltimaRegrearPrimeraPestanya(WebDriver driver) {
        ultimaPrimeraPestanya(driver, true);
    }
    
    public static WebElement obtenerElementoSpanTexto(WebDriver driver, String texto) {
        By boton = By.xpath("//span[contains(text(),'" + texto + "')]");
        return driver.findElement(boton);

    }
    
    public static void filtrarIrBodega(WebDriver driver,String nombreBodega) {
        clic(driver, By.xpath("//div[@class='tab-menu-holder'][1] | //div[@id='t9141747563313731920_0____$1']"));
        sleepSeconds(1);
        setTextField(driver,
                By.xpath("//div[@class='ui-tabs-panel ui-widget-content ui-corner-bottom']"
                        + "[@style='display: block;']//div[@id='typeinFilter']//input[@type='text']"),
                nombreBodega);
        sleepSeconds(1);
        WebElement elemento=driver.findElement(By.xpath("//a[contains(text(),'Aplicar')]"));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", elemento);
        esperarCargar(driver);
        sleepSeconds(1);
        clicElementoSpanTexto(driver, nombreBodega);
        sleepSeconds(1);
        esperarCargar(driver);
        
    }
    
    public static void filtrarEquipo(WebDriver driver,By modalInputBy,String nombreEquipo) {
        /** try{
         ActionsUtil.clic(driver, By.xpath("//span[@title='Ordenado y filtrado para columna IMEI']"));
         WebElement elementClear=driver.findElement(By.xpath("//button[@class='filter-dotted-link']"));
         elementClear.click();
         esperarCargar(driver);
         }
         catch (Exception e) {
             // TODO: handle exception
         }*/
         ActionsUtil.clic(driver, By.xpath("//span[@title='Ordenado y filtrado para columna Modelo']"));
         WebElement element=driver.findElement(modalInputBy);
         element.clear();
         element.sendKeys(nombreEquipo);
         //no es necesario dar clic, el busca al ingresar el texto
         try{
         WebElement elemento=driver.findElement(By.xpath("//button[contains(text(),'Aplicar')]"));
         JavascriptExecutor executor = (JavascriptExecutor) driver;
         executor.executeScript("arguments[0].click();", elemento);
         }catch (Exception e) {
             // TODO: handle exception
         }
         ActionsUtil.esperarCargar(driver);
     }
    
    public static void buscarPrimerResultadoBodega(WebDriver driver) {
        // TODO mover a la clase ObjetosPlmLogin.java
        ActionsUtil.sleepSeconds(7);
        By primerResultado = By.xpath("//*[@id='nc_refsel_list_row_0']/*[@class='refsel_name']");
        WebElement resultadoBusqueda = driver.findElement(primerResultado);
        resultadoBusqueda.click();
    }
    
    private static WebElement obtenerIconoBandejaMensajes(WebDriver driver, By bandejaMensajeBy) {
        WebElement mensajeElemento = null;
        try {
            driver.manage().timeouts().implicitlyWait(DIEZ_MILISEGUNDOS, TimeUnit.MILLISECONDS);
            mensajeElemento = driver.findElement(bandejaMensajeBy);
        } catch (NoSuchElementException e) {
            // TODO revisar Ecepciones
        } finally {
            driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
        }
        return mensajeElemento;
    }
    
    private static WebElement obtenerMensajes(WebDriver driver) {
        By mensajesBy = By.xpath("//div[@id='nc-message-tray']/div[@class='nc-message-tray-notifications']");
        WebElement mensajeElemento = null;
        try {
            driver.manage().timeouts().implicitlyWait(DIEZ_MILISEGUNDOS, TimeUnit.MILLISECONDS);
            mensajeElemento = driver.findElement(mensajesBy);
        } catch (Exception e) {
            // TODO revisar Ecepciones
        } finally {
            driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
        }
        return mensajeElemento;
    }
    
    static void guardarExceptionControlada(String mensaje) {
        guardarLog(mensaje, null);
    }
    
    static void guardarLog(String mensaje, Exception e) {
        String mensajeFinal = "EXCEPTION_CONTROLADA: ";
        if (mensaje != null) {
            mensajeFinal = mensaje;
        }
        if (e == null) {
            LOGGER.info(mensajeFinal);
        } else {
            LOGGER.info(mensajeFinal, e);
        }

    }
    
    public static void setIdEjecucion(String idEjecucion) {
        ActionsUtil.idEjecucion = idEjecucion;
    }
    
    public static String getIdEjecucion() {
        return idEjecucion;
    }
    
    public static String getEjecucion() {
        return ejecucion;
    }
    
    public static String obteberLinkElemento(WebElement elementoBy) {
        return elementoBy.getAttribute("href");
    }
    
    public static WebElement parent(SearchContext checkCuotas) {
        return checkCuotas.findElement(By.xpath(".."));
    }
    
    public static boolean obtenerMensajeErrorBandeja(WebDriver driver, Boolean esProcesoFinal) {
        driver.switchTo().defaultContent();
        Boolean existeError=Boolean.FALSE;
        By bandejaMensajeBy = By.xpath("//li[contains(@class,'notification-menu')]/div");
        WebElement bandejaMensajeElemento = obtenerIconoBandejaMensajes(driver, bandejaMensajeBy);
        String bandejaMensajeClase = null;
        if (bandejaMensajeElemento != null) {
            bandejaMensajeClase = bandejaMensajeElemento.getAttribute("class");
        }
        WebElement mensajeElemento = null;
        if (bandejaMensajeClase != null && !bandejaMensajeClase.contains("no-messages")) {
            mensajeElemento = obtenerMensajes(driver);
            if (mensajeElemento == null) {
                clic(driver, bandejaMensajeBy);
                mensajeElemento = obtenerMensajes(driver);
            }
        }
        if (mensajeElemento != null) {
            List<WebElement> mensajes = mensajeElemento.findElements(By.xpath("./div"));
            for (WebElement webElement : mensajes) {
                if (webElement.getAttribute("class").contains("nc-user-notification-error")) {
                    guardarExceptionControlada(webElement.getText());
                    takeSnapShot(driver, "ErrorProceso.png");
                    if(esProcesoFinal){
                    String urlActual=driver.getCurrentUrl();
                    String urlVerDetalles="";
                    WebElement detalles=null;
                    Boolean hayClick=Boolean.FALSE;
                    try{
                    //buscamos VerDetalles
                    detalles=driver.findElement(By.xpath("//a[contains(text(),'Ver detalles')]"));
                    detalles.click();
                    hayClick=Boolean.TRUE;
                    }
                    catch (Exception e) {
                       //buscamos Detalles
                        try{
                        detalles=driver.findElement(By.xpath("//a[contains(text(),'Detalles')]"));
                        detalles.click();
                        hayClick=Boolean.TRUE;
                        }
                        catch (Exception e1) {
                            hayClick=Boolean.FALSE;
                        }
                    }
                    if(hayClick){
                    cargandoFrameInterno(driver);
                    takeSnapShot(driver, "ErrorProceso.png");
                    urlVerDetalles=driver.getCurrentUrl();
                    operacionesBD.updateTablasSalida(urlActual,urlVerDetalles, getIdEjecucion(),getEjecucion());
                    }
                    }
                    existeError=Boolean.TRUE;
                    break;
                }
            }
        }
        return existeError;
    }
    
    public static String filtarEquipoObtenerImeiMoverEquipo(WebDriver driver, By modalInputBy, String nombreEquipo,
            String nombreBodegaActual) throws Exception {
        filtrarEquipo(driver, modalInputBy, nombreEquipo);
        String imeiInterno;

        imeiInterno = obtenerImeiInternoBodega(driver);
        if (imeiInterno == null) {
            throw new Exception("No hay equipo en la bodega");
        }
        clic(driver, By.xpath("//tr[contains(@class,'GDEEHVHDAN')]//td[1]//input"));
        cilcElementoBottonTexto(driver, "Mover");
        WebElement element = driver.findElement(modalInputBy);
        element.clear();
        element.clear();
        sleepSeconds(3);
        element.sendKeys(nombreBodegaActual);
        sleepSeconds(3);
        ActionsUtil.buscarPrimerResultadoBodega(driver);
        clic(driver,
                By.xpath("//div[@class='ui-dialog-content ui-widget-content']//button[text()='Mover']"));
        esperarCargar(driver);
        boolean existeError = false;
        try {
            // si no se cerro el modal es porque hay un error
            clic(driver,
                    By.xpath("//div[@class='ui-dialog-content ui-widget-content']//button[text()='Cancelar']"));
            existeError =obtenerMensajeErrorBandeja(driver, Boolean.FALSE);
        }catch (Exception e) {
            // TODO: handle exception
        }
        if(existeError) {
            throw new Exception("No se es posible mover el equipo");
        }
        return imeiInterno;

    }
    
    private static String obtenerImeiInternoBodega(WebDriver driver) throws Exception {
        String imeiInterno=null;
        try {
            imeiInterno=driver.findElement(By.xpath("//tr[contains(@class,'GDEEHVHDAN')]//td[2]//a")).getText();
        }catch (Exception e) {
          imeiInterno=null;
        }
        if(imeiInterno==null && (properties.getProperty("ambiente_produccion")==null ||
                properties.getProperty("ambiente_produccion").equalsIgnoreCase("true"))){
            throw new Exception("No hay equipo en la bodega");
        }
        return imeiInterno;
    }
    
    public static WebElement obtenerElementoReferenciaTabla(SearchContext driver, By tabla, String referencia)
            throws Exception {
        boolean encontrado = Boolean.FALSE;
        WebElement elementoEncontrado = null;
        List<WebElement> celdas = driver.findElements(tabla);
        for (WebElement webElement : celdas) {
            if (encontrado) {
                elementoEncontrado = webElement;
                break;
            }
            if (referencia.equals(webElement.getText())) {
                encontrado = Boolean.TRUE;
            }
        }
        if (elementoEncontrado == null) {
            throw new Exception("No se encontro elemento: " + referencia);
        }
        return elementoEncontrado;
    }
    
    public static void clicElementoSpanTexto(WebDriver driver, String texto) {
        obtenerElementoSpanTexto(driver,texto).click();
    }
    
    public static void esperarCargar(WebDriver driver) {
        invisibleListaBy(driver,By.xpath("//*[contains(@class,'nc-loading')]"));
    }
    
    public static String filtrarEquipoObtenerImeiInterno(WebDriver driver,By modalInputBy,String nombreEquipo) throws Exception {
        filtrarEquipo(driver,modalInputBy,nombreEquipo);
        return obtenerImeiInternoBodega(driver);
        
    }
    
    public static WebElement obtenerElementoBottonTexto(WebDriver driver, String texto) {
    	ActionsUtil.sleepSeconds(1);
        By boton = By.xpath("//button[text()='" + texto + "']");
        ActionsUtil.esperarPoderClicBy(driver, boton);
        return driver.findElement(boton);
    }
    
    public static void cilcElementoBottonTexto(WebDriver driver, String texto) {
        obtenerElementoBottonTexto(driver,texto).click();
    }
    
    public static WebElement validacionInmediata(WebDriver driver, By botonEnviar) {
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.MILLISECONDS);
        WebElement elemento = null;
        try {
            elemento = driver.findElement(botonEnviar);
        } catch (Exception e) {
            guardarExceptionControlada(e);
        } finally {
            driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
        }
        return elemento;
    }
    
    static void guardarExceptionControlada(Exception e) {
        guardarLog(null, e);
    }
    
    public static void botonOkConfirmar(WebDriver driver) {

        try {
            driver.manage().timeouts().implicitlyWait(DOS_MIL_MILISEGUNDOS, TimeUnit.MILLISECONDS);
            WebElement elemento = driver.findElement(By.xpath("//div[@class='ui-dialog-buttonset']//button[1]"));
            elemento.click();
            driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            String message = "Exception raised while authenticating user: " + e.getMessage();
            LOGGER.warn(message);
        }
    }
    
    public static void ocultarMensajeNotificacionAutomatico(WebDriver driver) {
        driver.switchTo().defaultContent();
        WebElement mensajeElemento = obtenerMensajesAutomatico(driver);
        By iconoBandejaMensajeBy = By.xpath("//li[contains(@class,'notification-menu')]/div");
        if (mensajeElemento != null) {
            clic(driver, iconoBandejaMensajeBy);
        }
    }
    
    private static WebElement obtenerMensajesAutomatico(WebDriver driver) {
        By mensajesBy = By.xpath(
                "//div[contains(@class,'ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-draggable nc-user-notification')]"
                        + "/div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix ui-draggable-handle']");
        WebElement mensajeElemento = null;
        try {
            driver.manage().timeouts().implicitlyWait(DIEZ_MILISEGUNDOS, TimeUnit.MILLISECONDS);
            mensajeElemento = driver.findElement(mensajesBy);
        } catch (Exception e) {
            // TODO revisar Ecepciones
        } finally {
            driver.manage().timeouts().implicitlyWait(TIMEOUTS, TimeUnit.MILLISECONDS);
        }
        return mensajeElemento;
    }
    
    public static String planActual(WebDriver driver) {
        By planActual = By.xpath("//div[@class='products-tree']/div[@class='products-tree-item']"
                + "/div[@class='products-tree-link-wrapper']");
        esperarVisibleBy(driver, planActual);

        String textPlan = (driver.findElement(planActual)).getText();
        textPlan = (textPlan.substring(textPlan.indexOf(':') + 1, textPlan.length())).trim();
        return textPlan;

    }
    
    public static Boolean validarPlanPrepago(String plan) {
        Boolean esPrepago = Boolean.FALSE;
        String[] planesPrepago = properties.getProperty("texto.planes.prepago").split(",");
        for (String item_plan : planesPrepago) {
            if (plan.startsWith(item_plan)) {
                esPrepago = Boolean.TRUE;
                break;
            }
        }
        return esPrepago;
    }
    
    public static String nombrePlanActual(WebDriver driver) {
        By planActual = By.xpath("(//*[@class='gwt-Anchor'])[1]");
        esperarVisibleBy(driver, planActual);

        return (driver.findElement(planActual)).getText();
    }
    
    public static void clicPanelIzquiedoExpandir(WebDriver driver) {
        ActionsUtil.sleepSeconds(1);
        By botonExpandir = By.xpath("//button[@class=\"gwt-Button left-panel-collapse-button collapsed\" "
                    + "or @class=\"left-panel-collapse-button collapsed gwt-Button\" "
                    + "or @class=\"collapsed left-panel-collapse-button gwt-Button\"]");
        ActionsUtil.esperarPoderClicBy(driver, botonExpandir);
        ActionsUtil.clic(driver, botonExpandir);
        ActionsUtil.sleepSeconds(1);
        
    }
    
    public static void clicElementoATexto(WebDriver driver, String texto) {
    	ActionsUtil.sleepSeconds(1);
        obtenerElementoATexto(driver,texto).click();
    }
    
    public static WebElement obtenerElementoATexto(WebDriver driver, String texto) {
        By boton = By.xpath("//a[contains(text(),'" + texto + "')]");
        return driver.findElement(boton);
    }

    public static void visibleElementoBottonTexto(WebDriver driver,
            String texto) {
        obtenerElementoBottonTexto(driver,texto);

    }
    
    public static void selectSamples(WebDriver driver,By by,String Texto)
    {
    	WebElement element = driver.findElement(by);
        //WebElement element= driver.findElement(by); //This is the 'Select' element locator
        Select sel=new Select(element);
        sel.selectByVisibleText(Texto); //This will select the 'Option' from 'Select' List who's visibility text is "Cycle". 
        //NOTE: This will be case sensitive
    }
    

    public static void clickLinkTexto(WebDriver driver, String texto) {
        By link = By.xpath("//a[contains(text(), '" + texto + "')]");
        esperarPoderClicBy(driver, link);
        clic(driver, link);
    }
    
    public static List<Object> asignarParametrosSalida(AsignarParametrosSalidaInDTO inDTO) {
        List<Object> parametros= new ArrayList<>();
        
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = format.format(inDTO.getFecha());
		
        parametros.add(inDTO.getId());
        parametros.add(dateString);
        parametros.add(inDTO.getNombreFeature());
        parametros.add(inDTO.getNombrescenario());
        parametros.add(inDTO.getTagsfeature());
        parametros.add(inDTO.getParametrosJson());
        parametros.add(inDTO.getFallascenario());
        return parametros;
    } 
    
}
	