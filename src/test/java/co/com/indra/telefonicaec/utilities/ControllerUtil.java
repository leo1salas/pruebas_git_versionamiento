package co.com.indra.telefonicaec.utilities;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.hamcrest.CoreMatchers;
import org.json.JSONArray;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.com.indra.telefonicaec.constants.ConstantesGlobales;
import co.com.indra.telefonicaec.constants.ConsultasDinamicasConstans;
import co.com.indra.telefonicaec.constants.FormatConstans;
import co.com.indra.telefonicaec.dto.AsignarParametrosSalidaInDTO;
import co.com.indra.telefonicaec.entities.Reporte;
import co.com.indra.telefonicaec.enums.ProcesadoEnum;
import co.com.indra.telefonicaec.enums.TablaEnum;
import co.com.indra.telefonicaec.objects.ObjetosPlmLogin;
import co.com.indra.telefonicaec.objects.ReporteController;
import gherkin.deps.com.google.gson.Gson;


public class ControllerUtil {

		private By objetoToAction = By.xpath("/html/body");
	    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerUtil.class);
	    static PropertiesLoader properties = PropertiesLoader.getInstance();
	    private static WebDriver driver;
	    private DriverManagerFactory page;
	    private Boolean esPrepago;
	    private String numeroOrdenVenta; // Extracción orden de venta al crearse
	    private String SubelementOrdenVenta; // Extracción orden de venta consulta final
	    private String url; // Extracción orden de venta consulta final
	    private String planActual;// para cambio de plan
	    private String tipoCambioPlan; // para cambio de plan
	    private static final String CASO_POS_PRE = "POS_PRE";// para cambio de plan
	    private static final String CASO_PRE_POS = "PRE_POS";// para cambio de plan
	    /** Número de teléfono asociado al proceso.*/
	    private String numeroTelefono; 
	    /** Número de cuenta facturación asociado al proceso.*/
	    /**Categoria asociado al proceso.*/
	    private String categoria;
	    private String tipo_cuenta;
	    private String nombre;
	    private String ciclo;
	    private String cuentaFacturacion;
	    /** Nombre cliente asociado al proceso.*/
	    private String nombreCliente;
	    private static String parametrosJson; //parametros con lso que fue ejecutado el flujo
	    private static String fechaSegundosMilisegundos;
	    public static List<String> listaIdsProcesados;
	    private String estadoOrden;
	    private String tipoProceso;
	    private String procesadoCuando;
	    private String bdClientePrepago;
	    private String bdClientePospago;
	    private boolean indicadorInAdvance=false;
	    private String bdClientePospagoInAdvance;
	    private String bdNumeroCuentaCliente;
	    private String bdPlan;
	    private String urlVisionGeneral;
	    public static Date fechaCreacion;
	    public static String Numcuota;
	    /**
	     * interno o externo
	     */
	    private String bdTipoEquipo;
	    static OperacionesBD operacionesBD = new OperacionesBD();

	    private String numeroAntiguo; // Extracción número de telefono
	    private String numeroNuevo; // Extracción número de telefono nuevo

	    private String simCardInicial = "";

	    private String nombreDelEquipoInterno = "";
	    private String imeiDelEquipoIterno = "";
	    private String nombreDelEquipoRenovacion = "";
	    private String imeiDelEquipoRenovacion = "";

	    private String simcardVieja;
	    private String simcardNueva;
	    private String sloActivados = "";
	    private String nombrePlan = "";

	    private String planNuevo = "";
	    private String bdNumeroDocumento;
	    private String bodegaDefecto;
	    private String fechaEnvio;
	    private String canalDistribucion;
	    private String confirmadoPor;
	    private String urlError;
	    public static Map<String, String> mapaJson;
	    
	    
	    public ControllerUtil( WebDriver driver,DriverManagerFactory page) {
	        this.driver=driver;
	        this.page=page;
	        this.fechaCreacion=new Date();
	        if (ActionsUtil.objetosIsEmpty()) {
	            LOGGER.info("Inicialización de objetos");
	            new ObjetosPlmLogin();
	        }
	        if(fechaSegundosMilisegundos==null){
		        Calendar fechaActual= Calendar.getInstance();
		        SimpleDateFormat format= new SimpleDateFormat(FormatConstans.FORMATO_FECHA_HORA_PEGADO);
		        fechaSegundosMilisegundos=format.format(fechaActual.getTime());
		        fechaSegundosMilisegundos=fechaSegundosMilisegundos.replace(":","");
		        ReportesUtil.setFechaSegundosMiligesundos(fechaSegundosMilisegundos);
		        ActionsUtil.setFechaSegundosMiligesundos(fechaSegundosMilisegundos);
	        }
	        if(listaIdsProcesados==null){
	            listaIdsProcesados= new ArrayList<>();
	        }
	       
	    }	
	     
	    /**
	     * 
	     * Método responsable el mapa con el resultado de la consulta
	     *
	     * @uthor INDRA <br>
	     *        Harold Tabord <br>
	     *        hstaborda@indracompany.com
	     *
	     * @date 27/01/2020
	     * @version 1.0
	     * @param rs
	     * @param mapaEntradaBD
	     */
	    private static void llenarMapa(ResultSet rs, Map<String, String> mapaEntradaBD) {
	        try {
	            
	            ResultSetMetaData rsmd = rs.getMetaData();
	            String nombreColumna=null;
	            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
	                nombreColumna=rsmd.getColumnName(i);
	                mapaEntradaBD.put(nombreColumna, rs.getString(nombreColumna));
	             }
	        } catch (SQLException e) {
	            LOGGER.error("Se presento un error al llenar el mapa", e);
	        }

	    }
	    
	    
	    /**
	     * Metodo encargado de finalizar el navegador
	     */
	    public static void cerrarNavegador() {
	      driver.quit();
	    }
	    	
		/**
	     * 
	     * Método responsable
	     *
	     * @uthor INDRA <br>
	     *        Harold Taborda<br>
	     *        hstaborda@indracompany.com
	     *
	     * @date 23/01/2020
	     * @version 1.0
	     * @param tabla
	     * @param id
	     */
	    public Map<String, String> consultarTablaEntradaFlujo(String tabla, String id) {
	        Map<String, String> mapaEntradaBD= new HashMap<>();
	        operacionesBD.abrirConexionBD();
	        String consulta=null;
	        List<Object> params= new ArrayList<>();
	        params.add(Long.parseLong(id));
	        if(TablaEnum.OTC_T_ALTAS_LINEAS.name().equals(tabla)){
	            consulta=ConsultasDinamicasConstans.SELECT_OTC_T_ALTAS_LINEAS_ID;
	        }
	        
	        ResultSet rs = operacionesBD.ejecutarConsulta2(consulta, params);
	        try {
	             if (rs != null && rs.next()) {
	                 llenarMapa(rs,mapaEntradaBD);
	                }
	             else{
	                 operacionesBD.cerrarStatement();
	                 cerrarNavegador();
	             }
	        } catch (SQLException e) {
	            LOGGER.error("Se presento un error al consultar las propiedades", e);
	        } finally {
	            if (rs != null)
	                try {
	                    rs.close();
	                } catch (SQLException e) {
	                    LOGGER.error("Error cerrando el resultSet al init", e);
	                }
	        }
	        operacionesBD.cerrarStatement();
	        String procesado=mapaEntradaBD.get("procesado");
	        properties.setProperty("isProcesado", "false");
	        if(procesado!=null && procesado.equalsIgnoreCase(ProcesadoEnum.PROCESADO.name())){
	            LOGGER.info("Se finaliza el flujo ya que el registro ya fue procesado");
	            properties.setProperty("isProcesado", "true");
	            cerrarNavegador();
	        }
	        return mapaEntradaBD;
	        
	    }
	    
	    private void buscarPrimerResultado(boolean hacerClic) {
	        // TODO mover a la clase ObjetosPlmLogin.java
	        ActionsUtil.sleepSeconds(1);
	        By primerResultado = By.xpath("//*[@id=\"nc_refsel_list_row_0\"]/*[@class=\"refsel_name\"]");
	        WebElement resultadoBusqueda = driver.findElement(primerResultado);
	        WebDriverWait wait = new WebDriverWait(driver, 30);
	        ActionsUtil.espererarDesparecerCargando(driver);
	        wait.until(ExpectedConditions.elementToBeClickable(resultadoBusqueda));
	        if (hacerClic) {
	            resultadoBusqueda.click();
	        }
	    }
	    
	    private void equipoInterno(String nombreEquipo, Boolean selecionarCantidadCopias) {
	        WebElement inputModelo = driver.findElement(By.xpath("//div[@class=\"search-filter__type\"]/input"));
	        inputModelo.clear();
	        inputModelo.sendKeys(nombreEquipo);
	        WebElement botonSiguiente = driver
	                .findElement(By.xpath("//*[@id=\"Celulares\"]/div/div[4]/table/tbody/tr/td[4]/img"));

	        String parar = null;
	        do {
	            parar = botonSiguiente.getAttribute("aria-disabled");
	            List<WebElement> filas = driver.findElements(By.xpath("//*[@id=\"Celulares\"]/div/div"));
	            for (WebElement selenideElement : filas) {
	                if (selenideElement.isDisplayed() && selenideElement.getText().contains(nombreEquipo)) {
	                    List<WebElement> filas2 = selenideElement.findElements(By.xpath("./div/table/tbody/tr"));
	                    for (WebElement selenideElement1 : filas2) {
	                        if (selenideElement1.isDisplayed() && selenideElement1.getText().contains(nombreEquipo)) {
	                            (selenideElement1.findElement(By.xpath("./td[3]/a"))).click();
	                            ActionsUtil.cargandoFrameInterno(driver);
	                            if (selecionarCantidadCopias) {
	                                By botonOk = By.xpath("//span[@class='ui-button-text'][contains(text(),'OK')]");
	                                ActionsUtil.esperarPoderClicBy(driver, botonOk);
	                                driver.findElement(botonOk).click();
	                            }
	                            return;
	                        }
	                    }
	                }
	            }
	            botonSiguiente.click();
	        } while ("false".equals(parar));
	    }
	    
	    public void selecionarContratoICCDNumeroTelefono(String contrato, String numeroSim, String numeroTelefono) {

	        By filas = By.xpath("//tr[@class='characteristic-item force_enabled']");
	        ActionsUtil.esperarVisibleListaBySegundo(driver, filas, 120);

	        List<WebElement> filaElemento = driver.findElements(filas);
	        String duracionContrato = properties.getProperty("texto.duracion.contrato");
	        String sim = properties.getProperty("texto.iccid");
	        String telefono = properties.getProperty("texto.numero.telefono");

	        for (WebElement webElement : filaElemento) {
	            if (!esPrepago) {
	                if (webElement.getText().contains(duracionContrato)) {
	                    Select selectContrato = new Select(webElement.findElement(By.xpath(".//select")));
	                    selectContrato.selectByVisibleText(contrato);
	                }
	            }
	            if (webElement.getText().contains(sim)) {
	                WebElement input = webElement.findElement(By.xpath(".//input[@class='refsel_input']"));
	                input.clear();
	                input.sendKeys(numeroSim);
	                buscarPrimerResultado(true);
	                ActionsUtil.cargandoFrameInterno(driver);
	                simCardInicial=input.getAttribute("value");
	            }
	            if (webElement.getText().contains(telefono)) {
	                WebElement input = webElement.findElement(By.xpath(".//input[@class='refsel_input']"));
	                if(numeroTelefono==null || numeroTelefono.isEmpty()) {
	                    numeroTelefono=input.getAttribute("value");
	                }else {
	                    input.clear();
	                    ActionsUtil.cargandoFrameInterno(driver);
	                    input.sendKeys(numeroTelefono);
	                    buscarPrimerResultado(true);
	                    ActionsUtil.cargandoFrameInterno(driver);
	                }
	                this.numeroTelefono=numeroTelefono;
	            }
	        }
	        boolean existeICCD = false;
	        boolean existeNumeroTelefono = false;
	        boolean existeDuracionContrato = false;

	    }
	    
	    public void cambiarFramePrincipal() {
	        ActionsUtil.switchToDefaultContent(driver);

	    }
	    
	    public void cambiarFrameInterno() {
	        ActionsUtil.switchFrameRoe(driver);

	    }
	    
	    private String obtenerImeiExterno() {
	        cambiarFramePrincipal();
	        String urlGeneradorImei = properties.getProperty("paht.generador.imei");
	        ActionsUtil.abriNuevaPestanya(driver, urlGeneradorImei);
	        
	        By botonBy = By.xpath("//input[@type='button']");
	        ActionsUtil.clic(driver, botonBy);
	        By textBy = By.xpath("//input[@id='imei_num']");
	        WebElement textElemento = driver.findElement(textBy);
	        String imeiValor=textElemento.getAttribute("value");
	        ActionsUtil.cerrarUltimaRegrearPrimeraPestanya(driver);
	        cambiarFrameInterno();
	        
	        return imeiValor;
	    }
	    
	    private void validarIMEIExterno(String imei) {
	        WebElement label;
	        List<WebElement> filas = driver.findElements(By.xpath("//div[@class='roe-widget-area selected-hardware-offers']"
	                + "//*[@class='characteristic-item force_enabled']"));
	        String texto = properties.getProperty("texto.imei.equipo.externo");
	        for (WebElement selenideElement : filas) {
	            label = selenideElement.findElement(By.xpath(".//span"));
	            if (texto.equals(label.getText())) {
	                WebElement input = selenideElement.findElement(By.xpath(".//input"));
	                input.clear();
	                if(imei==null || imei.isEmpty()) {
	                    imei=obtenerImeiExterno();
	                }
	                imeiDelEquipoIterno=imei;
	                input.sendKeys(imei);
	                WebElement boton = selenideElement
	                        .findElement(By.xpath(".//a[@class='bt_bt tfnecu-validate-imei-button']"));
	                boton.click();
	                ActionsUtil.cargandoFrameInterno(driver);
	                break;
	            }
	        }
	    }
	    
	    private String obtenerImeiInterno(String equipo) throws Exception {
	        cambiarFramePrincipal();
	        WebElement label = ActionsUtil.obtenerElementoSpanTexto(driver,"Bodegas");
	        String linkBodegas=ActionsUtil.obteberLinkElemento(ActionsUtil.parent(label));
	        ActionsUtil.abriNuevaPestanya(driver, linkBodegas);
	        
	        String nombreBodegaCentral = properties.getProperty("nombre.bodega.central");
	        String nombreBodegaActual = properties.getProperty("nombre.bodega.actual");
	        
	        By modalInputBy=By.xpath("//div[@class='ui-dialog-content ui-widget-content']//input[@type='text']");

	        ActionsUtil.filtrarIrBodega(driver, nombreBodegaActual);
	        String imeiInterno=ActionsUtil.filtrarEquipoObtenerImeiInterno(driver,modalInputBy,equipo);
	        
	        if(imeiInterno==null) {
	            driver.get(linkBodegas);
	            ActionsUtil.filtrarIrBodega(driver, nombreBodegaCentral);
	            imeiInterno=ActionsUtil.filtarEquipoObtenerImeiMoverEquipo(driver,modalInputBy,equipo,nombreBodegaActual);
	        }
	        
	        ActionsUtil.cerrarUltimaRegrearPrimeraPestanya(driver);
	        cambiarFrameInterno();
	        return imeiInterno;
	    }
	    
	    public void validarIMEIInterno(String nombreEquipo, String imei) throws Exception {
	        String textoIMEI = properties.getProperty("texto.imei");
	        WebElement input = obtenerElementoEspecificoTabla("tablaequipointerno","inputgenerico",textoIMEI+"*");
	        
	        if (imei == null || imei.isEmpty()) {
	            imei = obtenerImeiInterno(nombreEquipo);
	        }
	        input.clear();
	        input.sendKeys(imei);
	        ActionsUtil.cargandoFrameInterno(driver);
	        buscarPrimerResultado(Boolean.TRUE);
	        ActionsUtil.cargandoFrameInterno(driver);
	        imeiDelEquipoIterno = imei;

	    }
	    
	    public void setObjetoToAction(By objetoToCliked) {
	        objetoToAction = objetoToCliked;
	    }
	    
	    public void sharedObjet(String opcion) {
	        String nombreObjeto = (ActionsUtil.textoMinusculasSinEspacios(opcion));
	        By byObjeto = ActionsUtil.getObjeto(nombreObjeto);
	        setObjetoToAction(byObjeto);
	    }
	    
	    private void localizarPagoCuotasInterno(String coutas) {
	        String texto = properties.getProperty("texto.pago.cuotas");
	        WebElement checkCuotas = driver.findElement(By.xpath(
	                "//div[@class=\"roe-widget-area selected-hardware-offers\"]//table[@class=\"parameters\"]//span[contains(text(),'"
	                        + texto + "')]"));
	        WebElement parent = ActionsUtil.parent(checkCuotas);
	        WebElement inputCuotas = parent.findElement(By.xpath("./input[@class='checkbox']"));
	        inputCuotas.click();
	        ActionsUtil.cargandoFrameInterno(driver);
	        WebElement elementCuota = driver.findElement(By.xpath(
	                "//div[@class=\"roe-widget-area selected-hardware-offers\"]//table[@class=\"parameters\"]//tr[@class=\"characteristic-item force_enabled\"]//select"));

	        Select selectCuota = new Select(elementCuota);
	        selectCuota.selectByVisibleText(coutas);
	        ActionsUtil.cargandoFrameInterno(driver);
	    }
	    
	    public WebElement obtenerElementoEspecificoTabla(final String nombreByTabla,
	            final String nombreByElemento, final String referencia)
	            throws Exception {
	        sharedObjet(nombreByTabla);
	        WebElement filaColumna = ActionsUtil.obtenerElementoReferenciaTabla(
	                driver, getObjetoToAction(), referencia);
	        sharedObjet(nombreByElemento);
	        return filaColumna.findElement(getObjetoToAction());
	    }
	    
	    public void validarEquipoIngresarImeiNumeroCuotas(String nombreEquipo, String imei, String numeroCuota) throws Exception {
	        nombreDelEquipoRenovacion = nombreEquipo;
	        imeiDelEquipoRenovacion = imei;

	        String equipoExterno = properties.getProperty("texto.equipo.externo");
	        nombreDelEquipoInterno=nombreEquipo;
	        if (equipoExterno.equals(nombreEquipo)) {
	            bdTipoEquipo=equipoExterno;
	            // TODO mejorar xphat
	            By agregar = By.xpath("//*[@id=\"Celulares\"]/div/div[1]/div/table/tbody/tr[1]/td[3]/a");
	            ActionsUtil.esperarPoderClicBy(driver, agregar);
	            ActionsUtil.clic(driver, agregar);
	            ActionsUtil.cargandoFrameInterno(driver);
	            validarIMEIExterno(imei);
	        } else {
	            bdTipoEquipo="Equipo Interno";
	            equipoInterno(nombreEquipo, false);
	            validarIMEIInterno(nombreEquipo,imei);
	            if (!esPrepago && !"0".equals(numeroCuota)) {
	                localizarPagoCuotasInterno(numeroCuota);
	            }
	        }
	    }
	    
	    public void validarEquipoIngresarImei (String nombreEquipo, String imei) throws Exception {
	    	 
	    		bdTipoEquipo="Equipo Interno";
	            equipoInterno(nombreEquipo, false);
	            validarIMEIInterno(nombreEquipo,imei);
	            
	    }
	    
	    public By getObjetoToAction() {
	        return objetoToAction;
	    }
	    
	    public void validarRenovacionDownpayment(final String cuotas,
	            final String valorRenovacion, final String cuotaInicial)
	            throws Exception {
	        if (valorRenovacion != null && !valorRenovacion.isEmpty()) {
	            String textoRenovacion = properties.getProperty("texto.renovacion");
	            WebElement elemento = obtenerElementoEspecificoTabla(
	                    "tablaequipointerno", "selectgenerico",
	                    textoRenovacion + "*");
	            Select elementoSelect = new Select(elemento);
	            elementoSelect.selectByVisibleText(valorRenovacion);
	            ActionsUtil.cargandoFrameInterno(driver);
	        }

	        if (!esPrepago && !"0".equals(cuotas)) {
	            String numeroCuotaHabilitar = properties
	                    .getProperty("numero.cuota.habilita.inicial");
	            if (cuotaInicial != null && !cuotaInicial.isEmpty()
	                    && (numeroCuotaHabilitar.contains(cuotaInicial))) {
	                sharedObjet("cuotainicial");
	                WebElement input = driver.findElement(getObjetoToAction());
	                input.sendKeys(cuotaInicial);
	                input.sendKeys(Keys.TAB);
	                ActionsUtil.cargandoFrameInterno(driver);
	            }

	        }
	    }
	    
	    public void validarCrearCliente(String cliente) {
	        switch (cliente) {
	        case "0":
	            break;
	        case "1":
	            /* pestaña - Facturación y pago */
	            migaPanFacturacionPago();
	            existeCuentaFacturacion();
	            break;
	        default:
	            // throw new Exception("no esta contemplada la opcion de cliente: " + cliente);
	        }

	    }
	    
	    public void migaPanFacturacionPago() {
	        String texto = properties.getProperty("miga.pan.facturacion.pago");
	        localizarMigaDePan(texto);
	    }
	    
	    private void localizarMigaDePan(String migaPan) {
	        List<WebElement> pestania = driver.findElements(By.xpath("//a[@class=\"gwt-InlineHyperlink roe-pathList\"]"));
	        for (WebElement selenideElement : pestania) {
	            if (migaPan.equals(selenideElement.getText())) {
	                selenideElement.click();
	                ActionsUtil.cargandoFrameInterno(driver);
	                if (!"act".equals(ActionsUtil.parent(selenideElement).getAttribute("class"))) {
	                    selenideElement.click();
	                    ActionsUtil.cargandoFrameInterno(driver);
	                }
	                return;
	            }
	        }
	    }
	    
	    public void existeCuentaFacturacion() {
	        ActionsUtil.sleepSeconds(2);
	        By selectError = By.xpath("//select[@class='input-errorHighlight']");
	        WebElement selectElement = ActionsUtil.validacionInmediata(driver, selectError);
	        if (selectElement != null) {
	            botonNuevaCuentaFacturacion();
	            System.out.println("datos del select");
	            ActionsUtil.cargandoFrameInterno(driver);
	            selectElement = driver.findElement(selectError);
	            Select select = new Select(selectElement);
	            for (WebElement elemento : select.getAllSelectedOptions()) {
	                System.out.println(elemento.getText());
	            }
	            select.selectByIndex(1);
	            ActionsUtil.cargandoFrameInterno(driver);
	        }
	    }
	    
	    public void botonNuevaCuentaFacturacion() {

	        String texto = properties.getProperty("boton.crear");
	        String xpahtBotonCrear = "//div[@class=\"ui-widget-overlay-under-wrapper\"]//button[contains(text(),'" + texto
	                + "')]";
	        String botonNuevaCuentaFacturacion = null;
	        if (esPrepago) {
	            botonNuevaCuentaFacturacion = "boton.nueva.cuenta.facturacion.prepago";
	        } else {
	            botonNuevaCuentaFacturacion = "boton.nueva.cuenta.facturacion.pospago";
	        }

	        clicBotonTexto(botonNuevaCuentaFacturacion);
	        ActionsUtil.cargandoFrameInterno(driver);
	        By boton = By.xpath(xpahtBotonCrear);
	        ActionsUtil.esperarPoderClicBy(driver, boton);
	        ActionsUtil.clic(driver, boton);
	        ActionsUtil.cargandoFrameInterno(driver);

	    }
	    
	    public void clicBotonTexto(String texto) {
	        texto = properties.getProperty(texto);
	        // TODO mover a la clase ObjetosPlmLogin.java
	        By boton = By.xpath("//button[text()='" + texto + "']");
	        ActionsUtil.esperarPoderClicBy(driver, boton);
	        ActionsUtil.clic(driver, boton);
	    }
	    
	    public void migaPanRevision() {
	        String texto = properties.getProperty("miga.pan.revision");
	        localizarMigaDePan(texto);
	    }
	    
	    public void clicBotonFinalProcesoReservarActivo() {
	        String texto = properties.getProperty("boton.final.reservar.activos");
	        clicBotonFinalProceso(texto);
	        ActionsUtil.cargandoFrameInterno(driver);
	    }
	    
	    private List<Object> asignarParametrosResultadoFlujo(String nombreFlujo, String idFLujoTabla) {
	        List<Object> parametros= new ArrayList<>();
	        parametros.add(url);
	        parametros.add(estadoOrden);
	        parametros.add(cuentaFacturacion);
	        parametros.add(numeroTelefono);
	        parametros.add(simCardInicial);//sim
	        parametros.add(procesadoCuando);
	        parametros.add(nombreCliente);
	        parametros.add(nombreFlujo);
	        parametros.add(parametrosJson);
	        parametros.add(properties.getProperty("nc.user"));
	        parametros.add(bdPlan);//plan actual
	        parametros.add(bdTipoEquipo);//tipo equipop
	        parametros.add(nombreDelEquipoInterno);
	        parametros.add(nombreDelEquipoInterno);
	        parametros.add(imeiDelEquipoIterno);
	        parametros.add(sloActivados);
	        parametros.add(planActual);
	        parametros.add(planNuevo);
	        parametros.add(simcardVieja);
	        parametros.add(simcardNueva);
	        parametros.add(numeroAntiguo);
	        parametros.add(numeroNuevo);
	        parametros.add(nombreDelEquipoRenovacion);
	        parametros.add(nombreDelEquipoRenovacion);
	        parametros.add(imeiDelEquipoRenovacion);
	        parametros.add(nombreFlujo);//FLUJO
	        parametros.add(idFLujoTabla);//ID tabla ejecutada
	        parametros.add(fechaSegundosMilisegundos);
	        parametros.add(bdNumeroCuentaCliente);//"Número de cuenta del cliente"
	        parametros.add(bdClientePospago);//"Número cuenta prepago"
	        parametros.add(bdClientePospago);//"Número cuenta pospago"
	        parametros.add(bdClientePospagoInAdvance);//"Número Cuenta pospago in advance"
	        parametros.add(categoria);
	        parametros.add(bdNumeroDocumento);//"identificación"
	        parametros.add(nombre);//"nombre completo"
	        parametros.add(tipo_cuenta);//"tipo cuenta"
	        parametros.add(ciclo);//"ciclo"
	        parametros.add(fechaCreacion);
	        parametros.add(new Date());
	        return parametros;
	    }
	    
	    public void clicBotonFinalProceso(String nombreBoton) {
	        WebElement botonFinalProceso = botonFinalesProceso(nombreBoton);
	        if (botonFinalProceso != null && !botonFinalProceso.getAttribute("class").contains("disabled")) {
	            botonFinalProceso.click();
	        } else {
//	           System.out.println();
	            System.err.println("no encontro el boton:" + nombreBoton);
	        }

	    }
	    
	    private static void actualizarRegistroProcesado(String nombreFlujo, String idFLujoTabla) {
	        operacionesBD.abrirConexionBD();
	        String consulta=null;
	        if(nombreFlujo.equals("SUSPENCION_LINEA")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_SUSPENDER_REANUDAR_ID;
	        }
	        else if(nombreFlujo.equals("TRANSFERENCIA_BENEFICIARIO")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_TRASNFERENCIA_BENEFICIARIO_ID;
	        }
	        else if(nombreFlujo.equals("ACTIVACION_SLO")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_ACTIVACION_SLO_ID;
	        }
	        else if(nombreFlujo.equals("ALTAS_LINEAS")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_ALTAS_LINEAS_ID;
	        }
	        else if(nombreFlujo.equals("BAJA_ABONADO")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_BAJA;
	        }
	        else if(nombreFlujo.equals("CAMBIO_NUMERO")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_CAMBIO_NUMERO_ID;
	        }
	        else if(nombreFlujo.equals("CAMBIO_PLAN")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_CAMBIO_PLAN_ID;
	        }
	        else if(nombreFlujo.equals("CAMBIO_SIM")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_CABMIO_SIM;
	        }
	        else if(nombreFlujo.equals("CREAR_CLIENTE")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_CREAR_CLIENTE_ID;
	        }
	        else if(nombreFlujo.equals("FAC_EQUIPO_SIN_LINEA")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_FAC_EQUIPO_SIN_LINEA_ID;
	        }
	        else if(nombreFlujo.equals("RENOVACION_EQUIPO")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_RENOVACION_EQUIPO_ID;
	        }
	        else if(nombreFlujo.equals("FACTURAS_MISCELANEAS")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_FACTURAS_MISCELANEAS_ID;
	        }
	        else if(nombreFlujo.equals("GENERACION_DISPUTAS")){
	            consulta=ConsultasDinamicasConstans.UPDATE_OTC_T_GENERACION_DISPUTAS_ID;
	        }
	        operacionesBD.updateTablasEntrada(consulta,ProcesadoEnum.PROCESADO.name(), Long.parseLong(idFLujoTabla));
	        
	        
	    }
	    
	    public WebElement botonFinalesProceso(String botonFinalReservarActivos) {
	        By listaBotones = By.xpath(
	                "//li[@class='mobile-contracts__list-item']//div[@class='contracts__button-wrap'][not(contains(@style, 'display: none'))]//button");

	        List<WebElement> elemento = driver.findElements(listaBotones);

	        if (!elemento.isEmpty()) {
	            ActionsUtil.esperarVisibleBy(driver, listaBotones);
	        }

	        for (WebElement selenideElement : elemento) {
	            if (botonFinalReservarActivos.equals(selenideElement.getText())) {
	                return selenideElement;
	            }
	        }
	        // sino existe el boton con el texto
	        return null;
	    }
	    
	    public void clicBotonFinalProcesoConfigurarContrato() {
	        String texto = properties.getProperty("boton.final.configurar.contrato");
	        if (!esPrepago) {
	            clicBotonFinalProceso(texto);
	            /* Modo de gestión de contratos */
	            ActionsUtil.botonOkConfirmar(driver);
	            ActionsUtil.cargandoFrameInterno(driver);
	        }
	    }
	    
	    public void clicBotonFinalProcesoGenerarFactura() {
	        String texto = properties.getProperty("boton.final.generar.factura");
	        clicBotonFinalProceso(texto);
	        ActionsUtil.cargandoFrameInterno(driver);
	        /* boton ok */
	        ActionsUtil.botonOkConfirmar(driver);
	        ActionsUtil.cargandoFrameInterno(driver);
	        /* confirmar */
	        ActionsUtil.botonOkConfirmar(driver);
	        ActionsUtil.cargandoFrameInterno(driver);
	    }
	    
	    public void clicBotonFinalEnviar() {

	        By botonEnviar = By.xpath("//button[@class='mobile-contracts__submit-button']");
	        ActionsUtil.esperarPoderClicBy(driver, botonEnviar);
	        ActionsUtil.clic(driver, botonEnviar);
	        ActionsUtil.cargandoFrameInterno(driver);
	        ActionsUtil.botonOkConfirmar(driver);
	        ActionsUtil.cargandoFrameInterno(driver);

	        WebElement elemento = ActionsUtil.validacionInmediata(driver, botonEnviar);
	        if (elemento != null) {
	            ActionsUtil.sleepSeconds(8);
	            elemento.click();
	            ActionsUtil.cargandoFrameInterno(driver);
	            ActionsUtil.botonOkConfirmar(driver);
	            ActionsUtil.cargandoFrameInterno(driver);
	        }
	    }
	    
	    public void extraerNumeroordendeventa() throws Exception {
	    	
	        By orndeVenta = By.xpath("//div[@class=\"roe-widget-header\"]//h1[contains(text(),'La orden de venta')]");
	        ActionsUtil.esperarVisibleBy(driver, orndeVenta);
	        String ordenVenta = driver.findElement(orndeVenta).getText();
	        LOGGER.info(ordenVenta);
	        numeroOrdenVenta = ordenVenta.substring(19, 29);
	        LOGGER.info(numeroOrdenVenta);
	        driver.switchTo().defaultContent();
	        By ocultarBorradorTicket = By.xpath("//button[@class=\"gwt-Button ticket-panel-collapse-button expanded\"]");
	        ActionsUtil.clic(driver, ocultarBorradorTicket);
	        
	        }
	    
	    public void volveralaOrden() {
	    	
	        ActionsUtil.sleepSeconds(1);
	        cambiarFrameInterno();
	        ActionsUtil.sleepSeconds(1);
	        By optionVolverOrden = By.xpath("//a[contains(text(),\"Volver a la orden\")]");
	        ActionsUtil.esperarPoderClicBy(driver, optionVolverOrden);
	        ActionsUtil.clic(driver, optionVolverOrden);
	        ActionsUtil.sleepSeconds(1);
	        
	        }
	    
	    public void entradadeOrden() throws InterruptedException {
	    	
	        ActionsUtil.sleepSeconds(1);
	        driver.switchTo().defaultContent();
	        ActionsUtil.sleepSeconds(1);
	        ActionsUtil.ocultarMensajeNotificacionAutomatico(driver);
	        By botonExpandir = By.xpath("//button[@class=\"gwt-Button left-panel-collapse-button collapsed\" "
	                    + "or @class=\"left-panel-collapse-button collapsed gwt-Button\" "
	                    + "or @class=\"collapsed left-panel-collapse-button gwt-Button\"]");
	        ActionsUtil.esperarPoderClicBy(driver, botonExpandir);
	        ActionsUtil.clic(driver, botonExpandir);
	        ActionsUtil.sleepSeconds(1);
	        By optionEntradaOrden = By.xpath("//div[@title=\"Entrada de orden\"]");
	        ActionsUtil.esperarPoderClicBy(driver, optionEntradaOrden);
	        ActionsUtil.clic(driver, optionEntradaOrden);
	        ActionsUtil.sleepSeconds(1);
	        
	        }
	    
	    public WebDriver getDriver() {
	        return driver;
	        }
	    
	    public void seleccionarlaOrdenAnterior() throws Exception {
	    	
	        List<WebElement> items = driver
	                    .findElements(By.xpath("//div[@class=\"ui-pager-gwt\"]//div[@class=\"gwt-HTML\"]"));
	        if (!items.isEmpty()) {
	          JavascriptExecutor executor = (JavascriptExecutor) getDriver();
	          executor.executeScript("arguments[0].scrollIntoView(true);", items.get(0));
	            }

	        List<WebElement> filas = driver.findElements(By.xpath("//table[@class=\"GF5QP-ADOM TableCtrl\"]"));
	        WebElement referencia = null;
	        for (WebElement selenideElement : filas) {
	          if (selenideElement.getText().contains(numeroOrdenVenta)) {
	            referencia = selenideElement.findElement(By.xpath("//a[contains(text(),'" + numeroOrdenVenta + "')]"));
	            JavascriptExecutor executor = (JavascriptExecutor) getDriver();
	            ActionsUtil.sleepSeconds(1);
	            executor.executeScript("arguments[0].click();", referencia);
	            return;
	                }
	            }
	            throw new Exception("El numero " + numeroOrdenVenta + " No esta ");
	        }
	    
	    public void verificarNumerodeCuentayGuardarURL() throws Exception {
	        String elementOrdenVenta = driver.findElement(By.xpath("//div[@class=\"gwt-Label nc-uip-page-caption\"]"))
	                .getText();
	        SubelementOrdenVenta = elementOrdenVenta.substring(16, 26);
	        System.out.println(SubelementOrdenVenta);

	        if (numeroOrdenVenta.equals(SubelementOrdenVenta)) {
	            url = driver.getCurrentUrl();
	            System.out.println(url);
	        } else {
	            throw new Exception("La orden de venta no es igual");
	        }
	    }
	    
	    public void mostarOperador() {
	        // TODO elimiar al final de la evidencias
	        String usuario = properties.getProperty("nc.user");
	        By usuarioBy = By.xpath("//li[@class='separate']/a[contains(text(),'" + usuario + "')]");
	        ActionsUtil.clic(driver, usuarioBy);
	    }
	    
	    public void guardarResultadoFlujo(String nombreFlujo, String idFLujoTabla) {
	        //se guarda por BD
	          operacionesBD.abrirConexionBD();
	          List<Object> parametros= asignarParametrosResultadoFlujo(nombreFlujo, idFLujoTabla);
	          operacionesBD.insertarRegistroAutomatizacion(parametros);
	          actualizarRegistroProcesado(nombreFlujo, idFLujoTabla);
	          listaIdsProcesados.add(idFLujoTabla);
	          ReportesUtil.setListaIdsProcesados(listaIdsProcesados);
	          
	      }
	    
	    /**
	     * Método para cargar los valores iniciales de ejecucion de feature
	     * @param tabla
	     * @param id
	    */
	    public static Map<String, String> consultarTablaEntradaFlujo(String tabla, String id, String nombreFeature, String nombreFlujo, String nombreTag) {
	        Map<String, String> mapaEntradaBD= new HashMap<>();
	        operacionesBD.abrirConexionBD();
	        String consulta=null;
	        List<Object> params= new ArrayList<>();
	        params.add(Long.parseLong(id));
	        if(TablaEnum.OTC_T_ALTAS_LINEAS.name().equals(tabla)){
	            consulta=ConsultasDinamicasConstans.SELECT_OTC_T_ALTAS_LINEAS_ID;
	            
	            ResultSet rs = operacionesBD.ejecutarConsulta2(consulta, params);
		        try {
		             if (rs != null && rs.next()) {
		                 llenarMapa(rs,mapaEntradaBD);
		                }
		             else{
		                 operacionesBD.cerrarStatement();
		                 cerrarNavegador();
		             }
		        } catch (SQLException e) {
		            LOGGER.error("Se presento un error al consultar las propiedades", e);
		        } finally {
		            if (rs != null)
		                try {
		                    rs.close();
		                } catch (SQLException e) {
		                    LOGGER.error("Error cerrando el resultSet al init", e);
		                }
		        }
		        operacionesBD.cerrarStatement();
		        
	        }else if(TablaEnum.OTC_T_PARAMETRO.name().equals(tabla)){
	            //consulta=ConsultasDinamicasConstans.SELECT_OTC_T_ACTIVACION_SLO_ID;
	        	mapaEntradaBD.put("feature", nombreFeature);
	            
	        }
	        
	        String procesado=mapaEntradaBD.get("procesado");
	        properties.setProperty("isProcesado", "false");
	        if(procesado!=null && procesado.equalsIgnoreCase(ProcesadoEnum.PROCESADO.name())){
	            LOGGER.info("Se finaliza el flujo ya que el registro ya fue procesado");
	            properties.setProperty("isProcesado", "true");
	            cerrarNavegador();
	        }
	        
	        //se guarda el resultado
	        guardarResultadoFlujo(nombreTag,nombreFeature,nombreFlujo, mapaEntradaBD.get("id"));
	        
	        return mapaEntradaBD;
	        
	    }
	    
	    public static void guardarResultadoFlujo(String nombreTag, String nombreFeature, String nombreFlujo, String idFLujoTabla) {
	        //se guarda por BD
	    	  ActionsUtil.setIdEjecucion(idFLujoTabla);
	          operacionesBD.abrirConexionBD();
	          List<Object> parametros= asignarParametrosResultadoFlujo(nombreTag,nombreFeature, nombreFlujo, idFLujoTabla);
	          operacionesBD.insertarRegistroAutomatizacion(parametros);
	          //actualizarRegistroProcesado(nombreFlujo, idFLujoTabla);
	          listaIdsProcesados.add(idFLujoTabla);
	          ReportesUtil.setListaIdsProcesados(listaIdsProcesados);
	          
	      }
	    
	    private static List<Object> asignarParametrosResultadoFlujo(String nombreTag,String nombreFeature, String nombreFlujo, String idFLujoTabla) {
	    	AsignarParametrosSalidaInDTO inDTO = new AsignarParametrosSalidaInDTO();
	    	//inDTO.setId(ConsultasDinamicasConstans.NEXT_AUTOMATIZACIONES);
	    	boolean falla = false;
	    	inDTO.setId(idFLujoTabla);
	    	inDTO.setFecha(fechaCreacion);
	    	inDTO.setNombreFeature(nombreFeature);
	    	inDTO.setNombrescenario(nombreFlujo);
	    	inDTO.setTagsfeature(nombreTag);
	    	inDTO.setParametrosJson(null);
	    	inDTO.setFallascenario(falla);
	    		    	
	        return ActionsUtil.asignarParametrosSalida(inDTO);
	    }
	    
	    public static void agregaMapaJson(String key, String value) {	
							
			mapaJson.put(key, value);	
		}
	    
	    /**
	     * La funcion valida si las listas contienen la misma informacion segun lo que hay 
	     * en Netkraken y lo que se toma del archivo MEF
	     * 
	     * @param listValoresParaCuotas
	     * @param litValoresMontoCuota
	     */
	    public static void validarValoresCuotas(Reporte reporte, 
	    		ArrayList<Map<String, String>> listValoresParaCuotas, ArrayList<Map<String, String>> litValoresMontoCuota) {
	    	System.out.println(listValoresParaCuotas);
	    	System.out.println(litValoresMontoCuota);
	    	int cantidadIguales = 0;
	    	
	    	
	    	
	    	for (Map<String, String> entry : listValoresParaCuotas) {
	    		
	    		cantidadIguales = validarValoresMapa(reporte, entry, cantidadIguales, 
	    			ConstantesGlobales.OBTENIDO, ConstantesGlobales.ESPERADO,
	    			properties.getProperty("netkraker.equipo.montototal.diferentealesperado") +
					" Esperado : ", " obtenido : ","Validacion_PrecioMEF");
	    	}
	    	
	    	for (Map<String, String> entry : litValoresMontoCuota) {
	    		
	    		cantidadIguales = validarValoresMapa(reporte, entry, cantidadIguales, 
	    				ConstantesGlobales.VALOR_MODELO, ConstantesGlobales.MONTO_TOTAL_NO_FACTURADO,
	    				properties.getProperty("netkraker.equipo.montototal.nofacturado.diferente") +
	    				" Monto no facturado : ", " monto calculado : ","Validacion_MontoTotalFacturado");
	    		
	    		cantidadIguales = validarValoresMapa(reporte, entry, cantidadIguales, 
	    				ConstantesGlobales.MONTO_CUOTA_MODELO_CALCULADO, ConstantesGlobales.MONTO_CUOTA_MODELO,
	    				properties.getProperty("netkraker.equipo.montototal.cuotas.diferente") +
	    				" Monto de cuota : ", " monto del modelo : ","Validacion_MontoCuota");
	    	}
	    	
	    	guardarMensajeEnInforme(reporte, true);  
	    	
	    	
	    	if(cantidadIguales != (listValoresParaCuotas.size() + litValoresMontoCuota.size())) {
	    		assertTrue(false);
	    	}
	    }
	    
	    /**
	     * Funcion que realiza la valicion de un par de valores, uno obtenido de
	     * Netkraken y otro del archivo MEF
	     * 
	     * @param reporte
	     * @param entry
	     * @param cantidadIguales
	     * @param valor1
	     * @param valor2
	     * @param msg1
	     * @param msg2
	     * @return
	     */
	    private static int validarValoresMapa(Reporte reporte, Map<String, String> entry, 
	    		int cantidadIguales, String valor1, String valor2, String msg1, String msg2,String keyMsg) {
	    	
	    	if(entry.containsKey(valor1) && entry.containsKey(valor2)) {
	    		
				if(entry.get(valor1).equals(entry.get(valor2))) {
	    			cantidadIguales++;
	    			
	    			if(entry.get(valor1).isEmpty() || entry.get(valor2).isEmpty()) {
	    				assertTrue(entry.get(valor1).equals(entry.get(valor2)));
	    			}else {
		    			assertThat(entry.get(valor1), CoreMatchers.containsString(entry.get(valor2)));
		    		}
	    			reporte.setfallaScenario("false");  
	    			agregaMapaJson(keyMsg+"_Cuota_"+ entry.get("Cuota") ,"OK");
	    		}
	    		else {
	    			String mensaje = msg1 + entry.get(valor2) + msg2 + entry.get(valor1);
	    			agregaMapaJson(keyMsg+"_Cuota_" + entry.get("Cuota") ,mensaje);
	    			reporte.setfallaScenario("true");  			
	    			
	    		}
				
			}
	    	return cantidadIguales;
	    }
	    
	    public static void guardarMensajeEnInforme(Reporte reporte, Boolean pasaScenario) {
	    	//Reporte rep = new Reporte(reporte.getNombreFeature(), reporte.getNombreScenario(), reporte.getTagsFeature(), mensaje, pasaScenario);
	    	//agregaMapaJson("mensaje",mensaje);
	    	
	    	asignoParametros(mapaJson);
	    	reporte.setMensaje(parametrosJson);
	    	
			ReporteController.guardarEnReporte(reporte);
			
	    }
	    
	    public static void asignoParametros(Map<String, String> mapaEntradaBD) {
	        try {
	            Gson gson = new Gson(); 
	            Map<String, String> treeMap = new TreeMap<String, String>(mapaEntradaBD);
	            parametrosJson=gson.toJson(treeMap);
	       }catch (Exception err){
	          LOGGER.error("Se presento un error al castear el string a json", err);
	       }
	        
	    }
	    
	    public static String devuelveValorMapaJson(String key) {	
			
			return mapaJson.get(key);
		}

	}

