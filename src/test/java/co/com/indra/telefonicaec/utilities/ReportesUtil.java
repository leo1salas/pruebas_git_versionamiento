package co.com.indra.telefonicaec.utilities;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.com.indra.telefonicaec.constants.ConstantesBD;
import co.com.indra.telefonicaec.constants.FormatConstans;
import co.com.indra.telefonicaec.entities.Reporte;

import java.util.ArrayList;
import java.util.Arrays;

public class ReportesUtil {

	 static OperacionesBD operacionesBD = new OperacionesBD();
	    static PropertiesLoader properties = PropertiesLoader.getInstance();
	    static Logger LOGGER = (Logger) LoggerFactory.getLogger(ReportesUtil.class);
	    static String fechaSegundosMiligesundos;
	    //static List listaIdsProcesados;
	    static List<String> listaIdsProcesados;

	
	 public static void setFechaSegundosMiligesundos(String fechaSegundosMiligesundos) {
	        ReportesUtil.fechaSegundosMiligesundos = fechaSegundosMiligesundos;
	 }
	 
	 public static List<String> getListaIdsProcesados() {
	        return listaIdsProcesados;
	    }
	 
	 public static void setListaIdsProcesados(List<String> listaIdsProcesados) {
	        ReportesUtil.listaIdsProcesados = listaIdsProcesados;
	    }
	 
	 /**
     * Método responsable consultar las evidencias generadas en el proceso
     * @param fechaSegundosMilisegundos
     * @return
     */
	 public static void verificarExisteRutaEvidencia() {
	        File evidencia = new File(properties.getProperty("rutaArchivos") + getFechaSegundosMiligesundos());
	        if (!evidencia.exists()) {
	            evidencia.mkdir();
	        }

	    }
	 /**
     * Método que obtiene el valor de fechaSegundosMiligesundos
     * 
     * @return the fechaSegundosMiligesundos
     */
	 public static String getFechaSegundosMiligesundos() {
	        return fechaSegundosMiligesundos;
	    }
	 
	 /**
     * 
     * Método responsable consultar las evidencias generadas en el proceso
     * @param fechaSegundosMilisegundos
     * @return
     */
	 public static List<File> consultarImagenesProceso() {
        // se consulta la carpeta evidencias
        File evidencia = new File(properties.getProperty("rutaArchivos") + getFechaSegundosMiligesundos());
        if (evidencia.exists()) {
            return new ArrayList<File>(Arrays.asList(evidencia.listFiles()));
        }
        return null;

    }
	 
	 public static File generarReporteAutomatizacionesBD() {
	        return rerpoteGeneral();
	    }

	 /**
     * 
     * Método responsable de la generacion del reporte
     */
    private static File rerpoteGeneral() {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        workbook.setSheetName(0, "Reporte General");
        //URL url de inicio
        //ID_EJECUCION tabla hija que se ejecuto
        //DATOS_ENTRADA= JSON con los parametros de entrada
        //FECHA_INICIO fecha en q comenzo el robot
        //FECHA_FIN fecha que termino el robot
        //PLATAFORMA si es NC o SSP
        //URL_ANTES_ERROR = Direccion antes del error
        //URL_ERROR =  si es error de url se guarda con imagen
        String[] headers = new String[] { "ID", "FECHA","CANAL", "NOMBREFEATURE", "NOMBRESCENARIO","TAGSFEATURE", "MENSAJE", "FALLASCENARIO"};

        // consulto la BD
        List<Reporte> automatizaciones = consultarAutomatizaciones();

        CellStyle headerStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        headerStyle.setFont(font);

        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFRow headerRow = sheet.createRow(0);
        for (int j = 0; j < headers.length; ++j) {
            String header = headers[j];
            XSSFCell cell = headerRow.createCell(j);
            cell.setCellStyle(headerStyle);
            cell.setCellValue(header);
        }

        XSSFRow dataRow = null;
        //SimpleDateFormat fecha = new SimpleDateFormat(FormatConstans.FORMATO_FECHA_HORA);
        for (int i = 0; i < automatizaciones.size(); i++) {
            dataRow = sheet.createRow(i + 1);
            dataRow.createCell(FormatConstans.CERO).setCellValue(automatizaciones.get(i).getId());
            dataRow.createCell(FormatConstans.UNO).setCellValue(automatizaciones.get(i).getFecha());
            dataRow.createCell(FormatConstans.DOS).setCellValue(automatizaciones.get(i).getCanal());
            dataRow.createCell(FormatConstans.TRES).setCellValue(automatizaciones.get(i).getNombreFeature());
            dataRow.createCell(FormatConstans.CUATRO).setCellValue(automatizaciones.get(i).getNombreScenario());
            dataRow.createCell(FormatConstans.CINCO).setCellValue(automatizaciones.get(i).getTagsFeature());
            dataRow.createCell(FormatConstans.SEIS).setCellValue(automatizaciones.get(i).getMensaje());
            dataRow.createCell(FormatConstans.SIETE).setCellValue(automatizaciones.get(i).getfallaScenario());
        }

        // se crea una hoja por cada flujo
        List<Reporte> automatizacionesHija = null;
        XSSFSheet sheetHija = null;
        
        // POSPAGO
        automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FormatConstans.Pospago);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(FormatConstans.UNO,FormatConstans.Pospago);
        sheetHija = procesarHojaHija(sheetHija, workbook, automatizacionesHija);
        // PREPAGO
        automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FormatConstans.Prepago);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(FormatConstans.DOS, FormatConstans.Prepago);
        sheetHija = procesarHojaHija(sheetHija, workbook, automatizacionesHija);
        // Cambio plan
        automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FormatConstans.Retail);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(FormatConstans.TRES, FormatConstans.Retail);
        sheetHija = procesarHojaHija(sheetHija, workbook, automatizacionesHija);
        // Baja abonados
        automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FormatConstans.PlanFamilia);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(FormatConstans.CUATRO, FormatConstans.PlanFamilia);
        sheetHija = procesarHojaHija(sheetHija, workbook, automatizacionesHija);
        // Facturas miselane
        automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FormatConstans.Mayorista);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(FormatConstans.CINCO, FormatConstans.Mayorista);
        sheetHija = procesarHojaHija(sheetHija, workbook, automatizacionesHija);
        // Renovación equipo
        automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FormatConstans.Financiamiento);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(FormatConstans.SEIS, FormatConstans.Financiamiento);
        sheetHija = procesarHojaHija(sheetHija, workbook, automatizacionesHija);
        // Suspensioón linea
        automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FormatConstans.Empresas);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(FormatConstans.SIETE, FormatConstans.Empresas);
        sheetHija = procesarHojaHija(sheetHija, workbook, automatizacionesHija);
        // Activación slo
        automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FormatConstans.OnLine);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(FormatConstans.OCHO,FormatConstans.OnLine);
        sheetHija = procesarHojaHija(sheetHija, workbook, automatizacionesHija);
        // Altas
        automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FormatConstans.Altas);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(FormatConstans.NUEVE, FormatConstans.Altas);
        sheetHija = procesarHojaHija(sheetHija, workbook, automatizacionesHija);
     // ssp
        automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FormatConstans.ssp);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(FormatConstans.DIEZ, FormatConstans.ssp);
        sheetHija = procesarHojaHija(sheetHija, workbook, automatizacionesHija);
        // crear cliente
    /*    automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FlujoEnum.CREAR_CLIENTE);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(NumeroConstans.DIEZ, "Creación Cliente");
        sheetHija = procesarHojaHijaCliente(sheetHija, workbook, automatizacionesHija);
        // Cambio numero
        automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FlujoEnum.CAMBIO_NUMERO);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(NumeroConstans.ONCE, "Cambio Número");
        sheetHija = procesarHojaHijaNumero(sheetHija, workbook, automatizacionesHija);
        // Altas ssp
        automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FlujoEnum.ALTAS_LINEAS_SSP);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(NumeroConstans.DOCE, "Altas SSP");
        sheetHija = procesarHojaHijaAltaSsp(sheetHija, workbook, automatizacionesHija);
        // 
        automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FlujoEnum.CREAR_CLIENTE_EMPRESARIAL);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(NumeroConstans.TRECE, "Cliente Empresarial");
        sheetHija = procesarHojaHijaClienteEmpresarial(sheetHija, workbook, automatizacionesHija);

        // generación disputas y nota crédito
        automatizacionesHija = filtrarAutomatizaciones(automatizaciones, FlujoEnum.GENERACION_DISPUTAS_Y_NOTACREDITO);
        sheetHija = workbook.createSheet();
        workbook.setSheetName(NumeroConstans.CATORCE, "Disputas y Nota Crédito");
        sheetHija = procesarHojaHijaDisputasNotaCredito(sheetHija, workbook, automatizacionesHija);
                
       */ 
        
        // Resize all columns to fit the content size
        for (int i = 0; i < 7; i++) {
            sheet.autoSizeColumn(i);
        }
        String ruta = properties.getProperty("rutaArchivos") + getFechaSegundosMiligesundos() + "\\"
                + properties.getProperty("nombreXls") + ".xlsx";
        FileOutputStream file;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            file = new FileOutputStream(ruta);
            workbook.write(file);
            file.close();
        } catch (FileNotFoundException e) {
        	LOGGER.error("No se encontro el archivo ", e);
        } catch (IOException e) {
            LOGGER.error("IOException ", e);
        }
        return new File(ruta);

    }
    
    private static List<Reporte> consultarAutomatizaciones() {
        operacionesBD.abrirConexionBD();
        List<Reporte> listaDTO = new ArrayList<>();
        List<Object> param = new ArrayList<>();
        param.add(getFechaSegundosMiligesundos());
        ResultSet rs = operacionesBD.ejecutarConsulta(ConstantesBD.CONSULTAR_TABLA_REPORTE_ACTUAL, param);
        try {
        	Reporte dto = null;
            int j=1;
            if (rs != null) {
                while (rs.next()) {
                    j=1;
                    dto = new Reporte(rs.getLong(j++),rs.getString(j++),
                            		  rs.getString(j++), rs.getString(j++),
                        		  	  rs.getString(j++), rs.getString(j++),
                    		  	  	  rs.getString(j++), rs.getString(j++) );
                    listaDTO.add(dto);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Se presento un error al consultar las propiedades", e);
        } finally {
            if (rs != null)
                try {
                    rs.close();
                } catch (SQLException e) {
                    LOGGER.error("Error cerrando el resultSet al init", e);
                }
        }
        operacionesBD.cerrarStatement();
        return listaDTO;

    }
    
    private static List<Reporte> filtrarAutomatizaciones(List<Reporte> automatizaciones, String flujo)
    {
        List<Reporte> retorno = new ArrayList<>();
        for (Reporte automatizacionesDTO : automatizaciones) {
            if (automatizacionesDTO.getCanal().equals(flujo)) {
                retorno.add(automatizacionesDTO);
            }
        }
        return retorno;
    }
    
    private static XSSFSheet procesarHojaHija(XSSFSheet sheetHija, XSSFWorkbook workbook, 
            List<Reporte> automatizaciones) {

        String[] headersHija = new String[] {"ID", "FECHA", "NOMBREFEATURE", "NOMBRESCENARIO","TAGSFEATURE", "MENSAJE", "FALLASCENARIO"};
        
        CellStyle headerStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        headerStyle.setFont(font);

        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFRow headerRow = sheetHija.createRow(0);
        for (int j = 0; j < headersHija.length; ++j) {
            String header = headersHija[j];
            XSSFCell cell = headerRow.createCell(j);
            cell.setCellStyle(headerStyle);
            cell.setCellValue(header);
        }
        XSSFRow dataRow = null;
        SimpleDateFormat fecha = new SimpleDateFormat(FormatConstans.FORMATO_FECHA_HORA);
        for (int i = 0; i < automatizaciones.size(); i++) {
            dataRow = sheetHija.createRow(i + 1);
            dataRow.createCell(FormatConstans.CERO).setCellValue(automatizaciones.get(i).getId());
            dataRow.createCell(FormatConstans.UNO).setCellValue(automatizaciones.get(i).getFecha());
            dataRow.createCell(FormatConstans.DOS).setCellValue(automatizaciones.get(i).getNombreFeature());
            dataRow.createCell(FormatConstans.TRES).setCellValue(automatizaciones.get(i).getNombreScenario());
            dataRow.createCell(FormatConstans.CUATRO).setCellValue(automatizaciones.get(i).getTagsFeature());
            dataRow.createCell(FormatConstans.CINCO).setCellValue(automatizaciones.get(i).getMensaje());
            dataRow.createCell(FormatConstans.SEIS).setCellValue(automatizaciones.get(i).getfallaScenario());
        }

        // Resize all columns to fit the content size
        for (int i = 0; i < 7; i++) {
            sheetHija.autoSizeColumn(i);
        }
        return sheetHija;

    }
    
}
