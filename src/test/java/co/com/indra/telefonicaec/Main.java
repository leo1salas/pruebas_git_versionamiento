package co.com.indra.telefonicaec;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.com.indra.telefonicaec.constants.ConstantesBD;
import co.com.indra.telefonicaec.utilities.ExcelUtils;
import co.com.indra.telefonicaec.utilities.Generador;
import co.com.indra.telefonicaec.utilities.OperacionesBD;
import co.com.indra.telefonicaec.utilities.PropertiesLoader;

public class Main {

	static ExcelUtils mefExcel;
	static PropertiesLoader properties = PropertiesLoader.getInstance();
	static OperacionesBD operacionesBD = new OperacionesBD();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Main().run();
	}
	
	public void run() {
        try {

            // Leer parametro de ruta de plantilla
            String featureTemplate = System.getProperty("featureTemplate");
        	// Leer parametro de nombre de tarea
            String mefFile = System.getProperty("mefFile");
        	            
            // Leer parametro de nombre de tarea
            
            List<String[]> taskProcess = new ArrayList<String[]>();
            
            if(featureTemplate.contains("*")) {
                taskProcess = getTestPath(featureTemplate.substring(0, featureTemplate.length()-1));
            }else {
                taskProcess.add(new String[] {mefFile, featureTemplate});
            }
            
            proccesFile(taskProcess);

        } catch (Exception exception) {
            System.out.println("Excepción en run(): "+ exception.getMessage());
        }
    }
    
    private void proccesFile(List<String[]> taskProcess) {
        if(taskProcess != null && !taskProcess.isEmpty()) {
            for(String[] elemento: taskProcess) {
                // Carga de datos.
                List<String> data = loadDataTest(elemento[0]);
                // Generacion de archivos.
                Generador.processFile(elemento[1], data);
            }           
        }
    }
    
    private List<String[]> getTestPath(String taskName) {
         return Generador.getFiles(taskName);
        
    }

    private List<String> loadDataTest(String taskName) {
    	
    	consultaryCargarPropiedades();
    	
    	String url = properties.getProperty("nc.xls.url");
		String nombre = properties.getProperty("nc.xls.nombre");
		String EquipoFila =  properties.getProperty("nc.xls.constante.indexFila.equipo");
		String EquipoColumna =  properties.getProperty("nc.xls.constante.indexColumna.equipo");
		
    	if (mefExcel == null) {
			cargarXls(url, nombre);
		}
    	
    	List<String> valExcel = verificarExisteRegistro(EquipoFila, EquipoColumna);
    	
    	return valExcel;
//        OperacionesBD operacionesBD = new OperacionesBD();
//        operacionesBD.abrirConexionBD();
//        return operacionesBD.consultarDisponible(taskName);
    }
    
    private void cargarXls(String url, String nombre) {
		try {
			mefExcel = new ExcelUtils(url, nombre);
		} catch (Exception e) {
			System.out.println("Error leyendo el archivo xls: "+e.getMessage());
		}
	}
    
    public List<String> verificarExisteRegistro(String posicion, String propiedad) {
		Boolean existe = Boolean.TRUE;
		int fila = Integer.parseInt(posicion);
		List<String> listReg = new ArrayList<String>();
		
		if (mefExcel != null) {
			try {
				do
				{
					String valor1 = mefExcel.getValueMain(fila, Integer.parseInt(propiedad));
					System.out.println(valor1);
					if (valor1==null || valor1.isEmpty()) {
						existe = Boolean.FALSE;
						
						System.out.println("No se tiene mas registros en MEF");
					}		
					fila = fila + 1;
					if (existe) {
						listReg.add(String.valueOf(fila));	
					}
					
				}while (existe);
				System.out.println("prueba");
				
			} catch (Exception e) {
				existe = Boolean.FALSE;
				System.out.println("Error leyendo el archivo xls: "+ e.getMessage());
			}
		} else {
			System.out.println("No se ha cargado ningun xls para leer");
			existe = Boolean.FALSE;
		}
		return listReg;
	}
    
    private static void consultaryCargarPropiedades() {
    	operacionesBD.abrirConexionBD();
		ResultSet rs = operacionesBD.ejecutarSelect(ConstantesBD.SELECT_PROPIEDADES, null);
		try {
			if(rs != null){
			while (rs.next()) {
				properties.setProperty(rs.getString(2),rs.getString(3));
			}
			}
		} catch (SQLException e) {
			System.out.println("Se presento un error al consultar las propiedades");
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		operacionesBD.cerrarStatement();
		//operacionesBD.cerrarConexion();
		
	}

}
