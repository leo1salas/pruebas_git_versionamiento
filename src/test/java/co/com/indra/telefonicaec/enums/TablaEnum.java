/**
 *
 * files
 */
package co.com.indra.telefonicaec.enums;

/**
 * Clase responsable
 *
 * @uthor INDRA <br>
 *        Harold Taborda <br>
 *        hstaborda@indracompany.com
 *
 * @date 17/01/2020
 * @version 1.0
 */
public enum TablaEnum {

    OTC_T_PARAMETRO,
    OTC_T_ALTAS_LINEAS;

    TablaEnum() {
    }


}
