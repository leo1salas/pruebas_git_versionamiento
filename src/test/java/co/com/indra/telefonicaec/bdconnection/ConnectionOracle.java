
package co.com.indra.telefonicaec.bdconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import co.com.indra.telefonicaec.utilities.IConnection;
import co.com.indra.telefonicaec.utilities.PropertiesLoader;

public class ConnectionOracle implements IConnection {

	static PropertiesLoader properties = PropertiesLoader.getInstance();
	private static Connection cnx = null;

	/**
	 * Constructor de la clase
	 */
	public ConnectionOracle() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * <b>Descripcion: </b>Obtiene la conexion a la base de datos del Back
	 * 
	 * @return Connection
	 * @throws Exception
	 */
	public Connection getConnectionBack() throws Exception {

		if (cnx == null) {
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				String hostname = properties.getProperty("hostnameBD");
				String port = properties.getProperty("portBD");
				String url = "jdbc:oracle:thin:@" + hostname + ":" + port + ":XE";
				cnx = DriverManager.getConnection(url, properties.getProperty("usernameBD"),
						properties.getProperty("passwordBD"));
				//DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
			    //conn = DriverManager.getConnection("jdbc:oracle:thin:@machine:port:SID", "user", "pass");
			} catch (SQLException ex) {
				throw new SQLException(ex);
			} catch (ClassNotFoundException ex) {
				throw new ClassCastException(ex.getMessage());
			}
		}
		return cnx;

	}

}
