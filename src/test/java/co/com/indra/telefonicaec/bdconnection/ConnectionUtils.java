/**
 * ConnectionUtils.java
 */
package co.com.indra.telefonicaec.bdconnection;

import java.sql.Connection;

import co.com.indra.telefonicaec.utilities.IConnection;
import co.com.indra.telefonicaec.utilities.PropertiesLoader;

public class ConnectionUtils {

	static PropertiesLoader properties = PropertiesLoader.getInstance();
	/**
	 * Instancia de la clase
	 */
	private static ConnectionUtils instance = null;
	private IConnection delegado;

	/**
	 * Constructor de la clase
	 */
	private ConnectionUtils() {
		String tipoBD = properties.getProperty("tipoBD");
		if (tipoBD != null && tipoBD.equals("1")) {
			delegado = new ConnectionOracle();
		} else if (tipoBD != null && tipoBD.equals("2")) {
			delegado = new ConnectionMySql();
		} else if (tipoBD != null && tipoBD.equals("3")) {
			delegado = new ConnectionSqlServer();
		}  else if (tipoBD != null && tipoBD.equals("4")) {
			delegado = new ConnectionPostgresql();
		}
	}

	/**
	 * <b>Descripcion: </b>Devuelve la instancia de la clase
	 * @return ConnectionUtils
	 */
	public static ConnectionUtils getInstance() {
		if (instance == null) {
			instance = new ConnectionUtils();
		}
		return instance;
	}
	/**
	 * <b>Descripcion: </b>Obtiene la conexion a la base de datos del Back
	 * @return Connection
	 * @throws Exception
	 */
	public Connection getConnectionBack() throws Exception {

		return delegado.getConnectionBack();
	}
}
