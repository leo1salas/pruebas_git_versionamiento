
package co.com.indra.telefonicaec.bdconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import co.com.indra.telefonicaec.utilities.IConnection;
import co.com.indra.telefonicaec.utilities.PropertiesLoader;

public class ConnectionSqlServer implements IConnection {
	static PropertiesLoader properties = PropertiesLoader.getInstance();
	private static Connection cnx = null;
   
	/**
	 * Constructor de la clase
	 */
	public ConnectionSqlServer() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * <b>Descripcion: </b>Obtiene la conexion a la base de datos del Back
	 * 
	 * @return Connection
	 * @throws Exception
	 */
	public Connection getConnectionBack() throws Exception {

		if (cnx == null) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String hostname = properties.getProperty("hostnameBD");
				String port = properties.getProperty("portBD");//1433
				//TODO autenticacion windows
				//String connectionUrl = "jdbc:sqlserver://"+hostname+";databaseName="+properties.getProperty("database")+";integratedSecurity=true;";
				//cnx = DriverManager.getConnection(connectionUrl);
				
				//TODO autenticacion usuario contrasenia
				String connectionUrl = "jdbc:sqlserver://"+hostname+":"+port+";databaseName="+properties.getProperty("database");
			    cnx = DriverManager.getConnection(connectionUrl,properties.getProperty("usernameBD"),properties.getProperty("passwordBD"));
			} catch (SQLException ex) {
				throw new SQLException(ex);
			} catch (ClassNotFoundException ex) {
				throw new ClassCastException(ex.getMessage());
			}
		}
		return cnx;
	}

}
