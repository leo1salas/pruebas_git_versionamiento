
package co.com.indra.telefonicaec.bdconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import co.com.indra.telefonicaec.utilities.IConnection;
import co.com.indra.telefonicaec.utilities.PropertiesLoader;

public class ConnectionMySql implements IConnection {
	static PropertiesLoader properties = PropertiesLoader.getInstance();
	private static Connection cnx = null;
   
	/**
	 * Constructor de la clase
	 */
	public ConnectionMySql() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * <b>Descripcion: </b>Obtiene la conexion a la base de datos del Back
	 * 
	 * @return Connection
	 * @throws Exception
	 */
	public Connection getConnectionBack() throws Exception {

		if (cnx == null) {
			try {
				Class.forName("com.mysql.jdbc.Driver");
				String database = properties.getProperty("database");
				String hostname = properties.getProperty("hostnameBD");
				String port = properties.getProperty("portBD");
				 String url = "jdbc:mysql://" + hostname + ":" + port + "/" + database+"?useUnicode=true&characterEncoding=utf8&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC";
			    cnx = DriverManager.getConnection(url, properties.getProperty("usernameBD"), properties.getProperty("passwordBD"));
			} catch (SQLException ex) {
				throw new SQLException(ex);
			} catch (ClassNotFoundException ex) {
				throw new ClassCastException(ex.getMessage());
			}
		}
		return cnx;
	}

}
