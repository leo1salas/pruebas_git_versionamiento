package co.com.indra.telefonicaec.constants;

/**
 * Clase para las constantes del proyecto
 * @author htaborda- Harold Taborda
 *
 */
public class FormatConstans {

    private FormatConstans() {
    }
    
	public static final String FORMATO_FECHA="dd/MM/yyyy";
	public static final String FORMATO_FECHA_HORA="dd/MM/yyyy HH:mm";
	public static final String FORMATO_FECHA_HORA_PEGADO="yyyyMMddHHmmss";
	
	public static final Integer CERO=0;
    public static final Integer UNO=1;
    public static final Integer DOS=2; 
    public static final Integer TRES=3;
    public static final Integer CUATRO=4;
    public static final Integer CINCO=5;
    public static final Integer SEIS=6;
    public static final Integer SIETE=7;
    public static final Integer OCHO=8;
    public static final Integer NUEVE=9;
    public static final Integer DIEZ=10;
    
    public static final String Pospago="Pospago";
    public static final String Prepago="Prepago";
    public static final String Retail="Retail";
    public static final String PlanFamilia="PlanFamilia";
    public static final String Mayorista="Mayorista";
    public static final String Financiamiento="Financiamiento";
    public static final String Empresas="Empresas";
    public static final String OnLine="OnLine";
    public static final String Altas="Altas";
    public static final String ssp="ssp";
}
