/**
 * 
 */
package co.com.indra.telefonicaec.constants;

/**
 * @author Harold
 *
 */
public final class ConsultasDinamicasConstans {
    
    private ConsultasDinamicasConstans() {
      }
    
    
   // public static final String SECUENCIA_PARAMETROS="OTC_S_PARAMETROS";
   // public static final String SECUENCIA_AUTOMATIZACIONES="OTC_S_REPORTE";
   // public static final String NEXT_PARAMETROS =  SECUENCIA_PARAMETROS+".NEXTVAL";
   // public static final String NEXT_AUTOMATIZACIONES =  SECUENCIA_AUTOMATIZACIONES+".NEXTVAL";
    public static final String ABRIR =  "(";
    public static final String CERRAR =  ")";
    public static final String SELECT_PROPIEDADES="SELECT * FROM OTC_T_PARAMETROS";
	public static final String CONSULTAR_TABLA_AUTOMATIZACIONES = "SELECT * FROM OTC_T_AUTOMATIZACIONES";
	public static final String CONSULTAR_TABLA_AUTOMATIZACIONES_ACTUAL = "SELECT * FROM OTC_T_AUTOMATIZACIONES where \"EJECUCION\"=?";
	public static final String CONSULTAR_TABLA_AUTOMATIZACIONES_CREAR_CLIENTE="select \"ID_AUTOMATIZACION\",\"ID_EJECUCION\",numero_cuenta_cliente, numero_cuenta_prepago, numero_cuenta_postpago, num_cuenta_post_advance, "
	        + "categoria_cliente, identificacion, nombre_completo, tipo_cuenta, ciclo  FROM public.otc_t_automatizaciones where \"EJECUCION\"=?";
	
	public static final String INSER_FLUJOS_AUTOMATICOS = "INSERT INTO OTC_T_REPORTE "
	        + "(id,fecha,nombrefeature,nombrescenario,tagsfeature,mensaje,fallascenario) VALUES ";
	
	/*public static final String INSER_FLUJOS_AUTOMATICOS = "INSERT INTO OTC_T_AUTOMATIZACIONES "
	        + "(\"URL_ORDEN\", \"ESTADO_ORDEN\", \"CUENTA_FACTURACION\", \"NUMERO_LINEA\", \"SIM_CARD\", \"FECHA_ORDEN\", \"CLIENTE\","
	        + "\"NOMBRE_TRANSACCION\", \"DATOS_ENTRADA\",\"USUARIO\", \"PLAN\", \"TIPO_EQUIPO\", \"NOMBRE_EQUIPO_INTERNO\", "
	        + "\"MODELO_EQUIPO_INTERNO\", \"IMEI_EQUIPO_INTERNO\", \"SLO_ACTIVADOS\", \"PLAN_ANTIGUO\", \"PLAN_NUEVO\","
	        + "\"SIMCARD_ANTIGUA\", \"SIMCARD_NUEVA\", \"NUMERO_ANTIGUO\", \"NUMERO_NUEVO\", \"NOMBRE_EQUIPO\", \"MODELO_EQUIPO\","
	        + "\"IMEI_EQUIPO\",\"FLUJO\",\"ID_EJECUCION\",\"EJECUCION\","
	        + "numero_cuenta_cliente,numero_cuenta_prepago,numero_cuenta_postpago,num_cuenta_post_advance,"
	        + "categoria_cliente,identificacion,nombre_completo,tipo_cuenta,ciclo, fecha_inicio,fecha_fin) VALUES ";*/
	
	   public static final String INSERT_PARAMETROS = "INSERT INTO otc_t_parametros"
	            + " (nombre,valor) VALUES ";
	   
	   public static final String UPDATE_FLUJOS_AUTOMATICOS ="UPDATE OTC_T_AUTOMATIZACIONES SET \"ESTADO_ORDEN\"= ?, \"FECHA_ORDEN\" = ?,"
	           + " bodega_defecto= ?, canal_distribucion= ?,fecha_envio= ?,confirmado_por= ? WHERE \"ID_EJECUCION\" = ? AND \"EJECUCION\"=?";
	   public static final String UPDATE_FLUJOS_AUTOMATICOS_CLIENTE ="UPDATE OTC_T_AUTOMATIZACIONES SET categoria_cliente=?, nombre_completo=?, tipo_cuenta=?, ciclo=? WHERE  \"ID_EJECUCION\" = ? AND \"EJECUCION\"=?";
	   public static final String UPDATE_FLUJOS_AUTOMATICOS_CLIENTE_URL_ERROR ="UPDATE OTC_T_AUTOMATIZACIONES SET url_error=? WHERE  \"ID_EJECUCION\" = ? AND \"EJECUCION\"=?";
	   public static final String UPDATE_FLUJOS_AUTOMATICOS_CLIENTE_URL_ERROR_WARNING ="UPDATE OTC_T_AUTOMATIZACIONES SET url_antes_error= ?, url_error=? WHERE  \"ID_EJECUCION\" = ? AND \"EJECUCION\"=?";
	   
	   //SELECTS TABLAS FLUJOS
	   public static final String SELECT_TRASFERENCIA_BENEFICIARIO_ID="SELECT * FROM otc_t_trasferencia_beneficiario WHERE id = ?";
	   public static final String SELECT_OTC_T_ACTIVACION_SLO_ID="SELECT * FROM OTC_T_ACTIVACION_SLO WHERE id = ?";
	   public static final String SELECT_OTC_T_ALTAS_LINEAS_ID="SELECT * FROM OTC_T_ALTAS_LINEAS WHERE id = ?";
	   public static final String SELECT_OTC_T_BAJA_ID="SELECT * FROM OTC_T_BAJA WHERE id = ?";
	   public static final String SELECT_OTC_T_CABMIO_SIM_ID="SELECT * FROM OTC_T_CAMBIO_SIM WHERE id = ?";
	   public static final String SELECT_OTC_T_CAMBIO_NUMERO_ID="SELECT * FROM OTC_T_CAMBIO_NUMERO WHERE id = ?";
	   public static final String SELECT_OTC_T_CAMBIO_PLAN_ID="SELECT * FROM OTC_T_CAMBIO_PLAN WHERE id = ?";
	   public static final String SELECT_OTC_T_CREAR_CLIENTE_ID="SELECT * FROM OTC_T_CREAR_CLIENTE WHERE id = ?";
	   public static final String SELECT_OTC_T_FAC_EQUIPO_SIN_LINEA_ID="SELECT * FROM OTC_T_FAC_EQUIPO_SIN_LINEA WHERE id = ?";
	   public static final String SELECT_OTC_T_RENOVACION_EQUIPO_ID="SELECT * FROM OTC_T_RENOVACION_EQUIPO WHERE id = ?";
	   public static final String SELECT_OTC_T_SUSPENDER_REANUDAR_ID="SELECT * FROM otc_t_suspendeer_reanudar WHERE id = ?";
	   public static final String SELECT_OTC_T_FACTURAS_MISCELANEAS_ID="SELECT * FROM OTC_T_FACTURAS_MISCELANEAS WHERE id = ?";
       public static final String SELECT_OTC_T_GENERACION_DISPUTAS_ID="SELECT * FROM OTC_T_GENERACION_DISPUTAS WHERE id = ?";

	          
	   //updates
	   public static final String UPDATE_OTC_T_SUSPENDER_REANUDAR_ID="UPDATE otc_t_suspendeer_reanudar SET procesado= ? WHERE id = ?";
	   public static final String UPDATE_OTC_T_ACTIVACION_SLO_ID="UPDATE OTC_T_ACTIVACION_SLO SET procesado= ? WHERE id = ?";
	   public static final String UPDATE_OTC_T_TRASNFERENCIA_BENEFICIARIO_ID="UPDATE otc_t_trasferencia_beneficiario SET procesado= ? WHERE id = ?";
	   public static final String UPDATE_OTC_T_ALTAS_LINEAS_ID="UPDATE OTC_T_ALTAS_LINEAS SET procesado= ? WHERE id = ?";
	   public static final String UPDATE_OTC_T_BAJA="UPDATE OTC_T_BAJA SET procesado= ? WHERE id = ?";
	   public static final String UPDATE_OTC_T_CABMIO_SIM="UPDATE OTC_T_CAMBIO_SIM SET procesado= ? WHERE id = ?";
	   public static final String UPDATE_OTC_T_CAMBIO_NUMERO_ID="UPDATE OTC_T_CAMBIO_NUMERO SET procesado= ? WHERE id = ?";
	   public static final String UPDATE_OTC_T_CAMBIO_PLAN_ID="UPDATE OTC_T_CAMBIO_PLAN SET procesado= ? WHERE id = ?";
	   public static final String UPDATE_OTC_T_CREAR_CLIENTE_ID="UPDATE OTC_T_CREAR_CLIENTE SET procesado= ? WHERE id = ?";
	   public static final String UPDATE_OTC_T_FAC_EQUIPO_SIN_LINEA_ID="UPDATE OTC_T_FAC_EQUIPO_SIN_LINEA SET procesado= ? WHERE id = ?";
	   public static final String UPDATE_OTC_T_RENOVACION_EQUIPO_ID="UPDATE OTC_T_RENOVACION_EQUIPO SET procesado= ? WHERE id = ?";
  	   public static final String UPDATE_OTC_T_FACTURAS_MISCELANEAS_ID="UPDATE OTC_T_FACTURAS_MISCELANEAS SET procesado= ? WHERE id = ?";
       public static final String UPDATE_OTC_T_GENERACION_DISPUTAS_ID="UPDATE OTC_T_GENERACION_DISPUTAS SET procesado= ? WHERE id = ?";

	   
       public static final String UPDATE_FLUJOS_AUTOMATICOS_ALL = "UPDATE  OTC_T_AUTOMATIZACIONES SET "
	            + "URL_ORDEN= ?, ESTADO_ORDEN= ?, CUENTA_FACTURACION= ?, NUMERO_LINEA= ?, SIM_CARD= ?, FECHA_ORDEN= ?, CLIENTE= ?,"
	            + "NOMBRE_TRANSACCION= ?, DATOS_ENTRADA= ?,USUARIO= ?, PLAN= ?, TIPO_EQUIPO= ?, NOMBRE_EQUIPO_INTERNO= ?, "
	            + "MODELO_EQUIPO_INTERNO= ?, IMEI_EQUIPO_INTERNO= ?, SLO_ACTIVADOS= ?, SLO_DESACTIVADOS= ?, PLAN_ANTIGUO= ?, PLAN_NUEVO= ?,"
	            + "SIMCARD_ANTIGUA= ?, SIMCARD_NUEVA= ?, NUMERO_ANTIGUO= ?, NUMERO_NUEVO= ?, NOMBRE_EQUIPO= ?, MODELO_EQUIPO= ?,"
	            + "IMEI_EQUIPO= ?,FLUJO= ?,ID_EJECUCION= ?,EJECUCION= ?,"
	            + "NUMERO_CUENTA_CLIENTE= ?,NUMERO_CUENTA_PREPAGO= ?,NUMERO_CUENTA_POSTPAGO= ?,NUM_CUENTA_POST_ADVANCE= ?,"
	            + "CATEGORIA_CLIENTE= ?,IDENTIFICACION= ?,NOMBRE_COMPLETO= ?,TIPO_CUENTA= ?,CICLO= ?, FECHA_INICIO= ?,FECHA_FIN= ?,"
	            + "NUMERO_DISPUTA=?, FACTURA_DISPUTA=?, NUMERO_NOTA_CREDITO=?, URL_AJUSTE_DISPUTA=?, URL_DISPUTA=?, URL_DOCUMENTO_NOTA_CREDITO=? WHERE ID_EJECUCION = ? AND EJECUCION=? ";
	   
	   
       public static final String SELECT_FINAL_URL_ID = "SELECT \"URL_ORDEN\",\"ID_EJECUCION\",\"EJECUCION\",\"FLUJO\",identificacion,numero_cuenta_prepago,numero_cuenta_postpago,num_cuenta_post_advance FROM OTC_T_AUTOMATIZACIONES WHERE \"ID_EJECUCION\" IN (";
       public static final String SELECT_FINAL_URL_ID_FIN =" AND \"EJECUCION\"=?";

	
}


