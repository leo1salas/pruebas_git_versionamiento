package co.com.indra.telefonicaec.constants;

public class ConstantesGlobales {

	public static final String OBTENIDO = "obtenido";
	public static final String ESPERADO = "esperado";
	public static final String VALOR_MODELO = "valor_modelo";
	public static final String MONTO_CUOTA_MODELO = "monto_cuota_modelo";
	public static final String MONTO_CUOTA_MODELO_CALCULADO =  "monto_cuota_modelo_calculado";
	public static final String MONTO_TOTAL_NO_FACTURADO = "monto_total_no_facturado";
	public static final String CABECERA_MEF_GRUPO = "6";
	public static final String NUM_CUOTA = "Cuota";
	
	
	//Constantes para las propiedades en base de datos
	public static final String NC_CORREO_ORIGEN = "nc.correo.origen";
	public static final String NC_CORREO_DESTINO = "nc.correo.destino";
	public static final String NC_CORREO_ENCABEZADO = "nc.correo.encabezado";
	public static final String NC_CORREO_SMTP_PUERTO = "nc.correo.smtp.puerto";
	public static final String NC_CORREO_ORIGEN_PASS = "nc.correo.origen.pass";
	public static final String NC_CORREO_MENSAJE = "nc.correo.mensaje";
	public static final String NC_CORREO_ESGMAIL = "nc.correo.esGmail";
	public static final String NC_CORREO_HOST = "nc.correo.smtp";	
	public static final String NC_CORREO_SMTP = "nc.correo.smtp";
	public static final String MAIL_SMTP_HOST = "mail.smtp.host";	
	public static final String MAIL_SMTP_PORT = "mail.smtp.port";
	public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
	public static final String MAIL_SMTP_SSL_ENABLE = "mail.smtp.ssl.enable";
	public static final String MAIL_SMTP_STARTTTLS_ENABLE = "mail.smtp.starttls.enable";
	
	
	//Literales clase EamilUtil
	public static final String UTF_8 = "UTF-8";
}
