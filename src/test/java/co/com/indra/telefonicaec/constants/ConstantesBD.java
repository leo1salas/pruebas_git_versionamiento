package co.com.indra.telefonicaec.constants;

public class ConstantesBD {

	public static final int TIPO_SELECT = 0;
	public static final int TIPO_INSERT = 1;
	
	//TABLAS
	public static final String TABLA_PROPIEDADES="otc_t_parametro";
	public static final String TABLA_REPORTE="otc_t_reporte";
	
	
	//QUERYS otc_t_parametro
	public static final String SELECT_PROPIEDAD_POR_NOMBRE="SELECT valor FROM otc_t_parametro WHERE nombre = ?";	
	public static final String SELECT_PROPIEDADES="SELECT * FROM otc_t_parametro ";
	
	
	//Queries otc_t_reporte
	public static final String SELECT_REPORTE_TODO = "SELECT * FROM otc_t_reporte";
	public static final String SELECT_REPORTE_POR_CAMPO = "SELECT * FROM otc_t_reporte WHERE campo = ?";
	public static final String INSERT_REPORTE = "INSERT INTO otc_t_reporte(fecha,canal, nombreFeature, NombreScenario, tagsFeature, mensaje,fallascenario) VALUES(?,?,?,?,?,?,?)";
	public static final String CONSULTAR_TABLA_REPORTE_ACTUAL = "SELECT ID,FECHA,CANAL,NOMBREFEATURE,NOMBRESCENARIO,TAGSFEATURE,MENSAJE,FALLASCENARIO FROM OTC_T_REPORTE WHERE FECHA=?";
	
}