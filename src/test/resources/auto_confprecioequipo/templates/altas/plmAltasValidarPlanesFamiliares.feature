#Author: danielagarcia1309@gmail.com
@plm @altasplanesfamiliares
Feature: Altas Planes Familiares
  COMO Un empleado de Telefonica
  QUIERO realizar el alta validando el precio de un equipo seleccionado
  PARA Tener el proceso automatizado y no hacerlo manual
  
 Background: 
    Given Login "nc.url" con "nc.port" user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Iniciar Sesión"
    And Init "Altas Planes Familiares" y canal "Altas"
  
Scenario Outline: Altas Planes Familiares 
    Given Consulto la tabla "OTC_T_ALTAS_LINEAS" con el id "<ID>" 
    And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"
    And esperar carga pantalla	
    And Validar cargando    
	  And Ingreso "nc.otecel1" en el campo "campocliente"
	  And Doy clic en "boton.busqueda"  
   	And Doy clic en "Otecel 1"
   	And esperar carga pantalla 
   	And Validar cargando
   	And Clic en boton por texto "boton.productos"  
    And ingreso Canal de distribucion "canal_distribucion" 
    And seleciono combo orden venta "combo.orden.venta.vacia" para "n.a" 
    And Clic en boton por texto "boton.crear"
    And esperar carga pantalla    	
	  And cambiar frame interno
	  And espero habilitar servicios y caracteristicas
    And Ingreso "nc.busqueda.planFamiliares" en el campo servicio de busqueda
    And Selecciono el plan anadido 
	  And Validar tipo plan "plan" 
	  And Validar cargando
	  And selecionar contrato "contrato" ICCD "sim" numero telefono "numero_telefono" 
	  And Validar cargando
	  And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" 
	  And Validar cargando
	  And Doy clic en "Agregar"
	  And Validar cargando
	  And ingresar imei "imei" 
	  And Validar cargando
	  #And Esperar a que el campo "precioempresas" esté visible	
	  And validar crear cliente "cliente" 
    And cambio a miga de pan validar
    And cambiar limite credito "20000"
    And clic boton final reservar activo
    And esperar carga pantalla  
    And Validar cargando
    And clic boton final configurar contrato
   	And generar documento
    And clic boton final generar factura 
    And esperar carga pantalla  
    And Validar cargando
    And clic boton final enviar
    And extraer Numero orden de venta
    And Volver a la Orden
    And Entrada de Orden
    And Seleccionar la orden anterior   
    And Verificar Numero de Cuenta y Guardar URL
    #Then Guardar resultado en BD flujo "ALTAS_LINEAS" 
   	 
      Examples: 
      |ID|row_index|