#Author: danielagarcia1309@gmail.com
@plm @empresasfinanciado
Feature: Valida Equipo para el Modulo Empresas
  COMO un empleado de Telefonica
  QUIERO validar el precio del equipo
  PARA hacerlo Automatico y no manual

Background:
 Given Login "nc.url" con "nc.port" user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Iniciar Sesión"
  And Init "Valida Equipo para el Modulo Empresas Financiado" y canal "Empresas"
    
    
Scenario Outline: Validar precio contado empresas grupo dos
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda" 
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clienteEmpresarial" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo2E.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido 
		And Validar cargando 
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Validar cargando 
		And Doy clic en "Agregar" 
		And Validar cargando  
    And Doy clic en "Pago en cuotas" 
    And Ingreso "12" en el combo "Combo cuotas"
   	And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.financiado.grupo2e" NA "1" con "12"
   	And Almaceno el campo "precioempresas" con numero de cuotas "12" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "18" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.financiado.grupo2e" NA "1" con "18"
    And Almaceno el campo "precioempresas" con numero de cuotas "18" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "24" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.financiado.grupo2e" NA "1" con "24"
    And Almaceno el campo "precioempresas" con numero de cuotas "24" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Valido que los campos tengan los valores esperados
 
 Examples:
    |row_index|
 
 
 Scenario Outline: Validar precio contado empresas grupo tres
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda"  
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clienteEmpresarial" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo3E.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando 
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Validar cargando 
		And Doy clic en "Agregar"  
		And Validar cargando 
    And Doy clic en "Pago en cuotas" 
    And Ingreso "12" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.financiado.grupo3e" NA "1" con "12"
    And Almaceno el campo "precioempresas" con numero de cuotas "12" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "18" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.financiado.grupo3e" NA "1" con "18"
    And Almaceno el campo "precioempresas" con numero de cuotas "18" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "24" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.financiado.grupo3e" NA "1" con "24"
    And Almaceno el campo "precioempresas" con numero de cuotas "24" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
    And Valido que los campos tengan los valores esperados
    
 Examples:
    |row_index|
    
    
Scenario Outline: Validar precio contado empresas grupo cuatro
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda"
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clienteEmpresarial" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo4E.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando 
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Validar cargando 
		And Doy clic en "Agregar"
		And Validar cargando   
    And Doy clic en "Pago en cuotas" 
    And Ingreso "12" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.financiado.grupo4e" NA "1" con "12"
    And Almaceno el campo "precioempresas" con numero de cuotas "12" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
    And Ingreso "18" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.financiado.grupo4e" NA "1" con "18"
    And Almaceno el campo "precioempresas" con numero de cuotas "18" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
    And Ingreso "24" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.financiado.grupo4e" NA "1" con "24"
    And Almaceno el campo "precioempresas" con numero de cuotas "24" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
    And Valido que los campos tengan los valores esperados
    
 Examples:
    |row_index|
    
    
 Scenario Outline: Validar precio contado empresas grupo cinco
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda"
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clienteEmpresarial" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo5E.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido 
		And Validar cargando 
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Validar cargando 
		And Doy clic en "Agregar"
		And Validar cargando   
    And Doy clic en "Pago en cuotas" 
    And Ingreso "12" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.financiado.grupo5e" NA "1" con "12"
    And Almaceno el campo "precioempresas" con numero de cuotas "12" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
    And Ingreso "18" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.financiado.grupo5e" NA "1" con "18"
    And Almaceno el campo "precioempresas" con numero de cuotas "18" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "24" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.financiado.grupo5e" NA "1" con "24"
    And Almaceno el campo "precioempresas" con numero de cuotas "24" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
    And Valido que los campos tengan los valores esperados
    
 Examples:
    |row_index|