#Author: danielagarcia1309@gmail.com
@plm @empresascontado
Feature: Valida Equipo para el Modulo Empresas
  COMO un empleado de Telefonica
  QUIERO validar el precio del equipo
  PARA hacerlo Automatico y no manual

 Background: 
    Given Login en "nc.url" "nc.port" "paht.cliente.residencial" con user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Login"
     And Init "Valida Equipo para el Modulo Empresas Contado" y canal "Empresas"

 Scenario Outline: Validar precio contado empresas grupo uno
 		When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda" 
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clienteEmpresarial" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo1E.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando 
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Doy clic en "Agregar"  
		And Validar cargando
    And Esperar a que el campo "precioempresas" esté visible
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo1e" NA "1" con "0"
    And Doy clic en "Pago en cuotas" 
    And Ingreso "1" en el combo "Combo cuotas" 
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo1e" NA "1" con "1"
    And Almaceno el campo "precioempresas" con numero de cuotas "1" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "3" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo1e" NA "1" con "3"
    And Almaceno el campo "precioempresas" con numero de cuotas "3" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "6" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo1e" NA "1" con "6"
    And Almaceno el campo "precioempresas" con numero de cuotas "6" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Valido que los campos tengan los valores esperados
    
 Examples:
    |row_index|
 
 Scenario Outline: Validar precio contado empresas grupo dos
		When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda" 
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clienteEmpresarial" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo2E.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando 
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Doy clic en "Agregar"
		And Validar cargando  
    And Esperar a que el campo "precioempresas" esté visible
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo2e" NA "1" con "0"
    And Doy clic en "Pago en cuotas" 
    And Ingreso "1" en el combo "Combo cuotas" 
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo2e" NA "1" con "1"
    And Almaceno el campo "precioempresas" con numero de cuotas "1" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "3" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo2e" NA "1" con "3"
    And Almaceno el campo "precioempresas" con numero de cuotas "3" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "6" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo2e" NA "1" con "6"
    And Almaceno el campo "precioempresas" con numero de cuotas "6" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Valido que los campos tengan los valores esperados
    
 Examples:
    |row_index|
    
    Scenario Outline: Validar precio contado empresas grupo tres
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda" 
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clienteEmpresarial" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo3E.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Doy clic en "Agregar"
		And Validar cargando   
    And Esperar a que el campo "precioempresas" esté visible
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo3e" NA "1" con "0"
    And Doy clic en "Pago en cuotas" 
    And Ingreso "1" en el combo "Combo cuotas" 
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo3e" NA "1" con "1"
    And Almaceno el campo "precioempresas" con numero de cuotas "1" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "3" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo3e" NA "1" con "3"
    And Almaceno el campo "precioempresas" con numero de cuotas "3" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "6" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo3e" NA "1" con "6"
    And Almaceno el campo "precioempresas" con numero de cuotas "6" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Valido que los campos tengan los valores esperados
    
 Examples:
    |row_index|
    
    
    Scenario Outline: Validar precio contado empresas grupo cuatro
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda" 
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clienteEmpresarial" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo4E.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Doy clic en "Agregar"
		And Validar cargando   
    And Esperar a que el campo "precioempresas" esté visible
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo4e" NA "1" con "0"
    And Doy clic en "Pago en cuotas" 
    And Ingreso "1" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo4e" NA "1" con "1"
    And Almaceno el campo "precioempresas" con numero de cuotas "1" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "3" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo4e" NA "1" con "3"
    And Almaceno el campo "precioempresas" con numero de cuotas "3" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "6" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo4e" NA "1" con "6"
    And Almaceno el campo "precioempresas" con numero de cuotas "6" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Valido que los campos tengan los valores esperados
    
 Examples:
    |row_index|
    
    
    Scenario Outline: Validar precio contado empresas grupo cinco
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda"  
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clienteEmpresarial" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo5E.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido 
		And Validar cargando
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Doy clic en "Agregar"
		And Validar cargando   
    And Esperar a que el campo "precioempresas" esté visible
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo5e" NA "1" con "0"
    And Doy clic en "Pago en cuotas" 
    And Ingreso "1" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo5e" NA "1" con "1"
    And Almaceno el campo "precioempresas" con numero de cuotas "1" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "3" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo5e" NA "1" con "3"
    And Almaceno el campo "precioempresas" con numero de cuotas "3" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "6" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.empresas.contado.grupo5e" NA "1" con "6"
    And Almaceno el campo "precioempresas" con numero de cuotas "6" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Valido que los campos tengan los valores esperados
    
 Examples:
    |row_index|
