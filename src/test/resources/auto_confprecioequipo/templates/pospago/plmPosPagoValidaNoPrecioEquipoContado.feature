#Author: cfdavila.indracompany.com
@plm @napospagocontado
Feature: Validar no precio equipo Contado Pospago
  COMO usuario de pruebas
  QUIERO Validar el precio de contado, 1 y 3 Meses de los equipos del Bloque POSPAGO / PREVIOPAGO para el Canal Interno 
  PARA Tener el proceso automatizado y no hacerlo manual


  Background: 
    Given Login "nc.url" con "nc.port" user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Iniciar Sesión"
    And Init "Validar no precio equipo Contado Pospago" y canal "Pospago"
      
  Scenario Outline: Validar No precio equipo contado Pospago grupo2 
    	When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"
	    And esperar carga pantalla	    
	    And Ingreso "nc.otecel1" en el campo "campocliente" 
    	And Doy clic en "boton.busqueda" 
    	And Doy clic en "Otecel 1"
    	And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    	And esperar carga pantalla    	
	    And cambiar frame interno
	    And cargar servicios y caracteristicas
	    And Ingreso "nc.grupo2.plan" en el campo servicio de busqueda
	    And Selecciono el plan anadido
	    And Validar cargando
	    And Doy clic en "planempresas" 
	    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.pospago.contado.grupo2" 
    	And Valido estilo "nc.estilo.none" campo "Error" 

Examples:
    |row_index|
	    
 Scenario Outline: Validar No precio equipo contado Pospago grupo3
    	When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"
	    And esperar carga pantalla	    
	    And Ingreso "nc.otecel1" en el campo "campocliente" 
    	And Doy clic en "boton.busqueda" 
    	And Doy clic en "Otecel 1"
    	And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    	And esperar carga pantalla    	
	    And cambiar frame interno
	    And cargar servicios y caracteristicas
	    And Ingreso "nc.grupo3.plan" en el campo servicio de busqueda
	    And Selecciono el plan anadido
	    And Validar cargando
	    And Doy clic en "planempresas" 
	    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.pospago.contado.grupo3" 
    	And Valido estilo "nc.estilo.none" campo "Error" 

Examples:
    |row_index|
    
Scenario Outline: Validar No precio equipo contado Pospago grupo4
    	When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"
	    And esperar carga pantalla	    
	    And Ingreso "nc.otecel1" en el campo "campocliente" 
    	And Doy clic en "boton.busqueda" 
    	And Doy clic en "Otecel 1"
    	And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    	And esperar carga pantalla    	
	    And cambiar frame interno
	    And cargar servicios y caracteristicas
	    And Ingreso "nc.grupo4.plan" en el campo servicio de busqueda
	    And Selecciono el plan anadido
	    And Validar cargando
	    And Doy clic en "planempresas" 
	    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.pospago.contado.grupo4" 
    	And Valido estilo "nc.estilo.none" campo "Error" 

Examples:
    |row_index|
    
 Scenario Outline: Validar No precio equipo contado Pospago grupo4
    	When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"
	    And esperar carga pantalla	    
	    And Ingreso "nc.otecel1" en el campo "campocliente" 
    	And Doy clic en "boton.busqueda" 
    	And Doy clic en "Otecel 1"
    	And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    	And esperar carga pantalla    	
	    And cambiar frame interno
	    And cargar servicios y caracteristicas
	    And Ingreso "nc.grupo5.plan" en el campo servicio de busqueda
	    And Selecciono el plan anadido
	    And Validar cargando
	    And Doy clic en "planempresas" 
	    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.pospago.contado.grupo5" 
    	And Valido estilo "nc.estilo.none" campo "Error" 

Examples:
    |row_index|