#Author: cfdavila.indracompany.com
@plm @pospagocontado
Feature: Validar precio con RENOVACIÓN Bloque POSPAGO / PREVIOPAGO
  COMO usuario de pruebas
  QUIERO Validar el precio de contado, 1 y 3 Meses de los equipos del Bloque POSPAGO / PREVIOPAGO para el Canal Interno
  PARA Tener el proceso automatizado y no hacerlo manual

  Background: 
    Given Login "nc.url" con "nc.port" user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Iniciar Sesión"
    And Init "Validar precio con RENOVACIÓN Bloque POSPAGO / PREVIOPAGO" y canal "Pospago"
      
  Scenario Outline: Validar precio equipo contado Pospago grupo2
    	When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente" 
	    And esperar carga pantalla	    
	    And Ingreso "nc.otecel1" en el campo "campocliente"
    	And Doy clic en "boton.busqueda" 
    	And Validar cargando
    	And Doy clic en "Otecel 1"
    	And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    	And esperar carga pantalla    	
	    And cambiar frame interno
	    And espero habilitar servicios y caracteristicas
    	And Ingreso "nc.grupo2.plan" en el campo servicio de busqueda
    	And Selecciono el plan anadido
	    And Validar cargando
	    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
	    And Validar cargando
	    And Doy clic en "Agregar"
	    And Validar cargando	    
	    And Esperar a que el campo "precioempresas" esté visible	 
	    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.contado.grupo2" NA "1" con "0"
	    And Doy clic en "Pago en cuotas"
	    And Validar cargando
	    And Ingreso "1" en el combo "Combo cuotas"
	    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.contado.grupo2" NA "1" con "1"
	    And Almaceno el campo "precioempresas" con numero de cuotas "1" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
	    And Ingreso "3" en el combo "combocuotasselected"
	    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.contado.grupo2" NA "1" con "3"
	    And Almaceno el campo "precioempresas" con numero de cuotas "3" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
		  And Valido que los campos tengan los valores esperados


    Examples: 
      |row_index|
      
      
 Scenario Outline: Validar precio equipo contado Pospago grupo3
    	When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente" 
	    And esperar carga pantalla	    
	    And Ingreso "nc.otecel1" en el campo "campocliente"
    	And Doy clic en "boton.busqueda" 
    	And Validar cargando
    	And Doy clic en "Otecel 1"
    	And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    	And esperar carga pantalla    	
	    And cambiar frame interno
	    And espero habilitar servicios y caracteristicas
    	And Ingreso "nc.grupo3.plan" en el campo servicio de busqueda
    	And Selecciono el plan anadido
	    And Validar cargando
	    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
	    And Validar cargando
	    And Doy clic en "Agregar"
	    And Validar cargando	    
	    And Esperar a que el campo "precioempresas" esté visible	 
	    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.contado.grupo3" NA "1" con "0"
	    And Doy clic en "Pago en cuotas"
	    And Validar cargando
	    And Ingreso "1" en el combo "Combo cuotas"
	    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.contado.grupo3" NA "1" con "1"
	    And Almaceno el campo "precioempresas" con numero de cuotas "1" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
	    And Ingreso "3" en el combo "combocuotasselected"
	    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.contado.grupo3" NA "1" con "3"
	    And Almaceno el campo "precioempresas" con numero de cuotas "3" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
		  And Valido que los campos tengan los valores esperados


    Examples: 
      |row_index|
      
 Scenario Outline: Validar precio equipo contado Pospago grupo4
    	When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente" 
	    And esperar carga pantalla	    
	    And Ingreso "nc.otecel1" en el campo "campocliente"
    	And Doy clic en "boton.busqueda" 
    	And Validar cargando
    	And Doy clic en "Otecel 1"
    	And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    	And esperar carga pantalla    	
	    And cambiar frame interno
	    And espero habilitar servicios y caracteristicas
    	And Ingreso "nc.grupo4.plan" en el campo servicio de busqueda
    	And Selecciono el plan anadido
	    And Validar cargando
	    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
	    And Validar cargando
	    And Doy clic en "Agregar"
	    And Validar cargando	    
	    And Esperar a que el campo "precioempresas" esté visible	 
	    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.contado.grupo4" NA "1" con "0"
	    And Doy clic en "Pago en cuotas"
	    And Validar cargando
	    And Ingreso "1" en el combo "Combo cuotas"
	    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.contado.grupo4" NA "1" con "1"
	    And Almaceno el campo "precioempresas" con numero de cuotas "1" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
	    And Ingreso "3" en el combo "combocuotasselected"
	    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.contado.grupo4" NA "1" con "3"
	    And Almaceno el campo "precioempresas" con numero de cuotas "3" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
		  And Valido que los campos tengan los valores esperados


    Examples: 
      |row_index|
      
  Scenario Outline: Validar precio equipo contado Pospago grupo5
    	When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente" 
	    And esperar carga pantalla	    
	    And Ingreso "nc.otecel1" en el campo "campocliente"
    	And Doy clic en "boton.busqueda" 
    	And Validar cargando
    	And Doy clic en "Otecel 1"
    	And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    	And esperar carga pantalla    	
	    And cambiar frame interno
	    And espero habilitar servicios y caracteristicas
    	And Ingreso "nc.grupo5.plan" en el campo servicio de busqueda
    	And Selecciono el plan anadido
	    And Validar cargando
	    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
	    And Validar cargando
	    And Doy clic en "Agregar"
	    And Validar cargando	    
	    And Esperar a que el campo "precioempresas" esté visible	 
	    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.contado.grupo5" NA "1" con "0"
	    And Doy clic en "Pago en cuotas"
	    And Validar cargando
	    And Ingreso "1" en el combo "Combo cuotas"
	    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.contado.grupo5" NA "1" con "1"
	    And Almaceno el campo "precioempresas" con numero de cuotas "1" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
	    And Ingreso "3" en el combo "combocuotasselected"
	    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.contado.grupo5" NA "1" con "3"
	    And Almaceno el campo "precioempresas" con numero de cuotas "3" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
		  And Valido que los campos tengan los valores esperados


    Examples: 
      |row_index|
 	 	