#danielagarcia1309@gmail.com
@plm @planesfamiliarescontado
Feature: Valida precio Equipo para el Modulo Planes Familiares
  COMO un empleado de Telefonica
  QUIERO validar el precio del equipo
  PARA hacerlo Automatico y no manual

Background:
 Given Login "nc.url" con "nc.port" user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Iniciar Sesión"
 And Init "Valida precio Equipo para el Modulo Planes Familiares" y canal "PlanFamilia"
 
Scenario Outline: Validar precio contado Planes Familiares 
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda" 
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.canal.PlanesFamiliares" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.busqueda.planFamiliares" en el campo servicio de busqueda
    And Selecciono el plan anadido 
		And Validar cargando
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Validar cargando
		And Doy clic en "Agregar"  
		And Validar cargando
    And Esperar a que el campo "Precio" esté visible
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.familiares.contado" NA "1" con "0"
    And Doy clic en "Pago en cuotas" 
    And Ingreso "1" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.familiares.contado" NA "1" con "1"
    And Almaceno el campo "precioempresas" con numero de cuotas "1" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "3" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.familiares.contado" NA "1" con "3"
    And Almaceno el campo "precioempresas" con numero de cuotas "3" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Valido que los campos tengan los valores esperados
    
 Examples:
    |row_index|