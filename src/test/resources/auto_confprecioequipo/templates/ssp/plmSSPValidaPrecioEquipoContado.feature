#Author: danielagarcia1309@gmail.com
@plm 
Feature: Validar precio de equipo SSP
  COMO Un empleado de Telefonica
  QUIERO Validar el precio de un equipo seleccionado
  PARA Tener el proceso automatizado y no hacerlo manual

Background:
 	Given Ingreso a "ssp.url" con "ssp.port" clic en "Mi Movistar" 
 	And Initssp "Validar precio de equipo SSP Contado" y canal "ssp" 
 	#nombre de Feature y canal el nombre de carpeta

Scenario Outline: Validar SSP precio equipo contado grupo2
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And esperar carga pantalla 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.plan" 
	 And esperar carga pantalla 
	 And Ingreso "ssp.grupo2.plan" en el campo "busqueda plan"
	 And esperar carga pantalla  
 	 And Doy clic en "Enter" 
 	 And esperar carga pantalla 
 	 And Doy clic en "LO QUIERO"
 	 And esperar carga pantalla 
 	 And Doy clic en "comprarplanconcelular"
 	 And esperar carga pantalla  
 	 And Ingreso valor "<row_index>" en el campo "buscarmodelo" y "nc.xls.constante.inicio.equipo.nombressp"  
 	 And Doy clic en "Enter" 
 	 And esperar carga pantalla 
 	 And Doy clic en "LO QUIERO MODELO"
 	 And esperar carga pantalla 
 	 And Doy clic en "Comprar"
 	 And esperar carga pantalla 
 	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
 	 And Doy clic en las cuotas ssp sincuota "combocuotasspsincuotas" 
 	 And esperar carga pantalla 	  
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo2" con numero de cuota "1" y la propidad "preciocontadocuotassp" 
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
	 And Doy clic en las cuotas ssp una "combocuotassp1cuotas" 
	 And esperar carga pantalla 
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo2" con numero de cuota "1" y la propidad "preciocontadocuotassp" 
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
	 And Doy clic en las cuotas ssp tres "combocuotassp3cuotas" 
	 And esperar carga pantalla 
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo2" con numero de cuota "1" y la propidad "preciocontadocuotassp" 
	 And Doy clic en "eliminarorden"
 	 And esperar carga pantalla
 	 And Valido que los campos tengan los valores esperados
 	 
 	 Examples:
    |row_index|
    |46| 
 	 
  
  Scenario Outline: Validar SSP precio equipo contado grupo3
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And esperar carga pantalla 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.plan" 
	 And esperar carga pantalla 
	 And Ingreso "ssp.grupo3.plan" en el campo "busqueda plan"
	 And esperar carga pantalla  
 	 And Doy clic en "Enter" 
 	 And esperar carga pantalla 
 	 And Doy clic en "LO QUIERO"
 	 And esperar carga pantalla 
 	 And Doy clic en "comprarplanconcelular"
 	 And esperar carga pantalla  
 	 And Ingreso valor "<row_index>" en el campo "buscarmodelo" y "nc.xls.constante.inicio.equipo.nombressp"  
 	 And Doy clic en "Enter" 
 	 And esperar carga pantalla 
 	 And Doy clic en "LO QUIERO MODELO"
 	 And esperar carga pantalla 
 	 And Doy clic en "Comprar"
 	 And esperar carga pantalla 
 	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
 	 And Doy clic en las cuotas ssp sincuota "combocuotasspsincuotas" 
 	 And esperar carga pantalla 	  
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo3" con numero de cuota "1" y la propidad "preciocontadocuotassp" 
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
	 And Doy clic en las cuotas ssp una "combocuotassp1cuotas" 
	 And esperar carga pantalla 
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo3" con numero de cuota "1" y la propidad "preciocontadocuotassp" 
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
	 And Doy clic en las cuotas ssp tres "combocuotassp3cuotas" 
	 And esperar carga pantalla 
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo3" con numero de cuota "1" y la propidad "preciocontadocuotassp" 
	 And Doy clic en "eliminarorden"
 	 And esperar carga pantalla
 	 And Valido que los campos tengan los valores esperados
 	 
 	 Examples:
    |row_index|
    |46| 
 	  
 	  
  Scenario Outline: Validar SSP precio equipo contado grupo4
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And esperar carga pantalla 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.plan" 
	 And esperar carga pantalla 
	 And Ingreso "ssp.grupo4.plan" en el campo "busqueda plan"
	 And esperar carga pantalla  
 	 And Doy clic en "Enter" 
 	 And esperar carga pantalla 
 	 And Doy clic en "LO QUIERO"
 	 And esperar carga pantalla 
 	 And Doy clic en "comprarplanconcelular"
 	 And esperar carga pantalla  
 	 And Ingreso valor "<row_index>" en el campo "buscarmodelo" y "nc.xls.constante.inicio.equipo.nombressp"  
 	 And Doy clic en "Enter" 
 	 And esperar carga pantalla 
 	 And Doy clic en "LO QUIERO MODELO"
 	 And esperar carga pantalla 
 	 And Doy clic en "Comprar"
 	 And esperar carga pantalla 
 	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
 	 And Doy clic en las cuotas ssp sincuota "combocuotasspsincuotas" 
 	 And esperar carga pantalla 	  
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo4" con numero de cuota "1" y la propidad "preciocontadocuotassp" 
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
	 And Doy clic en las cuotas ssp una "combocuotassp1cuotas" 
	 And esperar carga pantalla 
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo4" con numero de cuota "1" y la propidad "preciocontadocuotassp" 
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
	 And Doy clic en las cuotas ssp tres "combocuotassp3cuotas" 
	 And esperar carga pantalla 
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo4" con numero de cuota "1" y la propidad "preciocontadocuotassp" 
	 And Doy clic en "eliminarorden"
 	 And esperar carga pantalla
 	 And Valido que los campos tengan los valores esperados
 	 
 	 Examples:
    |row_index|
    |46| 
    
  Scenario Outline: Validar SSP precio equipo contado grupo5
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And esperar carga pantalla 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.plan" 
	 And esperar carga pantalla 
	 And Ingreso "ssp.grupo5.plan" en el campo "busqueda plan"
	 And esperar carga pantalla  
 	 And Doy clic en "Enter" 
 	 And esperar carga pantalla 
 	 And Doy clic en "LO QUIERO"
 	 And esperar carga pantalla 
 	 And Doy clic en "comprarplanconcelular"
 	 And esperar carga pantalla  
 	 And Ingreso valor "<row_index>" en el campo "buscarmodelo" y "nc.xls.constante.inicio.equipo.nombressp"  
 	 And Doy clic en "Enter" 
 	 And esperar carga pantalla 
 	 And Doy clic en "LO QUIERO MODELO"
 	 And esperar carga pantalla 
 	 And Doy clic en "Comprar"
 	 And esperar carga pantalla 
 	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
 	 And Doy clic en las cuotas ssp sincuota "combocuotasspsincuotas" 
 	 And esperar carga pantalla 	  
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo5" con numero de cuota "1" y la propidad "preciocontadocuotassp" 
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
	 And Doy clic en las cuotas ssp una "combocuotassp1cuotas" 
	 And esperar carga pantalla 
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo5" con numero de cuota "1" y la propidad "preciocontadocuotassp" 
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
	 And Doy clic en las cuotas ssp tres "combocuotassp3cuotas" 
	 And esperar carga pantalla 
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo5" con numero de cuota "1" y la propidad "preciocontadocuotassp" 
	 And Doy clic en "eliminarorden"
 	 And esperar carga pantalla
 	 And Valido que los campos tengan los valores esperados
 	 
 	 Examples:
    |row_index|
    |46|   
 	  	  
