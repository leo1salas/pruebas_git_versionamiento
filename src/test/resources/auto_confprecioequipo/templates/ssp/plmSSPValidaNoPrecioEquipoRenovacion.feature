#Author: danielagarcia1309@gmail.com
@plm 
Feature: Validar No precio de equipo SSP Renovacion
  COMO Un empleado de Telefonica
  QUIERO Validar el precio de un equipo seleccionado
  PARA Tener el proceso automatizado y no hacerlo manual

Background:
 	Given Ingreso a "ssp.url" con "ssp.port" clic en "Mi Movistar" 
 	And Initssp "Validar No precio de equipo SSP Renovacion" y canal "ssp" 
 	#nombre de Feature y canal el nombre de carpeta
 	
 	Scenario Outline: Validar SSP No precio equipo renovacion grupo2
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And esperar carga pantalla 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.renovacion.plan" 
	 And Ingreso valor "<row_index>" en el campo "Buscar Modelo" y "nc.xls.constante.inicio.equipo.nombressp" con NA "nc.xls.constante.inicio.canal.online.renovacion.grupo2"
   And Valido estilo "nc.estilo.none" campo "Error" 
    
 	 Examples:
    |row_index|
    |46|
    
 	Scenario Outline: Validar SSP No precio equipo renovacion grupo3
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And esperar carga pantalla 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.renovacion.plan" 
	 And Ingreso valor "<row_index>" en el campo "Buscar Modelo" y "nc.xls.constante.inicio.equipo.nombressp" con NA "nc.xls.constante.inicio.canal.online.renovacion.grupo3"
   And Valido estilo "nc.estilo.none" campo "Error" 
    
 	 Examples:
    |row_index|
    |46|    
    
Scenario Outline: Validar SSP No precio equipo renovacion grupo4
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And esperar carga pantalla 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.renovacion.plan" 
	 And Ingreso valor "<row_index>" en el campo "Buscar Modelo" y "nc.xls.constante.inicio.equipo.nombressp" con NA "nc.xls.constante.inicio.canal.online.renovacion.grupo4"
   And Valido estilo "nc.estilo.none" campo "Error" 
    
 	 Examples:
    |row_index|
    |46|   
    
 Scenario Outline: Validar SSP No precio equipo renovacion grupo5
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And esperar carga pantalla 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.renovacion.plan" 
	 And Ingreso valor "<row_index>" en el campo "Buscar Modelo" y "nc.xls.constante.inicio.equipo.nombressp" con NA "nc.xls.constante.inicio.canal.online.renovacion.grupo5"
   And Valido estilo "nc.estilo.none" campo "Error" 
    
 	 Examples:
    |row_index|
    |46|    