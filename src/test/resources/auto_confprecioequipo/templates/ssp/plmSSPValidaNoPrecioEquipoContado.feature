#Author: danielagarcia1309@gmail.com
@plm 
Feature: Validar No precio de equipo SSP Contado
  COMO Un empleado de Telefonica
  QUIERO Validar el precio de un equipo seleccionado
  PARA Tener el proceso automatizado y no hacerlo manual

Background:
 	Given Ingreso a "ssp.url" con "ssp.port" clic en "Mi Movistar" 
 	And Initssp "Validar No precio de equipo SSP Contado" y canal "ssp" 
 	#nombre de Feature y canal el nombre de carpeta
 	
 	Scenario Outline: Validar SSP No precio equipo contado grupo2
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.plan" 
	 And esperar carga pantalla 
	 And Ingreso "ssp.grupo2.plan" en el campo "busqueda plan"
	 And esperar carga pantalla  
 	 And Doy clic en "Enter" 
 	 And esperar carga pantalla 
 	 And Doy clic en "LO QUIERO"
 	 And esperar carga pantalla 
 	 And Doy clic en "comprarplanconcelular"
 	 And esperar carga pantalla
	 And Ingreso valor "<row_index>" en el campo "Buscar Modelo" y "nc.xls.constante.inicio.equipo.nombressp" con NA "nc.xls.constante.inicio.canal.online.contado.grupo2"
   And Valido estilo "nc.estilo.none" campo "Error" 
    
 	 Examples:
    |row_index|
    |46|
    
 Scenario Outline: Validar SSP No precio equipo contado grupo3
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.plan" 
	 And esperar carga pantalla 
	 And Ingreso "ssp.grupo3.plan" en el campo "busqueda plan"
	 And esperar carga pantalla  
 	 And Doy clic en "Enter" 
 	 And esperar carga pantalla 
 	 And Doy clic en "LO QUIERO"
 	 And esperar carga pantalla 
 	 And Doy clic en "comprarplanconcelular"
 	 And esperar carga pantalla
	 And Ingreso valor "<row_index>" en el campo "Buscar Modelo" y "nc.xls.constante.inicio.equipo.nombressp" con NA "nc.xls.constante.inicio.canal.online.contado.grupo3"
   And Valido estilo "nc.estilo.none" campo "Error" 
    
 	 Examples:
    |row_index|
    |46|
    
 Scenario Outline: Validar SSP No precio equipo contado grupo4
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.plan" 
	 And esperar carga pantalla 
	 And Ingreso "ssp.grupo4.plan" en el campo "busqueda plan"
	 And esperar carga pantalla  
 	 And Doy clic en "Enter" 
 	 And esperar carga pantalla 
 	 And Doy clic en "LO QUIERO"
 	 And esperar carga pantalla 
 	 And Doy clic en "comprarplanconcelular"
 	 And esperar carga pantalla
	 And Ingreso valor "<row_index>" en el campo "Buscar Modelo" y "nc.xls.constante.inicio.equipo.nombressp" con NA "nc.xls.constante.inicio.canal.online.contado.grupo4"
   And Valido estilo "nc.estilo.none" campo "Error" 
    
 	 Examples:
    |row_index|
    |46|
    
 Scenario Outline: Validar SSP No precio equipo contado grupo5
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.plan" 
	 And esperar carga pantalla 
	 And Ingreso "ssp.grupo5.plan" en el campo "busqueda plan"
	 And esperar carga pantalla  
 	 And Doy clic en "Enter" 
 	 And esperar carga pantalla 
 	 And Doy clic en "LO QUIERO"
 	 And esperar carga pantalla 
 	 And Doy clic en "comprarplanconcelular"
 	 And esperar carga pantalla
	 And Ingreso valor "<row_index>" en el campo "Buscar Modelo" y "nc.xls.constante.inicio.equipo.nombressp" con NA "nc.xls.constante.inicio.canal.online.contado.grupo5"
   And Valido estilo "nc.estilo.none" campo "Error" 
    
 	 Examples:
    |row_index|
    |46|    
    
