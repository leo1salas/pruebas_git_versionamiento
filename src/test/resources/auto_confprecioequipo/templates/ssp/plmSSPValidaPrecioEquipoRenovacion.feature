#Author: danielagarcia1309@gmail.com
@plm 
Feature: Validar precio de equipo SSP Renovacion
  COMO Un empleado de Telefonica
  QUIERO Validar el precio de un equipo seleccionado
  PARA Tener el proceso automatizado y no hacerlo manual

Background:
 	Given Ingreso a "ssp.url" con "ssp.port" clic en "Mi Movistar" 
 	And Initssp "Validar precio de equipo SSP Renovacion" y canal "ssp" 
 	#nombre de Feature y canal el nombre de carpeta
 
Scenario Outline: Validar SSP precio equipo renovacion grupo2
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And esperar carga pantalla 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.renovacion.plan" 
	 And Ingreso valor "<row_index>" en el campo "Buscar Modelo" y "nc.xls.constante.inicio.equipo.nombressp" 
	 And esperar carga pantalla 
	 And Doy clic en "enterrenovacion" 
	 And esperar carga pantalla 
	 And Doy clic en "loquieromodelorenovacion" 
	 And esperar carga pantalla 
	 And Doy clic en "ver mas planes" 
	 And Doy clic en el plan "ssp.grupo2.plan" 
	 And esperar carga pantalla
	 And Doy clic en "Comprar" 
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
 	 And Doy clic en las cuotas ssp seis "combocuotassp6cuotasr" 
 	 And esperar carga pantalla 	  
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.renovacion.grupo2" con numero de cuota "6" y la propidad "preciocontadocuotassp"
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
	 And Doy clic en las cuotas ssp doce "combocuotassp12cuotasr" 
	 And esperar carga pantalla 
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.renovacion.grupo2" con numero de cuota "12" y la propidad "preciocontadocuotassp"
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
 	 And Doy clic en las cuotas ssp diesciocho "combocuotassp18cuotasr" 
 	 And esperar carga pantalla
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.renovacion.grupo2" con numero de cuota "18" y la propidad "preciocontadocuotassp" 
 	 And Doy clic en "eliminarorden"
 	 And esperar carga pantalla
 	 And Valido que los campos tengan los valores esperados
 	 
 	 Examples:
    |row_index|
    |46|  

   
Scenario Outline: Validar SSP precio equipo renovacion grupo3
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And esperar carga pantalla 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.renovacion.plan" 
	 And Ingreso valor "<row_index>" en el campo "Buscar Modelo" y "nc.xls.constante.inicio.equipo.nombressp" 
	 And esperar carga pantalla 
	 And Doy clic en "enterrenovacion" 
	 And esperar carga pantalla 
	 And Doy clic en "loquieromodelorenovacion" 
	 And esperar carga pantalla 
	 And Doy clic en "ver mas planes" 
	 And Doy clic en el plan "ssp.grupo3.plan" 
	 And esperar carga pantalla
	 And Doy clic en "Comprar" 
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
 	 And Doy clic en las cuotas ssp seis "combocuotassp6cuotasr" 
 	 And esperar carga pantalla 	  
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.renovacion.grupo3" con numero de cuota "6" y la propidad "preciocontadocuotassp"
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
	 And Doy clic en las cuotas ssp doce "combocuotassp12cuotasr" 
	 And esperar carga pantalla 
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.renovacion.grupo3" con numero de cuota "12" y la propidad "preciocontadocuotassp"
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
 	 And Doy clic en las cuotas ssp diesciocho "combocuotassp18cuotasr" 
 	 And esperar carga pantalla
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.renovacion.grupo3" con numero de cuota "18" y la propidad "preciocontadocuotassp" 
 	 And Doy clic en "eliminarorden"
 	 And esperar carga pantalla
 	 And Valido que los campos tengan los valores esperados
 	 
 	 Examples:
    |row_index|
    |46|    
    
 Scenario Outline: Validar SSP precio equipo renovacion grupo4
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And esperar carga pantalla 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.renovacion.plan" 
	 And Ingreso valor "<row_index>" en el campo "Buscar Modelo" y "nc.xls.constante.inicio.equipo.nombressp" 
	 And esperar carga pantalla 
	 And Doy clic en "enterrenovacion" 
	 And esperar carga pantalla 
	 And Doy clic en "loquieromodelorenovacion" 
	 And esperar carga pantalla 
	 And Doy clic en "ver mas planes" 
	 And Doy clic en el plan "ssp.grupo4.plan" 
	 And esperar carga pantalla
	 And Doy clic en "Comprar" 
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
 	 And Doy clic en las cuotas ssp seis "combocuotassp6cuotasr" 
 	 And esperar carga pantalla 	  
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.renovacion.grupo4" con numero de cuota "6" y la propidad "preciocontadocuotassp"
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
	 And Doy clic en las cuotas ssp doce "combocuotassp12cuotasr" 
	 And esperar carga pantalla 
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.renovacion.grupo4" con numero de cuota "12" y la propidad "preciocontadocuotassp"
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
 	 And Doy clic en las cuotas ssp diesciocho "combocuotassp18cuotasr" 
 	 And esperar carga pantalla
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.renovacion.grupo4" con numero de cuota "18" y la propidad "preciocontadocuotassp" 
 	 And Doy clic en "eliminarorden"
 	 And esperar carga pantalla
 	 And Valido que los campos tengan los valores esperados
 	 
 	 Examples:
    |row_index|
    |46|   
    
    
    Scenario Outline: Validar SSP precio equipo renovacion grupo5
	 And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	 And Ingreso cedula "ssp.cedula" en "Campo Cedula" clic en "Continuar" 
	 And Ingreso codigo "ssp.codigo" en "Campo Codigo" clic en "iniciarsesionssp" 
	 And esperar carga pantalla 
	 And Estoy en la URL "ssp.url" con "ssp.port" y "ssp.url.cuenta.renovacion.plan" 
	 And Ingreso valor "<row_index>" en el campo "Buscar Modelo" y "nc.xls.constante.inicio.equipo.nombressp" 
	 And esperar carga pantalla 
	 And Doy clic en "enterrenovacion" 
	 And esperar carga pantalla 
	 And Doy clic en "loquieromodelorenovacion" 
	 And esperar carga pantalla 
	 And Doy clic en "ver mas planes" 
	 And Doy clic en el plan "ssp.grupo5.plan" 
	 And esperar carga pantalla
	 And Doy clic en "Comprar" 
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
 	 And Doy clic en las cuotas ssp seis "combocuotassp6cuotasr" 
 	 And esperar carga pantalla 	  
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.renovacion.grupo5" con numero de cuota "6" y la propidad "preciocontadocuotassp"
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
	 And Doy clic en las cuotas ssp doce "combocuotassp12cuotasr" 
	 And esperar carga pantalla 
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.renovacion.grupo5" con numero de cuota "12" y la propidad "preciocontadocuotassp"
	 And esperar carga pantalla
	 And Doy clic en "combocuotasspbtn"  
	 And esperar carga pantalla
 	 And Doy clic en las cuotas ssp diesciocho "combocuotassp18cuotasr" 
 	 And esperar carga pantalla
 	 And Almaceno valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.renovacion.grupo5" con numero de cuota "18" y la propidad "preciocontadocuotassp" 
 	 And Doy clic en "eliminarorden"
 	 And esperar carga pantalla
 	 And Valido que los campos tengan los valores esperados
 	 
 	 Examples:
    |row_index|
    |46|    