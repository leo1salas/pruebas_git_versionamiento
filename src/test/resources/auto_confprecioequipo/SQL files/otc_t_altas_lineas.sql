-- DROP TABLE public.otc_t_altas_lineas;

CREATE TABLE public.otc_t_altas_lineas (
id bigserial NOT NULL,
numero_cuenta_distribucion text NULL,
canal_distribucion text NULL,
plan text NULL,
chip_prepago text NULL,
contrato text NULL,
sim text NULL,
numero_telefono text NULL,
equipo text NULL,
imei text NULL,
numero_cuota text NULL,
cliente text NULL,
procesado text NULL,
renovacion text NULL,
cuota_inicial text NULL,
CONSTRAINT otc_t_altas_lineas_pkey PRIMARY KEY (id)
);

INSERT INTO public.otc_t_altas_lineas (numero_cuenta_distribucion,canal_distribucion,plan,chip_prepago,contrato,sim,numero_telefono,equipo,imei,numero_cuota,cliente,procesado,renovacion,cuota_inicial) VALUES
('40330119','Interno','7E - PREPAGO TODO EN UNO','CHIP PREPAGO COMBO $3','24','8959',NULL,'GSM LTE SONY XPERIA X
',NULL,'1','0',NULL,NULL,NULL)
,('40691273','Interno','P0060 - PLAN MOVISTAR $12.99 TELEVENTAS',NULL,'24',NULL,NULL,'GSM LTE SONY XPERIA X
',NULL,NULL,NULL,NULL,NULL,NULL)
,('40687419','Interno','P0060 - PLAN MOVISTAR $12.99 TELEVENTAS',NULL,'24','8959',NULL,'Equipo Externo',NULL,'1','0',NULL,NULL,NULL)
,('40687419','Interno','P0060 - PLAN MOVISTAR $12.99 TELEVENTAS',NULL,'24','8959',NULL,'Equipo Externo',NULL,'1','0','PROCESADO',NULL,NULL)
,('40687426','Interno','7E - PREPAGO TODO EN UNO','CHIP PREPAGO COMBO $3','24','8959',NULL,'LTE SAMSUNG A30S NEGRO',NULL,'1','0',NULL,NULL,NULL)
,('40330064','Interno','P0060 - PLAN MOVISTAR $12.99 TELEVENTAS',NULL,'24','8959',NULL,'Equipo Externo',NULL,'1','0','PROCESADO',NULL,NULL)
,('80687650','Interno','P0063 - PLAN MOVISTAR $16.99','CHIP PREPAGO COMBO $3','24','8959',NULL,'LTE IPHONE 11 PRO 64GB GRIS','139999362251156','18','0',NULL,'Primera vez','600')
;