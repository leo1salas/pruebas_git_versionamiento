DROP TABLE public.otc_t_reporte;

CREATE TABLE public.otc_t_reporte
(
    id serial,
	fecha character varying(255) COLLATE pg_catalog."default" NOT NULL,
	canal character varying(255) COLLATE pg_catalog."default",
    nombreFeature character varying(255) COLLATE pg_catalog."default",
    nombreScenario character varying(255) COLLATE pg_catalog."default",
	tagsFeature character varying(255) COLLATE pg_catalog."default",
    mensaje character varying(2000) COLLATE pg_catalog."default",
	fallaScenario character varying(10) COLLATE pg_catalog."default",
    CONSTRAINT otc_t_reporte_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.otc_t_reporte
    OWNER to postgres;