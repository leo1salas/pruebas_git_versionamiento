DROP TABLE public.otc_t_parametro;

CREATE TABLE public.otc_t_parametro
(
    id serial,
    nombre character varying(255) COLLATE pg_catalog."default" NOT NULL,
    valor character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT otc_t_parametro_pkey PRIMARY KEY (id),
    CONSTRAINT otc_t_parametro_nombre_key UNIQUE (nombre)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.otc_t_parametro
    OWNER to postgres;
    