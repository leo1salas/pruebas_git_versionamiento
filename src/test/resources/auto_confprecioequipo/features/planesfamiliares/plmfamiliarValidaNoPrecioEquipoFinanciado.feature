#danielagarcia1309@gmail.com
@plm @naplanesfamiliaresfinanciado
Feature: Valida precio Equipo para el Modulo Planes Familiares
  COMO un empleado de Telefonica
  QUIERO validar el precio del equipo
  PARA hacerlo Automatico y no manual

Background:
 Given Login "nc.url" con "nc.port" user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Iniciar Sesión"
 And Init "Valida precio Equipo para el Modulo Planes Familiares" y canal "PlanFamilia"
 
Scenario Outline: Validar no precio equipo Financiado
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda" 
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.canal.PlanesFamiliares" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.busqueda.planFamiliares" en el campo servicio de busqueda
    And Selecciono el plan anadido 
    And Validar cargando
    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.familiares.financiado"
    And Valido estilo "nc.estilo.none" campo "Error" 
    
Examples:
    |row_index|
    |7|