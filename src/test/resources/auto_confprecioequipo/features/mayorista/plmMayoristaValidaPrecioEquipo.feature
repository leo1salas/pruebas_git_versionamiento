#Author: h.andresc1127@gmail.com
@plm @mayorista @precioEquipo
Feature: Validar precio de equipo Mayorista
  COMO Un empleado de Telefonica
  QUIERO Validar el precio de un equipo seleccionado
  PARA Tener el proceso automatizado y no hacerlo manual

Background:
 Given Login "nc.url" con "nc.port" user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Iniciar Sesión"
 And Init "Validar precio de equipo Mayorista" y canal "Mayorista"
 
Scenario Outline: Validar el precio para el canal mayorista
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
 		And Validar cargando
 		And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda" 
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.canal.Mayorista" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And cargar servicios y caracteristicas
    And Doy clic en "check"
    And esperar carga pantalla
    And Doy clic en "Equipos" 
    And Validar cargando
    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
    And Validar cargando
    And Doy clic en "Agregar"
    And Cambio al frame "Principal"
    And Cambio al frame "iframe roe"
    And Validar cargando
    And Doy clic en "OK"
    And Validar cargando
    And Esperar a que el campo "Precio" esté visible
    And Valido que el campo "Precio" tenga valor "<row_index>" y "nc.xls.constante.inicio.mayorista" NA "1"
    
    Examples:
    |row_index|
    |8|