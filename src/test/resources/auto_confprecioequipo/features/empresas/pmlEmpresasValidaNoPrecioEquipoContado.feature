#danielagarcia1309@gmail.com
@plm @naempresascontado
Feature: Valida precio Equipo para el Modulo Empresas Contado
  COMO un empleado de Telefonica
  QUIERO validar el precio del equipo
  PARA hacerlo Automatico y no manual

Background:
 Given Login "nc.url" con "nc.port" user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Iniciar Sesión"
 And Init "Valida precio Equipo para el Modulo Empresas Contado" y canal "Empresas"
 
 Scenario Outline: Validar precio financiado empresas grupo uno
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda"  
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clienteEmpresarial" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo1E.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando
    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.empresas.contado.grupo1e"
    And Valido estilo "nc.estilo.none" campo "Error" 
Examples:
    |row_index|
    |7|
    
Scenario Outline: Validar precio financiado empresas grupo dos 
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda"  
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clienteEmpresarial" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo2E.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando 
    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.empresas.contado.grupo2e"
    And Valido estilo "nc.estilo.none" campo "Error" 
Examples:
    |row_index|
    |7|
    
Scenario Outline: Validar precio financiado empresas grupo tres
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda"  
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clienteEmpresarial" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo3E.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando
    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.empresas.contado.grupo3e"
    And Valido estilo "nc.estilo.none" campo "Error" 
Examples:
    |row_index|
    |7|
    
Scenario Outline: Validar precio financiado empresas grupo cuatro
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda" 
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clienteEmpresarial" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo4E.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando
    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.empresas.contado.grupo4e"
    And Valido estilo "nc.estilo.none" campo "Error" 
Examples:
    |row_index|
    |7|    
    
Scenario Outline: Validar precio financiado empresas grupo cinco
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda" 
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clienteEmpresarial" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo5E.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido 
		And Validar cargando
    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.empresas.contado.grupo5e"
    And Valido estilo "nc.estilo.none" campo "Error" 
Examples:
    |row_index|
    |7|    