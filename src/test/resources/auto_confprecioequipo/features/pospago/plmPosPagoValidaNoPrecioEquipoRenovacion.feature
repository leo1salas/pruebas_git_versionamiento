#Author: cfdavila.indracompany.com
@plm @napospagorenovacion
Feature: Validar precio con financiado Bloque POSPAGO / PREVIOPAGO
  COMO usuario de pruebas
  QUIERO Validar el precio de contado
  PARA Tener el proceso automatizado y no hacerlo manual


  Background: 
    Given Login "nc.url" con "nc.port" user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Iniciar Sesión"
    And Init "Validar precio con financiado Bloque POSPAGO / PREVIOPAGO" y canal "Pospago"
      
  Scenario Outline: Validar no precio equipo Altas Renovacion Pospago grupo2
    	When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"
	    And esperar carga pantalla	    
	    And Ingreso "nc.otecel1" en el campo "campocliente"
    	And Doy clic en "boton.busqueda"  
    	And Doy clic en "Otecel 1"
    	And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    	And esperar carga pantalla    	
	    And cambiar frame interno
			And espero habilitar servicios y caracteristicas
    	And Ingreso "nc.grupo2.plan" en el campo servicio de busqueda
    	And Selecciono el plan anadido
	    And Validar cargando
	    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.pospago.renovacion.grupo2" 
    	And Valido estilo "nc.estilo.none" campo "Error"

    Examples: 
      |row_index|
      |14|
      
 	Scenario Outline: Validar no precio equipo Altas Renovacion Pospago grupo3
    	When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"
	    And esperar carga pantalla	    
	    And Ingreso "nc.otecel1" en el campo "campocliente"
    	And Doy clic en "boton.busqueda"  
    	And Doy clic en "Otecel 1"
    	And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    	And esperar carga pantalla    	
	    And cambiar frame interno
	    And espero habilitar servicios y caracteristicas
   		And Ingreso "nc.grupo3.plan" en el campo servicio de busqueda
    	And Selecciono el plan anadido
	    And Validar cargando
	    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.pospago.renovacion.grupo3" 
    	And Valido estilo "nc.estilo.none" campo "Error"

    Examples: 
      |row_index|
      |14|
      
	Scenario Outline: Validar no precio equipo Altas Renovacion Pospago grupo4
    	When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"
	    And esperar carga pantalla	    
	    And Ingreso "nc.otecel1" en el campo "campocliente"
    	And Doy clic en "boton.busqueda"  
    	And Doy clic en "Otecel 1"
    	And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    	And esperar carga pantalla    	
	    And cambiar frame interno
	    And espero habilitar servicios y caracteristicas
    	And Ingreso "nc.grupo4.plan" en el campo servicio de busqueda
    	And Selecciono el plan anadido
	    And Validar cargando
			And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.pospago.renovacion.grupo4" 
    	And Valido estilo "nc.estilo.none" campo "Error"	    

    Examples: 
      |row_index|
      |14|

 	Scenario Outline: Validar no precio equipo Altas Renovacion Pospago grupo5
    	When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
	    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"
	    And esperar carga pantalla	    
	    And Ingreso "nc.otecel1" en el campo "campocliente"
    	And Doy clic en "boton.busqueda" 
    	And Doy clic en "Otecel 1"
    	And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    	And esperar carga pantalla    	
	    And cambiar frame interno
	    And espero habilitar servicios y caracteristicas
      And Ingreso "nc.grupo5.plan" en el campo servicio de busqueda
      And Selecciono el plan anadido
	    And Validar cargando
	    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.pospago.renovacion.grupo5" 
    	And Valido estilo "nc.estilo.none" campo "Error"	

    Examples: 
      |row_index|
      |14|