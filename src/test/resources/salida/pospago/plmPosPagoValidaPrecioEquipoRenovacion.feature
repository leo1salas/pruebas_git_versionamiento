#Author: danielagarcia1309@gmail.com
@plm @pospagorenovacion
Feature: Valida Equipo para el Modulo Pospago Renovacion
  COMO un empleado de Telefonica
  QUIERO validar el precio del equipo
  PARA hacerlo Automatico y no manual

Background:
 Given Login "nc.url" con "nc.port" user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Iniciar Sesión" 
 And Init "Modulo Pospago Renovacion" y canal "Pospago"
 
Scenario Outline: Validar precio Financiado Renovacion Pospago Prepago Grupo2
		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda"  
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo2.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando 
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Validar cargando
		And Doy clic en "Agregar" 
		And Validar cargando 
		And Esperar a que el campo "precioempresas" esté visible
		And Doy clic en "Pago en cuotas"
		And Validar cargando
		And Doy clic en "Primera Vez" 
		And Validar cargando
    And Ingreso "6" en el combo "Combo cuotas"     
    And Validar cargando
    And esperar carga pantalla
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.renovacion.grupo2" NA "1" con "6"
    And Almaceno el campo "precioempresas" con numero de cuotas "6" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "12" en el combo "Combo cuotas" 
    And Validar cargando 
    And esperar carga pantalla
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.renovacion.grupo2" NA "1" con "12"
    And Almaceno el campo "precioempresas" con numero de cuotas "12" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
 		And Ingreso "18" en el combo "Combo cuotas"
 		And Validar cargando
 		And esperar carga pantalla
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.renovacion.grupo2" NA "1" con "18"
    And Almaceno el campo "precioempresas" con numero de cuotas "18" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
 		And Valido que los campos tengan los valores esperados
 		
	Examples:
    |row_index|
|8|
|9|
|10|
|11|
|12|
|13|
|14|
|15|
|16|
|17|
|18|
|19|
|20|
|21|
|22|
|23|
|24|
|25|
|26|
|27|
|28|
|29|
|30|
|31|
|32|
|33|
|34|
|35|
|36|
|37|
|38|
|39|
|40|
|41|
|42|
|43|
|44|
|45|
|46|
|47|
|48|
|49|
|50|
|51|
|52|
|53|
|54|
    
Scenario Outline: Validar precio Financiado Renovacion Pospago Prepago Grupo3
   	And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda"  
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo3.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando 
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Validar cargando
		And Doy clic en "Agregar" 
		And Validar cargando 
		And Esperar a que el campo "precioempresas" esté visible
		And Doy clic en "Pago en cuotas"
		And Validar cargando
		And Doy clic en "Primera Vez"
		And Validar cargando 
    And Ingreso "6" en el combo "Combo cuotas"     
    And Validar cargando
    And esperar carga pantalla
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.renovacion.grupo3" NA "1" con "6"
    And Almaceno el campo "precioempresas" con numero de cuotas "6" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "12" en el combo "Combo cuotas" 
    And Validar cargando 
    And esperar carga pantalla
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.renovacion.grupo3" NA "1" con "12"
    And Almaceno el campo "precioempresas" con numero de cuotas "12" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
 		And Ingreso "18" en el combo "Combo cuotas"
 		And Validar cargando
 		And esperar carga pantalla
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.renovacion.grupo3" NA "1" con "18"
    And Almaceno el campo "precioempresas" con numero de cuotas "18" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Valido que los campos tengan los valores esperados
 		
  Examples:
    |row_index|
|8|
|9|
|10|
|11|
|12|
|13|
|14|
|15|
|16|
|17|
|18|
|19|
|20|
|21|
|22|
|23|
|24|
|25|
|26|
|27|
|28|
|29|
|30|
|31|
|32|
|33|
|34|
|35|
|36|
|37|
|38|
|39|
|40|
|41|
|42|
|43|
|44|
|45|
|46|
|47|
|48|
|49|
|50|
|51|
|52|
|53|
|54|
        
 Scenario Outline: Validar precio Financiado Renovacion Pospago Prepago Grupo4
   	And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda"
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo4.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando 
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Validar cargando
		And Doy clic en "Agregar" 
		And Validar cargando 
		And Esperar a que el campo "precioempresas" esté visible
		And Doy clic en "Pago en cuotas"
		And Validar cargando
		And Doy clic en "Primera Vez"
		And Validar cargando 
    And Ingreso "6" en el combo "Combo cuotas"     
    And Validar cargando
    And esperar carga pantalla
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.renovacion.grupo4" NA "1" con "6"
    And Almaceno el campo "precioempresas" con numero de cuotas "6" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "12" en el combo "Combo cuotas" 
    And Validar cargando 
    And esperar carga pantalla
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.renovacion.grupo4" NA "1" con "12"
    And Almaceno el campo "precioempresas" con numero de cuotas "12" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
 		And Ingreso "18" en el combo "Combo cuotas"
 		And Validar cargando
 		And esperar carga pantalla
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.renovacion.grupo4" NA "1" con "18"
    And Almaceno el campo "precioempresas" con numero de cuotas "18" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Valido que los campos tengan los valores esperados
 		
  Examples:
    |row_index|
|8|
|9|
|10|
|11|
|12|
|13|
|14|
|15|
|16|
|17|
|18|
|19|
|20|
|21|
|22|
|23|
|24|
|25|
|26|
|27|
|28|
|29|
|30|
|31|
|32|
|33|
|34|
|35|
|36|
|37|
|38|
|39|
|40|
|41|
|42|
|43|
|44|
|45|
|46|
|47|
|48|
|49|
|50|
|51|
|52|
|53|
|54|
    
 Scenario Outline: Validar precio Financiado Renovacion Pospago Prepago Grupo5
   	When Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda"  
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.clientePospago" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo5.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando 
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Validar cargando
		And Doy clic en "Agregar" 
		And Validar cargando 
		And Esperar a que el campo "precioempresas" esté visible
		And Doy clic en "Pago en cuotas"
		And Validar cargando
		And Doy clic en "Primera Vez"
		And Validar cargando 
    And Ingreso "6" en el combo "Combo cuotas"     
    And Validar cargando
    And esperar carga pantalla
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.renovacion.grupo5" NA "1" con "6"
    And Almaceno el campo "precioempresas" con numero de cuotas "6" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "12" en el combo "Combo cuotas" 
    And Validar cargando 
    And esperar carga pantalla
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.renovacion.grupo5" NA "1" con "12"
    And Almaceno el campo "precioempresas" con numero de cuotas "12" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
 		And Ingreso "18" en el combo "Combo cuotas"
 		And Validar cargando
 		And esperar carga pantalla
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.pospago.renovacion.grupo5" NA "1" con "18"
    And Almaceno el campo "precioempresas" con numero de cuotas "18" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Valido que los campos tengan los valores esperados
 		
  Examples:
    |row_index|
|8|
|9|
|10|
|11|
|12|
|13|
|14|
|15|
|16|
|17|
|18|
|19|
|20|
|21|
|22|
|23|
|24|
|25|
|26|
|27|
|28|
|29|
|30|
|31|
|32|
|33|
|34|
|35|
|36|
|37|
|38|
|39|
|40|
|41|
|42|
|43|
|44|
|45|
|46|
|47|
|48|
|49|
|50|
|51|
|52|
|53|
|54|
