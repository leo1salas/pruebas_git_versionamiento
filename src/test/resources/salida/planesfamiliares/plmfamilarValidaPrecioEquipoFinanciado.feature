#danielagarcia1309@gmail.com
@plm @planesfamiliaresfinanciado
Feature: Valida precio Equipo para el Modulo Planes Familiares
  COMO un empleado de Telefonica
  QUIERO validar el precio del equipo
  PARA hacerlo Automatico y no manual

Background:
 Given Login "nc.url" con "nc.port" user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Iniciar Sesión"
 And Init "Valida precio Equipo para el Modulo Planes Familiares" y canal "PlanFamilia"
 
Scenario Outline: Validar  precio financiado
 		And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda" 
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.canal.PlanesFamiliares" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.busqueda.planFamiliares" en el campo servicio de busqueda
    And Selecciono el plan anadido  
		And Validar cargando
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Validar cargando
		And Doy clic en "Agregar"  
		And Validar cargando
    And Doy clic en "Pago en cuotas" 
    And Ingreso "6" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.familiares.financiado" NA "1" con "6"
    And Almaceno el campo "precioempresas" con numero de cuotas "6" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "12" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.familiares.financiado" NA "1" con "12"
    And Almaceno el campo "precioempresas" con numero de cuotas "12" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "18" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.familiares.financiado" NA "1" con "18"
    And Almaceno el campo "precioempresas" con numero de cuotas "18" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Ingreso "24" en el combo "Combo cuotas"
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.familiares.financiado" NA "1" con "24"
   	And Almaceno el campo "precioempresas" con numero de cuotas "24" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
   	And Valido que los campos tengan los valores esperados
   
   Examples:
    |row_index|
|8|
|9|
|10|
|11|
|12|
|13|
|14|
|15|
|16|
|17|
|18|
|19|
|20|
|21|
|22|
|23|
|24|
|25|
|26|
|27|
|28|
|29|
|30|
|31|
|32|
|33|
|34|
|35|
|36|
|37|
|38|
|39|
|40|
|41|
|42|
|43|
|44|
|45|
|46|
|47|
|48|
|49|
|50|
|51|
|52|
|53|
|54|
