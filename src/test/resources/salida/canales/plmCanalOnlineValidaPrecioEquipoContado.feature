#Author: danielagarcia1309@gmail.com
@plm @canalonlinecontado
Feature: Valida Equipo para el Modulo Canal online y asistido
  COMO un empleado de Telefonica
  QUIERO validar el precio del equipo
  PARA hacerlo Automatico y no manual

Background:
 Given Login "nc.url" con "nc.port" user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Iniciar Sesión"
 And Init "Valida Equipo para el Modulo Canal online y asistido" y canal "OnLine"
 
 Scenario Outline: Validar precio contado Canal Online y Asistido grupo2
   	And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda"  
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.canal.asistido" 
    And esperar carga pantalla 
    And cambiar frame interno 
   	And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo2.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando 
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Validar cargando
		And Doy clic en "Agregar" 
		And Validar cargando 
    And Esperar a que el campo "precioempresas" esté visible
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo2" NA "1" con "0"
		And Doy clic en "Pago en cuotas" 
    And Ingreso "1" en el combo "Combo cuotas"
    And Validar cargando  
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo2" NA "1" con "1"
    And Almaceno el campo "precioempresas" con numero de cuotas "1" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
    And Ingreso "3" en el combo "Combo cuotas"
    And Validar cargando 
  	And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo2" NA "1" con "3"
  	And Almaceno el campo "precioempresas" con numero de cuotas "3" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
  	And Valido que los campos tengan los valores esperados
 
  Examples:
    |row_index|
|8|
|9|
|10|
|11|
|12|
|13|
|14|
|15|
|16|
|17|
|18|
|19|
|20|
|21|
|22|
|23|
|24|
|25|
|26|
|27|
|28|
|29|
|30|
|31|
|32|
|33|
|34|
|35|
|36|
|37|
|38|
|39|
|40|
|41|
|42|
|43|
|44|
|45|
|46|
|47|
|48|
|49|
|50|
|51|
|52|
|53|
|54|
    
Scenario Outline: Validar precio contado Canal Online y Asistido grupo3
   	And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda" 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.canal.asistido" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo3.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando 
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Validar cargando
		And Doy clic en "Agregar" 
		And Validar cargando 
    And Esperar a que el campo "precioempresas" esté visible
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo3" NA "1" con "0"
		And Doy clic en "Pago en cuotas" 
    And Ingreso "1" en el combo "Combo cuotas" 
    And Validar cargando
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo3" NA "1" con "1"
    And Almaceno el campo "precioempresas" con numero de cuotas "1" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
    And Ingreso "3" en el combo "Combo cuotas"
    And Validar cargando
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo3" NA "1" con "3"
    And Almaceno el campo "precioempresas" con numero de cuotas "3" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
    And Valido que los campos tengan los valores esperados
    
  Examples:
    |row_index|
|8|
|9|
|10|
|11|
|12|
|13|
|14|
|15|
|16|
|17|
|18|
|19|
|20|
|21|
|22|
|23|
|24|
|25|
|26|
|27|
|28|
|29|
|30|
|31|
|32|
|33|
|34|
|35|
|36|
|37|
|38|
|39|
|40|
|41|
|42|
|43|
|44|
|45|
|46|
|47|
|48|
|49|
|50|
|51|
|52|
|53|
|54|
    
    
    Scenario Outline: Validar precio contado Canal Online y Asistido grupo4
   	And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda"   
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.canal.asistido" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo4.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido  
		And Validar cargando 
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Validar cargando
		And Doy clic en "Agregar" 
		And Validar cargando 
    And Esperar a que el campo "precioempresas" esté visible
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo4" NA "1" con "0"
		And Doy clic en "Pago en cuotas" 
    And Ingreso "1" en el combo "Combo cuotas"
    And Validar cargando 
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo4" NA "1" con "1"
    And Almaceno el campo "precioempresas" con numero de cuotas "1" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
    And Ingreso "3" en el combo "Combo cuotas"
    And Validar cargando
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo4" NA "1" con "3"
    And Almaceno el campo "precioempresas" con numero de cuotas "3" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Valido que los campos tengan los valores esperados 
 
  Examples:
    |row_index|
|8|
|9|
|10|
|11|
|12|
|13|
|14|
|15|
|16|
|17|
|18|
|19|
|20|
|21|
|22|
|23|
|24|
|25|
|26|
|27|
|28|
|29|
|30|
|31|
|32|
|33|
|34|
|35|
|36|
|37|
|38|
|39|
|40|
|41|
|42|
|43|
|44|
|45|
|46|
|47|
|48|
|49|
|50|
|51|
|52|
|53|
|54|
    
    
Scenario Outline: Validar precio contado Canal Online y Asistido Grupo5
   	And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda"  
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.canal.asistido" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo5.plan" en el campo servicio de busqueda
    And Selecciono el plan anadido
		And Validar cargando 
		And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo"
		And Validar cargando
		And Doy clic en "Agregar" 
		And Validar cargando 
    And Esperar a que el campo "precioempresas" esté visible
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo5" NA "1" con "0"
		And Doy clic en "Pago en cuotas" 
    And Ingreso "1" en el combo "Combo cuotas"
    And Validar cargando
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo5" NA "1" con "1"
    And Almaceno el campo "precioempresas" con numero de cuotas "1" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado" 
    And Ingreso "3" en el combo "Combo cuotas"
    And Validar cargando
    And Almaceno el campo "precioempresas" y su valor "<row_index>" y propiedad "nc.xls.constante.inicio.canal.online.contado.grupo5" NA "1" con "3"
    And Almaceno el campo "precioempresas" con numero de cuotas "3" y las propiedades "equipomontodecuota" y "equipomontototalnofacturado"
    And Valido que los campos tengan los valores esperados 
 
  Examples:
    |row_index|
|8|
|9|
|10|
|11|
|12|
|13|
|14|
|15|
|16|
|17|
|18|
|19|
|20|
|21|
|22|
|23|
|24|
|25|
|26|
|27|
|28|
|29|
|30|
|31|
|32|
|33|
|34|
|35|
|36|
|37|
|38|
|39|
|40|
|41|
|42|
|43|
|44|
|45|
|46|
|47|
|48|
|49|
|50|
|51|
|52|
|53|
|54|
