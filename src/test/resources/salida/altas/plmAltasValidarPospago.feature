#Author: danielagarcia1309@gmail.com
@plm @altaspospago
Feature: Altas Pospago 
  COMO Un empleado de Telefonica
  QUIERO realizar el alta validando el precio de un equipo seleccionado
  PARA Tener el proceso automatizado y no hacerlo manual
  
 Background: 
    Given Login "nc.url" con "nc.port" user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Iniciar Sesión"
    And Init "Altas Pospago" y canal "Altas"
  
Scenario Outline: Alta pospago Grupo Dos
    Given Consulto la tabla "OTC_T_ALTAS_LINEAS" con el id "<ID>" 
    And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"
    And esperar carga pantalla	
    And Validar cargando    
	  And Ingreso "nc.otecel1" en el campo "campocliente"
	  And Doy clic en "boton.busqueda"  
   	And Doy clic en "Otecel 1"
   	And esperar carga pantalla 
   	And Validar cargando
   	And Clic en boton por texto "boton.productos"  
    And ingreso Canal de distribucion "canal_distribucion" 
    And seleciono combo orden venta "combo.orden.venta.vacia" para "n.a" 
    And Clic en boton por texto "boton.crear"
    And esperar carga pantalla    	
	  And cambiar frame interno
	  And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo2.planB" en el campo servicio de busqueda
    And Selecciono el plan anadido  
	  And Validar tipo plan "plan" 
	  And Validar cargando
	  And selecionar contrato "contrato" ICCD "sim" numero telefono "numero_telefono" 
	  And Validar cargando
	  And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" 
	  And Validar cargando
	  And Doy clic en "Agregar"
	  And Validar cargando
	  And ingresar imei "imei" 
	  And Validar cargando
	  #And Esperar a que el campo "precioempresas" esté visible	
	  And validar crear cliente "cliente" 
    And cambio a miga de pan validar
    And cambiar limite credito "20000"
    And clic boton final reservar activo
    And esperar carga pantalla  
    And Validar cargando
    And clic boton final configurar contrato
   	And generar documento
    And clic boton final generar factura 
    And esperar carga pantalla  
    And Validar cargando
    And clic boton final enviar
    And extraer Numero orden de venta
    And Volver a la Orden
    And Entrada de Orden
    And Seleccionar la orden anterior   
    And Verificar Numero de Cuenta y Guardar URL
    #Then Guardar resultado en BD flujo "ALTAS_LINEAS" 
   	 
      Examples: 
      | ID |row_index|
|1|8|
|2|9|
|3|10|
|4|11|
|5|12|
|6|13|
|7|14|
|8|15|
|9|16|
|10|17|
|11|18|
|12|19|
|13|20|
|14|21|
|15|22|
|16|23|
|17|24|
|18|25|
|19|26|
|20|27|
|21|28|
|22|29|
|23|30|
|24|31|
|25|32|
|26|33|
|27|34|
|28|35|
|29|36|
|30|37|
|31|38|
|32|39|
|33|40|
|34|41|
|35|42|
|36|43|
|37|44|
|38|45|
|39|46|
|40|47|
|41|48|
|42|49|
|43|50|
|44|51|
|45|52|
|46|53|
|47|54|

  
	Scenario Outline: Alta pospago Grupo Tres
    Given Consulto la tabla "OTC_T_ALTAS_LINEAS" con el id "<ID>" 
    And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"
    And esperar carga pantalla	
    And Validar cargando    
	  And Ingreso "nc.otecel1" en el campo "campocliente"
	  And Doy clic en "boton.busqueda"  
   	And Doy clic en "Otecel 1"
   	And esperar carga pantalla 
   	And Validar cargando
   	And Clic en boton por texto "boton.productos"  
    And ingreso Canal de distribucion "canal_distribucion" 
    And seleciono combo orden venta "combo.orden.venta.vacia" para "n.a" 
    And Clic en boton por texto "boton.crear"
    And esperar carga pantalla    	
	  And cambiar frame interno
	  And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo3.planB" en el campo servicio de busqueda
    And Selecciono el plan anadido 
	  And Validar tipo plan "plan" 
	  And Validar cargando
	  And selecionar contrato "contrato" ICCD "sim" numero telefono "numero_telefono" 
	  And Validar cargando
	  And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" 
	  And Validar cargando
	  And Doy clic en "Agregar"
	  And Validar cargando
	  And ingresar imei "imei" 
	  And Validar cargando
	  #And Esperar a que el campo "precioempresas" esté visible	
	  And validar crear cliente "cliente" 
    And cambio a miga de pan validar
    And cambiar limite credito "20000"
    And clic boton final reservar activo
    And esperar carga pantalla  
    And Validar cargando
    And clic boton final configurar contrato
   	And generar documento
    And clic boton final generar factura 
    And esperar carga pantalla  
    And Validar cargando
    And clic boton final enviar
    And extraer Numero orden de venta
    And Volver a la Orden
    And Entrada de Orden
    And Seleccionar la orden anterior   
    And Verificar Numero de Cuenta y Guardar URL
    #Then Guardar resultado en BD flujo "ALTAS_LINEAS" 
   	 
      Examples: 
      | ID |row_index|
|1|8|
|2|9|
|3|10|
|4|11|
|5|12|
|6|13|
|7|14|
|8|15|
|9|16|
|10|17|
|11|18|
|12|19|
|13|20|
|14|21|
|15|22|
|16|23|
|17|24|
|18|25|
|19|26|
|20|27|
|21|28|
|22|29|
|23|30|
|24|31|
|25|32|
|26|33|
|27|34|
|28|35|
|29|36|
|30|37|
|31|38|
|32|39|
|33|40|
|34|41|
|35|42|
|36|43|
|37|44|
|38|45|
|39|46|
|40|47|
|41|48|
|42|49|
|43|50|
|44|51|
|45|52|
|46|53|
|47|54|
      
      
 Scenario Outline: Alta pospago Grupo Cuatro
    Given Consulto la tabla "OTC_T_ALTAS_LINEAS" con el id "<ID>" 
    And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"
    And esperar carga pantalla	
    And Validar cargando    
	  And Ingreso "nc.otecel1" en el campo "campocliente"
	  And Doy clic en "boton.busqueda"  
   	And Doy clic en "Otecel 1"
   	And esperar carga pantalla 
   	And Validar cargando
   	And Clic en boton por texto "boton.productos"  
    And ingreso Canal de distribucion "canal_distribucion" 
    And seleciono combo orden venta "combo.orden.venta.vacia" para "n.a" 
    And Clic en boton por texto "boton.crear"
    And esperar carga pantalla    	
	  And cambiar frame interno
	  And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo4.planB" en el campo servicio de busqueda
    And Selecciono el plan anadido  
	  And Validar tipo plan "plan" 
	  And Validar cargando
	  And selecionar contrato "contrato" ICCD "sim" numero telefono "numero_telefono" 
	  And Validar cargando
	  And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" 
	  And Validar cargando
	  And Doy clic en "Agregar"
	  And Validar cargando
	  And ingresar imei "imei" 
	  And Validar cargando
	  #And Esperar a que el campo "precioempresas" esté visible	
	  And validar crear cliente "cliente" 
    And cambio a miga de pan validar
    And cambiar limite credito "20000"
    And clic boton final reservar activo
    And esperar carga pantalla  
    And Validar cargando
    And clic boton final configurar contrato
   	And generar documento
    And clic boton final generar factura 
    And esperar carga pantalla  
    And Validar cargando
    And clic boton final enviar
    And extraer Numero orden de venta
    And Volver a la Orden
    And Entrada de Orden
    And Seleccionar la orden anterior   
    And Verificar Numero de Cuenta y Guardar URL
    #Then Guardar resultado en BD flujo "ALTAS_LINEAS" 
   	 
      Examples: 
      | ID |row_index|
|1|8|
|2|9|
|3|10|
|4|11|
|5|12|
|6|13|
|7|14|
|8|15|
|9|16|
|10|17|
|11|18|
|12|19|
|13|20|
|14|21|
|15|22|
|16|23|
|17|24|
|18|25|
|19|26|
|20|27|
|21|28|
|22|29|
|23|30|
|24|31|
|25|32|
|26|33|
|27|34|
|28|35|
|29|36|
|30|37|
|31|38|
|32|39|
|33|40|
|34|41|
|35|42|
|36|43|
|37|44|
|38|45|
|39|46|
|40|47|
|41|48|
|42|49|
|43|50|
|44|51|
|45|52|
|46|53|
|47|54|
      
 Scenario Outline: Alta pospago Grupo Cinco
    Given Consulto la tabla "OTC_T_ALTAS_LINEAS" con el id "<ID>" 
    And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify" 
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"
    And esperar carga pantalla	
    And Validar cargando    
	  And Ingreso "nc.otecel1" en el campo "campocliente"
	  And Doy clic en "boton.busqueda"  
   	And Doy clic en "Otecel 1"
   	And esperar carga pantalla 
   	And Validar cargando
   	And Clic en boton por texto "boton.productos"  
    And ingreso Canal de distribucion "canal_distribucion" 
    And seleciono combo orden venta "combo.orden.venta.vacia" para "n.a" 
    And Clic en boton por texto "boton.crear"
    And esperar carga pantalla    	
	  And cambiar frame interno
	  And espero habilitar servicios y caracteristicas
    And Ingreso "nc.grupo5.planA" en el campo servicio de busqueda
    And Selecciono el plan anadido  
	  And Validar tipo plan "plan" 
	  And Validar cargando
	  And selecionar contrato "contrato" ICCD "sim" numero telefono "numero_telefono" 
	  And Validar cargando
	  And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" 
	  And Validar cargando
	  And Doy clic en "Agregar"
	  And Validar cargando
	  And ingresar imei "imei" 
	  And Validar cargando
	  #And Esperar a que el campo "precioempresas" esté visible	
	  And validar crear cliente "cliente" 
    And cambio a miga de pan validar
    And cambiar limite credito "20000"
    And clic boton final reservar activo
    And esperar carga pantalla  
    And Validar cargando
    And clic boton final configurar contrato
   	And generar documento
    And clic boton final generar factura 
    And esperar carga pantalla  
    And Validar cargando
    And clic boton final enviar
    And extraer Numero orden de venta
    And Volver a la Orden
    And Entrada de Orden
    And Seleccionar la orden anterior   
    And Verificar Numero de Cuenta y Guardar URL
    #Then Guardar resultado en BD flujo "ALTAS_LINEAS" 
   	 
      Examples: 
      |ID|row_index|
|1|8|
|2|9|
|3|10|
|4|11|
|5|12|
|6|13|
|7|14|
|8|15|
|9|16|
|10|17|
|11|18|
|12|19|
|13|20|
|14|21|
|15|22|
|16|23|
|17|24|
|18|25|
|19|26|
|20|27|
|21|28|
|22|29|
|23|30|
|24|31|
|25|32|
|26|33|
|27|34|
|28|35|
|29|36|
|30|37|
|31|38|
|32|39|
|33|40|
|34|41|
|35|42|
|36|43|
|37|44|
|38|45|
|39|46|
|40|47|
|41|48|
|42|49|
|43|50|
|44|51|
|45|52|
|46|53|
|47|54|
