#Author: h.andresc1127@gmail.com
@plm @mayorista @noPrecio
Feature: Validar no precio de equipo 
  COMO Un empleado de Telefonica
  QUIERO Iniciar sesión en la plataforma
  PARA Tener el proceso automatizado y no hacerlo manual

Background:
 Given Login "nc.url" con "nc.port" user "nc.user" en "Username" pass "nc.pass" en "Password" clic en "Iniciar Sesión"
 
Scenario Outline: Validar no precio mayorista
    And Validar "<row_index>" modificado "nc.modify" columna "nc.xls.constante.inicio.modify"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.cliente"   
    And esperar carga pantalla 
    And Ingreso "nc.otecel1" en el campo "campocliente"
    And Doy clic en "boton.busqueda" 
    And esperar carga pantalla 
    And Doy clic en "Otecel 1"
    And Estoy en la URL "nc.url" con "nc.port" y "nc.url.cuenta.canal.Mayorista" 
    And esperar carga pantalla 
    And cambiar frame interno 
    And cargar servicios y caracteristicas
    And Doy clic en "check"
    And esperar carga pantalla
    And Doy clic en "Equipos"
    And Validar cargando
    And Ingreso valor "<row_index>" en el campo "Modelo" y "nc.xls.constante.inicio.equipo" con NA "nc.xls.constante.inicio.mayorista"
    And Valido estilo "nc.estilo.none" campo "Error" 
    
     Examples:
    |row_index|
|8|
|9|
|10|
|11|
|12|
|13|
|14|
|15|
|16|
|17|
|18|
|19|
|20|
|21|
|22|
|23|
|24|
|25|
|26|
|27|
|28|
|29|
|30|
|31|
|32|
|33|
|34|
|35|
|36|
|37|
|38|
|39|
|40|
|41|
|42|
|43|
|44|
|45|
|46|
|47|
|48|
|49|
|50|
|51|
|52|
|53|
|54|
